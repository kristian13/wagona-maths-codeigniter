<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Teacher extends CI_Controller {

	function __construct() {

		parent::__construct();
		$this->load->model('School_model');
	}

	// Dashboard
	function dashboard() {

		// Teacher Authorize
		$this->authorize();

		$data['classes'] = $this->School_model->get_teacher_classes($this->session->userdata('teacher_id'));
		foreach($data['classes'] as $key => $val) {
			$running_student_average = 0;
			$runnng_student_count = 0;
			$class_average = '';
			// $data['classes'][$key]['average'] = $this->School_model->get_teacher_class_average($val['class_id']);

			$students = $this->School_model->get_class_students_average($val['class_id']);
			foreach($students as $sKey => $sVal) {
				$students[$sKey]['name'] = $sVal['surname'] . ', ' . $sVal['firstname'];
				// $students[$sKey]['subject_average'] = is_numeric($sVal['subject_average']) && $sVal['tests_taken'] >= WG_LEAST_COLOR_CODING_NUM ? round($sVal['subject_average']) : '--';
				$student_average = $this->School_model->get_student_average_by_topic_details($val['class_id'], $sVal['student_id']);
				$students[$sKey]['subject_average'] = $student_average;

				$numeric_student_average = preg_replace('/\D/', '', $student_average);
				if(is_numeric($numeric_student_average)) {
					$running_student_average += $numeric_student_average;
					$runnng_student_count++;
				}
			}

			// Calculate Class Average
			if($runnng_student_count > 0 && $runnng_student_count >= (count($students) / 2)) {
				$class_average = round($running_student_average / $runnng_student_count) . '%';
			}

			$data['classes'][$key]['average'] = $class_average;
			$data['classes'][$key]['students'] = $students;
		}

		$this->load->view('school/teacher_dashboard', $data);
	}

	// Get Teacher Subject Topics
	function get_teacher_subject_topics() {

		$topics = $this->School_model->get_teacher_subject_topics($this->input->post('subject_id'));
		$html = '';

		if(count($topics) > 0) {
			foreach($topics as $key => $val) {
				$average_details = $this->School_model->get_teacher_topic_average_details($val['topic_id'], $this->input->post('class_id'));
				$description = str_replace(".", "", preg_replace('/[0-9]+/', '', $val['description']));

				$html .= '
					<div class="checkbox-row ' . helper_get_score_class($average_details['score'], $average_details['num_questions']) . '">
						<label>
							<table style="width: 100%;">
								<tbody>
									<tr>
										<td width="50%"><input type="checkbox" name="topchoice[]" id="top_'. $val['topic_id'] . '" class="checkbox_topic" value=' . $val['topic_id'] . ' data-description="' . $description . '">' . $description . '</td>
										<td style="text-align: right; width:12.5%">' . $average_details['num_questions'] . '</td>
										<td style="text-align: right; width:12.5%">' . $average_details['num_correct'] . '</td>
										<td style="text-align: right; width:12.5%">' . $average_details['num_wrong'] . '</td>
										<td style="text-align: right; width:12.5%">' . $average_details['percentage'] . '</td>
									</tr>
								</tbody>
							</table>
						</label>
					</div>
				';
			}
		}

		$data = array(
			'result' => true,
			'html' => $html
		);
		die(json_encode($data));
	}

	// Get Student Report
	function get_student_report() {
		$report_data = $this->student_report_data();

		$json_data = array(
			'draw' => 1,
			'recordsTotal' => count($report_data['data']),
			'recordsFiltered' => count($report_data['data']),
			'data' => $report_data['formatted_data']
		);
		die(json_encode($json_data));
	}

	// Generate Student Report
	function generate_student_report() {

		ini_set('memory_limit', '-1');

		$report_data = $this->student_report_data();

		// Load Helpers
		$this->load->helper(array('dompdf', 'file', 'download'));

		// $data = array(
		// 	'column_headers' => $this->input->post('column_headers'),
		// 	'data' => $report_data['formatted_data']
		// );

		$html = $this->html_student_report_data($report_data['formatted_data']);
		$pdf = pdf_create($html, '', false, false);
		$file_name = 'download_' . date('U') . '.pdf';
		$file_path = 'reports/' . $file_name;
		write_file($file_path, $pdf);

		$json_data = array(
			'file_name' => $file_name
		);
		die(json_encode($json_data));
	}

	// Student Report Data
	private function student_report_data() {
		$data = $this->School_model->get_class_students($this->input->post('class_id'));
		$formatted_data = array();
		$topic_ids = $this->input->post('topic_ids');

		foreach($data as $key => $val) {
			$num_topics_taken = 0;
			$topics_running_percentage = 0;
			$details = array();

			// Add Name
			$details[] = $val['surname'];
			$details[] = $val['firstname'];

			// Add Topic Details
			foreach($topic_ids as $tKey => $topic_id) {
				$topic_questions_answered_details = $this->School_model->get_student_answered_topic_question_details($val['student_id'], $topic_id);
				$details[] = '<div class="text-center school-student-data-percentage-td ' . helper_get_score_class($topic_questions_answered_details['percentage'], $topic_questions_answered_details['num_questions']) . '">' . $topic_questions_answered_details['percentage'] . '<div>';

				$percentage = preg_replace('/\D/', '', $topic_questions_answered_details['percentage']);
				if(is_numeric($percentage)) {
					$num_topics_taken++;
					$topics_running_percentage += $percentage;
				}
			}

			// // Add Average
			// if($num_topics_taken > 0) {
			// 	$student_average = round(number_format($topics_running_percentage / $num_topics_taken, 2, '.', '')) . '%';
			// 	$details[] = '<div class="text-center school-student-data-percentage-td ' . helper_get_score_class($student_average) . '"><strong>' . $student_average . '<strong><div>';
			// }
			// else {
			// 	$details[] = '<div class="text-center school-student-data-percentage-td score-black"><strong>--<strong><div>';
			// }

			// Add Average - same in students average on class list
			$student_class_average = $this->School_model->get_class_students_average($this->input->post('class_id'), $val['student_id']);
			$student_class_average = $student_class_average[0];
			if(is_numeric($student_class_average['subject_average']) && $student_class_average['tests_taken'] >= WG_LEAST_COLOR_CODING_NUM) {
				$details[] = '<div class="text-center school-student-data-percentage-td ' . helper_get_score_class($student_class_average['subject_average']) . '"><strong>' . round($student_class_average['subject_average']) . '%</strong></div>';
			}
			else {
				$details[] = '<div class="text-center school-student-data-percentage-td score-black"><strong>--</strong></div>';
			}

			$formatted_data[] = $details;
		}

		return array(
			'data' => $data,
			'formatted_data' => $formatted_data
		);
	}

	// HTML Student Report Data
	function html_student_report_data($data) {
		$html = '';
		$column_headers = $this->input->post('column_headers');

		$html .= '
		<!DOCTYPE html>
		<html>
			<head>
				<meta charset="utf-8">
				<title></title>
				<style>
					body {
						color: #000;
						font-family: "verdana", "sans-serif";
						margin: 0px;
						padding-top: 0px;
						font-size: 12px;
					}
					table {
						width: 100%;
						border-collapse: collapse;
					}
					td, th {
						border: 1px solid #000;
						padding: 2px 5px;
					}
					.text-center {
						text-align: center;
					}
					hr {
						border: 0;
						border-bottom: 1px solid rgba(0, 0, 0, 0.3);
						margin: 20px 0;
					}
					.score-red {color: #B02115;}
					.score-amber {color: #FF7E00;}
					.score-green {color: #319208;}
					.score-black {color: #333333;}
				</style>
			</head>
			<body>
		';

		foreach($data as $k => $v) {
			$surname = $v[0];
			$firstname = $v[1];
			$average = $v[count($v) - 1];

			$html .= '
			<span>' . $surname . ', ' . $firstname . '</span><br><span>Average: ' . strip_tags($average, '<strong>') . '</span>
			<table>
				<tbody>
					<tr>
						<th width="45%">Subject</th>
						<th width="5%">%</th>
						<th width="45%">Subject</th>
						<th width="5%">%</th>
					</tr>
			';

			foreach($v as $sK => $sV) {
				if($sK > 1 && $sK < count($v) - 1) {
					$key = $sK - 2;

					if($key % 2 == 0) {
						$html .= '<tr>';
					}

					$html .= '<td>' . $column_headers[$sK] . '</td>';
					$html .= '<td>' . $sV . '</td>';

					if($key % 2 == 0 && $sK == count($v) - 2) {
						$html .= '<td></td><td></td>';
					}

					if($key % 2 == 1 || $sK == count($v) - 2) {
						$html .= '</tr>';
					}
				}
			}

			$html .= '
				</tbody>
			</table>
			<hr>
			';
		}

		$html .= '
			</body>
		</html>
		';
		// print'<pre>';print_r($html);die();
		return $html;
	}

	// Teacher Authorize
	function authorize() {
		if(!$this->session->userdata('login')){
			redirect('site/login');
		}
		else {
			// Check if not School Account
			// Or Check if School Account User is not admin
			if(!$this->session->userdata('school_account') || $this->session->userdata('school_user') != 'teacher') {
				redirect('');
			}
		}
	}
}