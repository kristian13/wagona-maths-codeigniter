<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Account extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Account_model');
		$this->load->model('School_model');
	}

	public function dashboard()
	{
		$this->authorize();
		$data['page'] = 'home';
		$data['template'] = 'default';
		$data['is_trial_taken'] = $this->Account_model->is_trial_taken();
		$data['subjects'] = $this->Account_model->get_all_subject();
		// $this->load->view('account/account_dashboard', $data);
		$this->load->view('account/account_dashboard_launch', $data);
	}

	public function account_mytests()
	{
		$this->authorize();
		$data['page'] = 'account_mytests';
		$data['template'] = 'default';
		$data['alltest'] = $this->Account_model->get_my_tests();
		if($this->session->userdata('school_account') && $this->session->userdata('school_user') == 'teacher') {
			$class_id = $this->input->get('class_id');
			$student_id = $this->input->get('student_id');

			// Get Class Details
			$class = $this->School_model->get_classes(null, $class_id);
			$class = $class[0];
			$data['class_name'] = $class['name'];

			// Get Student Details
			$student = $this->School_model->get_students($student_id);
			$student = $student[0];
			$data['student_name'] = $student['surname'] . ', ' . $student['firstname'];

			// Get Student Average
			$data['average'] = $this->School_model->get_student_average_by_topic_details($class_id, $student_id) . '%';
		}
		$this->load->view('account/account_mytests', $data);
	}


	public function logout()
	{
		$this->session->sess_destroy();
		redirect('site/login');
	}

	public function test_questions($topid = 0)
	{
		$this->authorize();

		$data['page'] = 'questions';
		$data['template'] = 'default question';
		//$data['test'] = $this->Account_model->sequence_puller($topid);
		// $data['test'] = $this->Account_model->dyna_sequence($topid);
		$data['test'] = $this->Account_model->paid_puller($topid);
		$this->load->view('account/account_question', $data);

	}

	public function test_result($testid = 0)
	{
		$this->authorize();

		// // Check if account is FREE-ACCOUNT
		// // Then Check if the time is already expired to view a test result
		// if($this->session->userdata('acctype') == 'FREE-ACCOUNT') {
		// 	$time_taken_free_test = $this->session->userdata('time_taken_free_test');
		// 	$time_now = date('U');

		// 	// Render the Forbidden View
		// 	if($time_now - $time_taken_free_test > WG_TIME_VIEW_FREE_TEST) {
		// 		$data['template'] = 'default';
		// 		$this->load->view('forbidden_view', $data);
		// 		return;
		// 	}
		// }

		$data['page'] = 'questions';
		$data['template'] = 'default result question';
		$data['result'] = $this->Account_model->test_result($testid);
		$this->load->view('account/account_result', $data);
	}

	public function test_free_quiz($topid = 0)
	{
		$this->authorize();
		$data['page'] = 'questions';
		$data['template'] = 'default question';
		$data['test'] = $this->Account_model->free_puller($topid);
		$this->load->view('account/account_free_quiz', $data);
	}

	public function generate_result()
	{
		$this->Account_model->generate_result();
	}

	public function generate_free_result()
	{
		@session_start();

		$this->authorize();

		$result = $this->Account_model->generate_free_result(@$_POST['data']);
		$_SESSION['free_quiz'] = $result;

		$datas = array("success"=> true);
		die(json_encode($datas));

	}

	public function free_result_viewer()
	{
		$this->authorize();
		$data['page'] = 'questions';
		$data['template'] = 'default result question';
		$this->Account_model->update_trial_record();
		$this->load->view('account/account_trial_result', $data);
	}

	public function get_check_topics()
	{
		$this->Account_model->get_check_topics($_POST);
	}

	public function get_question_details() {
		$this->Account_model->get_question_details($_POST);
	}

	public function authorize()
	{
		if($this->session->userdata('login') != true) {
			redirect('site/login');
		}
		else {
			// Check if Individual Account
			// Or Check if School Student Account
			// Or Check if School Teacher Account accessing Test Questions
			if(
				!$this->session->userdata('acctype') &&
				!(
					$this->session->userdata('school_account') &&
					(
						$this->session->userdata('school_user') == 'student' ||
						(
							$this->session->userdata('school_user') == 'teacher' &&
							(
								strpos(uri_string(), 'account/test_free_quiz') !== false ||
								strpos(uri_string(), 'account/generate_free_result') !== false ||
								strpos(uri_string(), 'account/free_result_viewer') !== false ||
								(strpos(uri_string(), 'account/account_mytests') !== false && $this->input->get('student_id') && $this->input->get('class_id')) ||
								strpos(uri_string(), 'account/test_result') !== false
							)
						)
					)
				)
			) {
				redirect('');
			}
		}
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */