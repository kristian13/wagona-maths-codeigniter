<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Site extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Site_model');
	}

	public function index()
	{
		$data['page'] = 'home';
		$data['template'] = 'home';
		$this->load->view('content_home', $data);
	}

	public function features()
	{
		$data['page'] = 'features';
		$data['template'] = 'default';
		$this->load->view('content_feature', $data);
	}

	public function pricing()
	{
		$data['page'] = 'pricing';
		$data['template'] = 'default';
		$this->load->view('content_pricing', $data);
	}

	public function aboutus()
	{
		$data['page'] = 'aboutus';
		$data['template'] = 'default';
		$this->load->view('content_about', $data);
	}

	public function forum()
	{
		$data['page'] = 'forum';
		$data['template'] = 'default';
		$this->load->view('content_forum', $data);
	}

	public function reviews()
	{
		$data['page'] = 'reviews';
		$data['template'] = 'default';
		$this->load->view('content_review', $data);
	}

	public function free_trial()
	{
		$data['page'] = 'free_trial';
		$data['template'] = 'default';
		$this->load->view('free_form/content_form', $data);
	}

	public function contactus()
	{
		$data['page'] = 'contactus';
		$data['template'] = 'default';
		$this->load->view('content_contact', $data);
	}

	public function dailyq()
	{
		$data['page'] = 'dailyq';
		$data['template'] = 'default';
		$this->load->view('content_dailyq', $data);
	}

	public function privacy()
	{
		$data['page'] = 'privacy';
		$data['template'] = 'default';
		$this->load->view('content_privacy', $data);
	}

	public function cookie()
	{
		$data['page'] = 'cookie';
		$data['template'] = 'default';
		$this->load->view('content_cookie', $data);
	}

	public function term_condition()
	{
		$data['page'] = 'term_condition';
		$data['template'] = 'default';
		$this->load->view('content_term_condition', $data);
	}

	public function sign_option()
	{
		$data['page'] = 'sign_option';
		$data['template'] = 'default';
		$this->load->view('content_sign_option', $data);
	}

	public function login()
	{
		$data['page'] = 'login';
		$data['template'] = 'default';
		$this->load->view('content_login', $data);
	}

	public function howitworks()
	{
		$data['page'] = 'howitworks';
		$data['template'] = 'default';
		$this->load->view('content_howitworks', $data);
	}

	public function schools()
	{
		$data['page'] = 'schools';
		$data['template'] = 'default';
		$this->load->view('content_schools', $data);
	}

	public function methodology()
	{
		$data['page'] = 'methodology';
		$data['template'] = 'default';
		$this->load->view('content_methodology', $data);
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect('site/login');
	}

	public function send_my_message()
	{
		$this->Site_model->send_my_message($_POST);
	}

	public function send_my_feedback()
	{
		$this->Site_model->send_my_feedback($_POST);
	}

	public function authorize()
	{
		if($this->session->userdata('login') != true){
			redirect('site/login');
		}

	}

	public function loginprocess()
	{
		$username = $this->input->post('useremail');
		$password = $this->input->post('password');

		$login = $this->Site_model->loginprocess($username,$password);
		if($login['result'] == true){
			die(json_encode(array("success"=> true, "link"=> $login['link'])));
		}else{
			die(json_encode(array("success"=> false, "msg" => "Email/Username and Password did not match.")));
		}
	}

# paypal transaction area here ================================================================

	public function paidform()
	{
		$data['page'] = 'payment_form1';
		$data['template'] = 'default';
		$this->load->view('paid_form/content_form', $data);
	}

	public function paidregistration()
	{
		$this->Site_model->paidregistration();
	}

	public function ipn()
	{
		$this->Site_model->ipn($_POST);
	}

	public function success_subscription()
	{
		$data['page'] = 'thank_you';
		$data['template'] = 'default thanks';
		$this->load->view('paid_form/content_thankyou', $data);
	}

	public function cancel_subscription()
	{
		$data['page'] = 'thank_you';
		$data['template'] = 'default thanks';
		$this->load->view('paid_form/content_cancel', $data);
	}

# free account process here ==================================================================

	public function freeregistration()
	{
		$this->Site_model->freeregistration();
	}


	public function upgrade_account()
	{
		$this->Site_model->upgrade_account();
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */