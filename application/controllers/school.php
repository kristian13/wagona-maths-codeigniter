<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class School extends CI_Controller {

	function __construct() {

		parent::__construct();
		$this->load->model('School_model');
		$this->load->model('Site_model');
	}

	// Dashboard
	function dashboard() {

		// Admin Authorize
		$this->authorize();

		$data['subjects'] = $this->School_model->get_subjects();
		$data['classes'] = $this->School_model->get_classes();
		$this->load->view('school/school_dashboard', $data);
	}

	// School Signup
	function signup() {

		$this->load->view('school/school_signup');
	}

	// Process Signup
	function process_signup() {

		// Validate if School Name exists
		$valid = $this->School_model->validate_school_name_exists(mysql_real_escape_string($this->input->post('name')));
		if(!$valid) {
			$data = array(
				'result' => false,
				'field' => 'name',
				'message' => 'School Name already exists'
			);
			die(json_encode($data));
		}

		// Validate if School Email Address exists
		$valid = $this->School_model->validate_school_email_address_exists(mysql_real_escape_string($this->input->post('email_address')));
		if(!$valid) {
			$data = array(
				'result' => false,
				'field' => 'email_address',
				'message' => 'Email Address already exists'
			);
			die(json_encode($data));
		}

		// Insert Data
		$school_id = $this->School_model->insert_school();

		// Get School Details
		$school = $this->School_model->get_schools($school_id);
		$this->Site_model->details = $school[0];
		$this->Site_model->set_school_account_session();

		$data = array(
			'result' => true,
			'link' => base_url() . 'school'
		);
		die(json_encode($data));
	}

	// Get Schools
	function school_get() {

		// Check if there's a school id
		if($this->input->post('school_id')) {
			$data = $this->School_model->get_schools($this->input->post('school_id'));
			die(json_encode($data[0]));
		}
		else {
			$data = $this->School_model->get_schools();
			$formatted_data = array();

			foreach($data as $key => $val) {
				$details = array();
				foreach($val as $kCol => $vVal) {
					if(in_array($kCol, array('name', 'website_url', 'num_students'))) {
						$details[] = $vVal;
					}
				}
				$details[] = '
					<a href="#" data-id="' . $val['school_id'] . '" data-action="edit" data-name="' . $val['name'] . '" data-toggle="tooltip" data-placement="top" title="Edit School"><span class="glyphicon glyphicon-pencil"></span></a>&emsp;
					<a href="#" data-id="' . $val['school_id'] . '" data-action="view" data-name="' . $val['name'] . '" data-toggle="tooltip" data-placement="top" title="View School Details"><span class="glyphicon glyphicon-search"></span></a>
				';
				$formatted_data[] = $details;
			}

			$json_data = array(
				'draw' => 1,
				'recordsTotal' => count($data),
				'recordsFiltered' => count($data),
				'data' => $formatted_data
			);
			die(json_encode($json_data));
		}
	}

	// View School
	function school_view() {

		$data = $this->School_model->get_schools($this->input->post('school_id'));
		$data = $data[0];

		$html = '
			<p><strong>School Name: </strong>' . $data['name'] . '</p>
			<p><strong>Address 1:</strong>' . ($data['address_1'] != '' ? '<br>' . $data['address_1'] : '') . '</p>
			<p><strong>Address 2:</strong>' . ($data['address_2'] != '' ? '<br>' . $data['address_2'] : '') . '</p>
			<p><strong>Address 3:</strong>' . ($data['address_3'] != '' ? '<br>' . $data['address_3'] : '') . '</p>
			<p><strong>Admin Contact: </strong>' . $data['admin_contact'] . '</p>
			<p><strong>Telephone Number: </strong>' . $data['telephone_num'] . '</p>
			<p><strong>Cellphone Number: </strong>' . $data['cellphone_num'] . '</p>
			<p><strong>Email Address: </strong>' . $data['email_address'] . '</p>
			<p><strong>Website url: </strong>' . $data['website_url'] . '</p>
			<p><strong>Number of Students: </strong>' . $data['num_students'] . '</p>
		';

		$json_data = array(
			'html' => $html
		);
		die(json_encode($json_data));
	}

	// Create School
	function school_create() {

		$data = array();

		// Validate if School Name exists
		$valid = $this->School_model->validate_school_name_exists(mysql_real_escape_string($this->input->post('name')));
		if(!$valid) {
			$data = array(
				'result' => false,
				'message' => 'School Name already exists'
			);
			die(json_encode($data));
		}

		// Insert Data
		$this->School_model->insert_school();
		$data = array(
			'result' => true
		);
		die(json_encode($data));
	}

	// Update School
	function school_update() {

		$data = array();

		// Validate if School Name exists
		$valid = $this->School_model->validate_school_name_exists(mysql_real_escape_string($this->input->post('name')), $this->input->post('school_id'));
		if(!$valid) {
			$data = array(
				'result' => false,
				'message' => 'School Name already exists'
			);
			die(json_encode($data));
		}

		// Update Data
		$this->School_model->update_school();
		$data = array(
			'result' => true
		);
		die(json_encode($data));
	}

	// Get Classes
	function class_get() {

		// Check if there's a class id
		if($this->input->post('class_id')) {
			$data = $this->School_model->get_classes(null, $this->input->post('class_id'), false);
			die(json_encode($data[0]));
		}
		else {
			$data = $this->School_model->get_classes($this->input->post('subject_id'), null, false);
			$formatted_data = array();

			foreach($data as $key => $val) {
				$details = array();
				foreach($val as $kCol => $vVal) {
					if(in_array($kCol, array('name', 'description', 'class_status'))) {
						if($kCol == 'class_status') {
							$text = $vVal == 1 ? 'Active' : 'Inactive';
							$class = $vVal == 1 ? 'active' : 'inactive';
							$details[] = '<span class="' . $class . '">' . $text . '</span>';
						}
						else {
							$details[] = $vVal;
						}
					}
				}
				$change_status_icon = $val['class_status'] == 1 ? 'glyphicon-remove' : 'glyphicon-ok';
				$change_status_text = $val['class_status'] == 1 ? 'Deactive Class' : 'Activate Class';
				$change_status_action = $val['class_status'] == 1 ? 'deactivate' : 'activate';
				$change_status_class = $val['class_status'] == 1 ? 'inactive' : 'active';
				$details[] = '
					<a href="#" data-id="' . $val['class_id'] . '" data-action="edit" data-name="' . $val['name'] . '" data-toggle="tooltip" data-placement="top" title="Edit Class"><span class="glyphicon glyphicon-pencil"></span></a>&emsp;
					<a href="#" data-id="' . $val['class_id'] . '" data-action="' . $change_status_action . '" data-toggle="tooltip" data-placement="top" title="' . $change_status_text . '"><span class="glyphicon ' . $change_status_icon . ' ' . $change_status_class . '"></span></a>
				';
				$formatted_data[] = $details;
			}

			$json_data = array(
				'draw' => 1,
				'recordsTotal' => count($data),
				'recordsFiltered' => count($data),
				'data' => $formatted_data
			);
			die(json_encode($json_data));
		}
	}

	// Get Class Dropdown
	function class_get_dropdown() {

		$data = $this->School_model->get_classes();
		die(json_encode($data));
	}

	// Create Class
	function class_create() {

		$data = array();

		// Validate if Class Name exists
		$valid = $this->School_model->validate_class_name_exists(mysql_real_escape_string($this->input->post('name')));
		if(!$valid) {
			$data = array(
				'result' => false,
				'message' => 'Class Name already exists'
			);
			die(json_encode($data));
		}

		// Insert Data
		$this->School_model->insert_class();
		$data = array(
			'result' => true
		);
		die(json_encode($data));
	}

	// Update Class
	function class_update() {

		$data = array();

		// Validate if Class Name exists
		$valid = $this->School_model->validate_class_name_exists(mysql_real_escape_string($this->input->post('name')), $this->input->post('class_id'));
		if(!$valid) {
			$data = array(
				'result' => false,
				'message' => 'Class Name already exists'
			);
			die(json_encode($data));
		}

		// Update Data
		$this->School_model->update_class();
		$data = array(
			'result' => true
		);
		die(json_encode($data));
	}

	// Update Class Status
	function class_update_status() {

		// Update Data
		$this->School_model->update_class_status();
		$data = array(
			'result' => true
		);
		die(json_encode($data));
	}

	// Get Teachers
	function teacher_get() {

		// Check if there's a teacher id
		if($this->input->post('teacher_id')) {
			$data = $this->School_model->get_teachers($this->input->post('teacher_id'));
			die(json_encode($data[0]));
		}
		else {
			$data = $this->School_model->get_teachers();
			$formatted_data = array();

			foreach($data as $key => $val) {
				$details = array();
				foreach($val as $kCol => $vVal) {
					if(in_array($kCol, array('firstname', 'surname', 'username', 'password', 'classes', 'teacher_status'))) {
						if($kCol == 'classes') {
							$details[] = '<span class="ellipsis teacher" data-toggle="tooltip" data-placement="top" title="' . $vVal . '">' . $vVal . '<span>';
						}
						else if($kCol == 'teacher_status') {
							$text = $vVal == 1 ? 'Active' : 'Inactive';
							$class = $vVal == 1 ? 'active' : 'inactive';
							$details[] = '<span class="' . $class . '">' . $text . '</span>';
						}
						else {
							$details[] = $vVal;
						}
					}
				}
				$change_status_icon = $val['teacher_status'] == 1 ? 'glyphicon-remove' : 'glyphicon-ok';
				$change_status_text = $val['teacher_status'] == 1 ? 'Deactive Teacher Account' : 'Activate Teacher Account';
				$change_status_action = $val['teacher_status'] == 1 ? 'deactivate' : 'activate';
				$change_status_class = $val['teacher_status'] == 1 ? 'inactive' : 'active';
				$details[] = '
					<a href="#" data-id="' . $val['teacher_id'] . '" data-action="edit" data-name="' . $val['firstname'] . ' ' . $val['surname'] . '" data-toggle="tooltip" data-placement="top" title="Edit Teacher"><span class="glyphicon glyphicon-pencil"></span></a>&emsp;
					<a href="#" data-id="' . $val['teacher_id'] . '" data-action="' . $change_status_action . '" data-toggle="tooltip" data-placement="top" title="' . $change_status_text . '"><span class="glyphicon ' . $change_status_icon . ' ' . $change_status_class . '"></span></a>
				';
				$formatted_data[] = $details;
			}

			$json_data = array(
				'draw' => 1,
				'recordsTotal' => count($data),
				'recordsFiltered' => count($data),
				'data' => $formatted_data
			);
			die(json_encode($json_data));
		}
	}

	// Create Teacher
	function teacher_create() {

		$data = array();

		// Validate if Teacher Name exists
		$valid = $this->School_model->validate_teacher_name_exists(mysql_real_escape_string($this->input->post('firstname')), mysql_real_escape_string($this->input->post('surname')));
		if(!$valid) {
			$data = array(
				'result' => false,
				'message' => 'Teacher Name already exists'
			);
			die(json_encode($data));
		}

		// Validate if Classes are Taken
		$validation_data = $this->School_model->validate_classes_are_taken($this->input->post('classes'));
		if(!$validation_data['valid']) {
			$data = array(
				'result' => false,
				'message' => 'Classes [' . implode(', ', $validation_data['classes_taken']) . '] are already assigned'
			);
			die(json_encode($data));
		}

		// Insert Data
		$this->School_model->insert_teacher();
		$data = array(
			'result' => true
		);
		die(json_encode($data));
	}

	// Update Teacher
	function teacher_update() {

		$data = array();

		// Validate if Teacher Name exists
		$valid = $this->School_model->validate_teacher_name_exists(mysql_real_escape_string($this->input->post('firstname')), mysql_real_escape_string($this->input->post('surname')), $this->input->post('teacher_id'));
		if(!$valid) {
			$data = array(
				'result' => false,
				'message' => 'Teacher Name already exists'
			);
			die(json_encode($data));
		}

		// Validate if Classes are Taken
		$validation_data = $this->School_model->validate_classes_are_taken($this->input->post('classes'), $this->input->post('teacher_id'));
		if(!$validation_data['valid']) {
			$data = array(
				'result' => false,
				'message' => 'Classes [' . implode(', ', $validation_data['classes_taken']) . '] are already assigned'
			);
			die(json_encode($data));
		}

		// Update Data
		$this->School_model->update_teacher();
		$data = array(
			'result' => true
		);
		die(json_encode($data));
	}

	// Update Teacher Status
	function teacher_update_status() {

		// Update Data
		$this->School_model->update_teacher_status();
		$data = array(
			'result' => true
		);
		die(json_encode($data));
	}

	// Get Students
	function student_get() {

		// Check if there's a student id
		if($this->input->post('student_id')) {
			$data = $this->School_model->get_students($this->input->post('student_id'));
			die(json_encode($data[0]));
		}
		else {
			$data = $this->School_model->get_students();
			$formatted_data = array();

			foreach($data as $key => $val) {
				$details = array();
				foreach($val as $kCol => $vVal) {
					if(in_array($kCol, array('firstname', 'surname', 'class_name', 'username', 'password', 'student_status'))) {
						if($kCol == 'student_status') {
							$text = $vVal == 1 ? 'Active' : 'Inactive';
							$class = $vVal == 1 ? 'active' : 'inactive';
							$details[] = '<span class="' . $class . '">' . $text . '</span>';
						}
						else {
							$details[] = $vVal;
						}
					}
				}
				$change_status_icon = $val['student_status'] == 1 ? 'glyphicon-remove' : 'glyphicon-ok';
				$change_status_text = $val['student_status'] == 1 ? 'Deactive Student Account' : 'Activate Student Account';
				$change_status_action = $val['student_status'] == 1 ? 'deactivate' : 'activate';
				$change_status_class = $val['student_status'] == 1 ? 'inactive' : 'active';
				$details[] = '
					<a href="#" data-id="' . $val['student_id'] . '" data-action="edit" data-name="' . $val['firstname'] . ' ' . $val['surname'] . '" data-toggle="tooltip" data-placement="top" title="Edit Student"><span class="glyphicon glyphicon-pencil"></span></a>&emsp;
					<a href="#" data-id="' . $val['student_id'] . '" data-action="' . $change_status_action . '" data-toggle="tooltip" data-placement="top" title="' . $change_status_text . '"><span class="glyphicon ' . $change_status_icon . ' ' . $change_status_class . '"></span></a>
				';
				$formatted_data[] = $details;
			}

			$json_data = array(
				'draw' => 1,
				'recordsTotal' => count($data),
				'recordsFiltered' => count($data),
				'data' => $formatted_data
			);
			die(json_encode($json_data));
		}
	}

	// Create Student
	function student_create() {

		$data = array();

		// Validate if Student Name exists
		$valid = $this->School_model->validate_student_name_exists(mysql_real_escape_string($this->input->post('firstname')), mysql_real_escape_string($this->input->post('surname')));
		if(!$valid) {
			$data = array(
				'result' => false,
				'message' => 'Student Name already exists'
			);
			die(json_encode($data));
		}

		// Validate if Student Username exists
		$valid = $this->School_model->validate_student_username_exists(mysql_real_escape_string($this->input->post('username')));
		if(!$valid) {
			$data = array(
				'result' => false,
				'message' => 'Student Username already exists'
			);
			die(json_encode($data));
		}

		// Insert Data
		$this->School_model->insert_student();
		$data = array(
			'result' => true
		);
		die(json_encode($data));
	}

	// Update Student
	function student_update() {

		$data = array();

		// Validate if Student Name exists
		$valid = $this->School_model->validate_student_name_exists(mysql_real_escape_string($this->input->post('firstname')), mysql_real_escape_string($this->input->post('surname')), $this->input->post('student_id'));
		if(!$valid) {
			$data = array(
				'result' => false,
				'message' => 'Student Name already exists'
			);
			die(json_encode($data));
		}

		// Validate if Student Username exists
		$valid = $this->School_model->validate_student_username_exists(mysql_real_escape_string($this->input->post('username')), $this->input->post('student_id'));
		if(!$valid) {
			$data = array(
				'result' => false,
				'message' => 'Student Username already exists'
			);
			die(json_encode($data));
		}

		// Update Data
		$this->School_model->update_student();
		$data = array(
			'result' => true
		);
		die(json_encode($data));
	}

	// Update Student Status
	function student_update_status() {

		// Update Data
		$this->School_model->update_student_status();
		$data = array(
			'result' => true
		);
		die(json_encode($data));
	}

	// Admin Authorize
	function authorize() {
		if(!$this->session->userdata('login')){
			redirect('site/login');
		}
		else {
			// Check if not School Account
			// Or Check if School Account User is not admin
			if(!$this->session->userdata('school_account') || $this->session->userdata('school_user') != 'admin') {
				redirect('');
			}
		}
	}
}