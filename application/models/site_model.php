<?php
Class Site_model extends CI_Model
{

	var $details;

	function __construct()
	{
		parent::__construct();
		ini_set('date.timezone', 'Europe/London');
	}

	function loginprocess($email,$password)
	{
		// Check in User Accounts
		$this->db->from('user_accounts');
		$this->db->where('user_email', $email);
		$this->db->where('password', md5($password));
		$this->db->where('status', 1);
		$login = $this->db->get()->result();
		if ( is_array($login) && count($login) == 1 ) {
			$this->details = $login[0];
			$this->set_session();
			return array(
				'result' => true,
				'link' => base_url() . 'account/dashboard'
			);
		}

		// Check in School Admin Accounts
		$this->db->from('school_list');
		$this->db->where('email_address', $email);
		$this->db->where('password', md5($password));
		$this->db->where('status', 1);
		$login = $this->db->get()->result_array();
		if(is_array($login) && count($login) == 1) {
			$this->details = $login[0];
			$this->set_school_account_session();
			return array(
				'result' => true,
				'link' => base_url() . 'school'
			);
		}

		// Check in School Teacher Accounts
		$this->db->from('view_teacher_details_list');
		$this->db->where('username', $email);
		$this->db->where('password', $password);
		// $this->db->where('password', md5($password));
		$this->db->where('teacher_status', 1);
		$login = $this->db->get()->result_array();
		if(is_array($login) && count($login) == 1) {
			$this->details = $login[0];
			$this->set_teacher_account_session();
			return array(
				'result' => true,
				'link' => base_url() . 'teacher'
			);
		}

		// Check in School Student Accounts
		$this->db->from('view_student_class_details_list');
		$this->db->where('username', $email);
		$this->db->where('password', $password);
		// $this->db->where('password', md5($password));
		$this->db->where('student_status', 1);
		$login = $this->db->get()->result_array();
		if(is_array($login) && count($login) == 1) {
			$this->details = $login[0];
			$this->set_student_account_session();
			return array(
				'result' => true,
				'link' => base_url() . 'account/dashboard'
			);
		}

		return array(
			'result' => false
		);
	}

	function set_session() {
		$this->session->set_userdata(
			array(
				'id'           => $this->details->account_id,
				'email'        => $this->details->user_email,
				'firstname'    => $this->details->first_name,
				'lastname'     => $this->details->surname,
				'acctype'      => $this->details->account_type,
				'account_name' => $this->details->user_email,
				'login'        => true
			)
		);
	}

	function set_school_account_session() {
		$this->session->set_userdata(
			array(
				'school_account' => true,
				'school_user'    => 'admin',
				'school_id'      => $this->details['school_id'],
				'school_name'    => $this->details['name'],
				'email_address'  => $this->details['email_address'],
				'account_name'   => $this->details['email_address'],
				'login'          => true
			)
		);
	}

	function set_teacher_account_session() {
		$this->session->set_userdata(
			array(
				'school_account' => true,
				'school_user'    => 'teacher',
				'school_id'      => $this->details['school_id'],
				'school_name'    => $this->details['school_name'],
				'teacher_id'     => $this->details['teacher_id'],
				'firstname'      => $this->details['firstname'],
				'surname'        => $this->details['surname'],
				'account_name'   => $this->details['surname'] . ', ' . $this->details['firstname'],
				'login'          => true
			)
		);
	}

	function set_student_account_session() {
		$this->session->set_userdata(
			array(
				'school_account' => true,
				'school_user'    => 'student',
				'school_id'      => $this->details['school_id'],
				'school_name'    => $this->details['school_name'],
				'class_id'       => $this->details['class_id'],
				'class_name'     => $this->details['class_name'],
				'student_id'     => $this->details['student_id'],
				'firstname'      => $this->details['firstname'],
				'surname'        => $this->details['surname'],
				'account_name'   => $this->details['surname'] . ', ' . $this->details['firstname'],
				'login'          => true
			)
		);
	}

	function is_email_exist($email)
	{
		$this->db->select("count(*) as alldata");
		$this->db->from("user_accounts");
		$this->db->where("user_email", $email);
		$this->db->where("payment_status", "COMPLETE");
		$this->db->where("status", 1);
		$res = $this->db->get()->result();
		$total = $res[0]->alldata;

		if($total > 0){
			return true;
		}else{
			return false;
		}
	}

	function paidregistration()
	{
		parse_str($_POST['datax'],$data);

		#check the email is already exist and activated
		if(!filter_var($data['email'], FILTER_VALIDATE_EMAIL)){
			$datas = array("success"=> false, "msg" => "Email is not valid, please enter a valid email address.");
			die(json_encode($datas));
		}

		if($this->is_email_exist($data['email'])){
			$datas = array("success"=> false, "msg" => "Email is already not available. Please use another email.");
			die(json_encode($datas));
		}

		$item_num = time();

		$datax = array(
			'item_number_ref' 	=> $item_num,
			'user_email' 		=> $data['email'],
			'password' 			=> md5($data['pass']),
			'first_name' 		=> $data['fname'],
			'surname' 			=> $data['sname'],
			'nationality' 		=> $data['nationality'],
			'account_type' 		=> 'PAID-ACCOUNT',
			'payment_status' 	=> 'NOT-COMPLETE',
			'status' 			=> 0,
			'date_added' 		=> date("Y-m-d H:i:s")
		);

		$this->db->insert("user_accounts", $datax);

		# set paypal payment link
		//$company_email = "company@passuktest.co.uk";

		$company_email = "subscriptions@passuktest.co.uk";

		$item_amount = 20;
		$return_url = base_url()."site/success_subscription";
		$cancel_url = base_url()."site/cancel_subscription";

		$querystring  = "?business=".urlencode($company_email)."&";
		$querystring .= "image_url=".urlencode('http://passuktest.co.uk/assets/images/passuktest-paypal.jpg')."&";
		$querystring .= "item_name=".urlencode('passUktest SUBSCRIPTION')."&";
		$querystring .= "amount=".urlencode($item_amount)."&";

		$querystring .= "cmd=". urlencode($data['cmd'])."&";
		$querystring .= "no_note=". urlencode($data['no_note'])."&";
		$querystring .= "lc=". urlencode($data['lc'])."&";
		$querystring .= "currency_code=". urlencode($data['currency_code'])."&";
		$querystring .= "bn=". urlencode($data['bn'])."&";
		$querystring .= "first_name=". urlencode($data['fname'])."&";
		$querystring .= "last_name=". urlencode($data['sname'])."&";
		$querystring .= "item_number=". urlencode($item_num)."&";

		$querystring .= "return=".urlencode(stripslashes($return_url))."&";
		$querystring .= "cancel_return=".urlencode(stripslashes($cancel_url));

		# check if the site is in DEV and change the paypal link
		$a = base_url();
		if (strpos($a,'dev') !== false) {
			$paypal_link = "https://www.sandbox.paypal.com/cgi-bin/webscr".$querystring;
		}else{
			$paypal_link = "https://www.paypal.com/cgi-bin/webscr".$querystring;
		}

		$datas = array("success"=> true, "link" => $paypal_link);
		die(json_encode($datas));
	}

	function upgrade_account()
	{
		parse_str($_POST['datax'],$data);

		#get accoutn info for paypal processing
		$info = $this->get_account_info($this->session->userdata('id'));

		# set paypal payment link
		//$company_email = "company@passuktest.co.uk";

		$company_email = "subscriptions@passuktest.co.uk";

		$item_amount = 5;
		$return_url = base_url()."site/success_subscription";
		$cancel_url = base_url()."site/cancel_subscription";

		$querystring  = "?business=".urlencode($company_email)."&";
		$querystring .= "image_url=".urlencode('http://wagona.com/dev/assets/images/paypal.jpg')."&";
		$querystring .= "item_name=".urlencode('Wagona SUBSCRIPTION')."&";
		$querystring .= "amount=".urlencode($item_amount)."&";

		$querystring .= "cmd=". urlencode($data['cmd'])."&";
		$querystring .= "no_note=". urlencode($data['no_note'])."&";
		$querystring .= "lc=". urlencode($data['lc'])."&";
		$querystring .= "currency_code=". urlencode($data['currency_code'])."&";
		$querystring .= "bn=". urlencode($data['bn'])."&";
		$querystring .= "first_name=". urlencode($info->first_name)."&";
		$querystring .= "last_name=". urlencode($info->surname)."&";
		$querystring .= "item_number=". urlencode($info->item_number_ref)."&";

		$querystring .= "return=".urlencode(stripslashes($return_url))."&";
		$querystring .= "cancel_return=".urlencode(stripslashes($cancel_url));

		# check if the site is in DEV and change the paypal link
		$a = base_url();
		if (strpos($a,'dev') !== false) {
			$paypal_link = "https://www.sandbox.paypal.com/cgi-bin/webscr".$querystring;
		}else{
			$paypal_link = "https://www.paypal.com/cgi-bin/webscr".$querystring;
		}

		$datas = array("success"=> true, "link" => $paypal_link);
		die(json_encode($datas));
	}

	function get_account_info($accid)
	{
		$this->db->select("*");
		$this->db->from("user_accounts");
		$this->db->where("account_id", $accid);
		$res = $this->db->get()->result();

		return $res[0];
	}

	function ipn($data)
	{
		$datax = array(
			'item_num' 			=> $data['item_number'],
			'txn_id' 			=> $data['txn_id'],
			'txn_type' 			=> $data['txn_type'],
			'item_name' 		=> $data['item_name'],
			'mc_currency' 		=> $data['mc_currency'],
			'mc_gross' 			=> $data['mc_gross'],
			'payment_date' 		=> $data['payment_date'],
			'payment_status' 	=> $data['payment_status'],
			'payer_email' 		=> $data['payer_email'],
			//'data_array' 		=> $data,
			'date_inserted' 	=> date("Y-m-d H:i:s")
		);

		$this->db->insert("payment_records", $datax);

		#update usser account table for payment status complete
		$updata = array(
			'account_type' 	=> 'PAID-ACCOUNT',
			'payment_status' => 'COMPLETE',
			'status' => 1
		);
		$this->db->where("item_number_ref",$data['item_number']);
		$this->db->update("user_accounts",$updata);

		$userdata = $this->get_user_data($data['item_number']);

		$this->email_confirm($userdata->first_name, $data['mc_gross'], $userdata->user_email);
	}

	function get_user_data($iten_num)
	{
		$this->db->select("*");
		$this->db->from("user_accounts");
		$this->db->where("item_number_ref",$iten_num);
		$res = $this->db->get()->result();

		return $res[0];
	}

	function email_confirm($fname,$payment,$email)
	{
		$this->load->library('email');
		$config['protocol'] = 'sendmail';
		$config['mailpath'] = '/usr/sbin/sendmail';
		$config['charset'] = 'iso-8859-1';
		$config['wordwrap'] = TRUE;
		$config['mailtype'] = 'html';

		$this->email->initialize($config);

		$html = "<p>Dear ".$fname.",</p>";
		$html .= "<p>Thank you for your PayPal payment of $".$payment."  to passUKtest.</p>";
		$html .= "<p>You can now begin to prepare for your life in the UK test. Follow this link  and get started today;
				 <a href='".base_url()."site/login'>www.passuktest.co.uk</a>. If you can't click on the link, simply copy and paste it into your browser address bar.</p>";

		$html .= "<p>If you have any queries please contact us on subscriptions@passuktest.co.uk</p>";
		$html .= "<p>Wish you all the best with the life in the UK test!</p><br />";
		$html .= "<p><strong>passUKtest team</strong><br />
					34 New House<br />
					67-68 Hatton Garden<br />
					LONDON, EC1N 8JY<br />
					United Kingdom</p><br />";
		$html .= "<p style='font-size: 10px;'><strong>Breach of confidentiality & accidental breach of confidentiality</strong></p>";
		$html .= "<p style='font-size: 10px;'>This email and any files transmitted with it are confidential and intended solely for the use of the individual or entity to whom they are addressed. If you have received this email in error please notify the system manager. This message contains confidential information and is intended only for the individual named. If you are not the named addressee you should not disseminate, distribute or copy this e-mail. Please notify the sender immediately by e-mail if you have received this e-mail by mistake and delete this e-mail from your system. If you are not the intended recipient you are notified that disclosing, copying, distributing or taking any action in reliance on the contents of this information is strictly prohibited.</p>";

		$this->email->from('subscriptions@passuktest.co.uk', 'PassUkTest');
		$this->email->to($email);
		$this->email->bcc('johnpaulclam.blog@gmail.com');

		$this->email->subject('Thank You');
		$this->email->message($html);

		$this->email->send();
	}

	function freeregistration()
	{
		parse_str($_POST['datax'],$data);

		#check the email is already exist and activated
		if(!filter_var($data['email'], FILTER_VALIDATE_EMAIL)){
			$datas = array("success"=> false, "msg" => "Email is not valid, please enter a valid email address.");
			die(json_encode($datas));
		}

		if($this->is_email_exist($data['email'])){
			$datas = array("success"=> false, "msg" => "Email is already not available. Please use another email.");
			die(json_encode($datas));
		}

		$item_num = time();

		$datax = array(
			'item_number_ref' 	=> $item_num,
			'user_email' 		=> $data['email'],
			'password' 			=> md5($data['pass']),
			'first_name' 		=> $data['fname'],
			'surname' 			=> $data['sname'],
			'nationality' 		=> $data['nationality'],
			'account_type' 		=> 'FREE-ACCOUNT',
			'payment_status' 	=> 'NOT-COMPLETE',
			'status' 			=> 1,
			'date_added' 		=> date("Y-m-d H:i:s")
		);

		$this->db->insert("user_accounts", $datax);
		$userid = $this->db->insert_id();

		#insert free trial data record to test_trial_record db table
		$userx = array(
			'user_id' => $userid,
			'test_count' => 0
		);

		$this->db->insert("test_trial_record", $userx);

		$this->free_confirm($data['fname'], $data['email']);
		$datas = array("success"=> true, "link" => base_url()."site/login");
		die(json_encode($datas));
	}

	function free_confirm($fname,$email)
	{
		$this->load->library('email');
		$config['protocol'] = 'sendmail';
		$config['mailpath'] = '/usr/sbin/sendmail';
		$config['charset'] = 'iso-8859-1';
		$config['wordwrap'] = TRUE;
		$config['mailtype'] = 'html';

		$this->email->initialize($config);

		$link = "<p><a href='".base_url()."site/login'>Click here to activate.</a></p>";

		$html = "
			<html><header>
				<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700,300' rel='stylesheet' type='text/css'>
	    		<link href='http://fonts.googleapis.com/css?family=Oswald:400,300,700' rel='stylesheet' type='text/css'>
			</header><body>
			<div style='width: 600px; margin: auto;'>
			<h1 style='text-align: center;'><strong><img src='".base_url()."assets/images/logo.png' alt='logo' width='143' height='106' /></strong></h1>
			<p>Dear ".$fname."</p>
			<p>Thank you for signing up to see WagonaMaths in action! Your FREE trial registration is almost complete!</p>
			<p><strong>To activate your FREE trial please click on the link below:</strong></p>
			".$link."
			<br />
			<p style='color: #EC6429;'><strong>The Top 3 Reasons to try WagonaMaths for free</strong></p>
			<p style='color: #EC6429;'><strong>1. WagonaMaths e-learning tool is fully diagnostic and is an effective tool for every school child</strong></p>
			<p style='color: #EC6429;'><strong>2. All the Questions and Answers are set by experienced experts and are of a very high quality</strong></p>
			<p style='color: #EC6429;'><strong>3. We keep you informed of the progress all the way through easy to understand reports</strong></p>
			<br />

			<p>WagonaMaths hopes that you enjoy the learning experience. If you have any questions, please send a email to subscriptions@wagona.com. WagonaMaths is here to facilitate you</p>
			<p>&nbsp;</p>
			<br />
			<p>Yours in Education,</p>
			<p align='left'>
			<strong>WagonaMaths Team</strong><br />
			34 New House<br />
			67-68 Hatton Garden<br />
			LONDON, EC1N 8JY<br />
			United Kingdom <br />
			</p>
			<br />
			<p style='font-size: 11px;'><strong>Breach of confidentiality & accidental breach of confidentiality</strong><br />
			This email and any files transmitted with it are confidential and intended solely for the use of the
			individual or entity to whom they are addressed. If you have received this email in error please notify
			the system manager. This message contains confidential information and is intended only for the individual
			named. If you are not the named addressee you should not disseminate, distribute or copy this e-mail.
			Please notify the sender immediately by e-mail if you have received this e-mail by mistake and delete this
			e-mail from your system. If you are not the intended recipient you are notified that disclosing, copying,
			distributing or taking any action in reliance on the contents of this information is strictly prohibited.
			</p>
			</div>
			</body>
			</html>
		";

		$this->email->from('subscriptions@wagona.com', 'Wagona');
		$this->email->to($email);
		$this->email->bcc('johnpaulclam.blog@gmail.com');

		$this->email->subject('FREE Trial Account Confirmation');
		$this->email->message($html);

		$this->email->send();
	}

	function send_my_message($data)
	{
		$this->load->library('email');
		$config['protocol'] = 'sendmail';
		$config['mailpath'] = '/usr/sbin/sendmail';
		$config['charset'] = 'iso-8859-1';
		$config['wordwrap'] = TRUE;
		$config['mailtype'] = 'html';

		$this->email->initialize($config);

		$html = "<p>Please see below support data below:</p>";
		$html .= "<p>Name: ".$data['name']."</p>";
		$html .= "<p>Email: ".$data['email']."</p>";
		$html .= "<p>Phone: ".$data['phone']."</p>";
		$html .= "<p>Subject: ".$data['subject']."</p>";
		$html .= "<p>Message:</p><hr />";
		$html .= "<p> ".$data['message']."</p>";

		$this->email->from($data['email']);
		$this->email->to('feedback@wagona.com');
		$this->email->bcc('ray@wagona.com');
		$this->email->bcc('rex@wagona.com');
		$this->email->bcc('farayi@wagona.com');

		$this->email->subject('Customer Contact Message');
		$this->email->message($html);

		$this->email->send();

		$datas = array("success"=> true);
		die(json_encode($datas));
	}

	function send_my_feedback($data)
	{
		$this->load->library('email');
		$config['protocol'] = 'sendmail';
		$config['mailpath'] = '/usr/sbin/sendmail';
		$config['charset'] = 'iso-8859-1';
		$config['wordwrap'] = TRUE;
		$config['mailtype'] = 'html';

		$this->email->initialize($config);

		$html = "<p>Please see below feedback:</p>";
		$html .= "<p>Name: ".$data['name']."</p>";
		$html .= "<p>Email: ".$data['email']."</p>";
		$html .= "<p>Message: ".$data['message']."</p>";

		// Feedback
		$html .= '<br>';
		foreach($data['feedback'] as $key => $val) {
			$html .= '<p>';
				$html .= '<span>' . $val['question'] . '</span>';
				$html .= '<ul>';
					$html .= '<li>Answer: ' . ($val['choice'] == '' ? '*No Answer Selected*' : $val['choice']) . '</li>';
					$html .= '<li>Comment: ' . ($val['comment'] == '' ? '*No Comment*' : $val['comment']) . '</li>';
				$html .= '</ul>';
			$html .= '</p>';
		}

		$this->email->from($data['email']);
		$this->email->to('feedback@wagona.com');
		$this->email->bcc('ray@wagona.com');
		$this->email->bcc('rex@wagona.com');
		$this->email->bcc('farayi@wagona.com');

		$this->email->subject('Customer Feedback');
		$this->email->message($html);

		$this->email->send();

		$datas = array("success"=> true);
		die(json_encode($datas));
	}
}
?>
