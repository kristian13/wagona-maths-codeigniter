<?php
Class Account_model extends CI_Model
{

	var $details;

	function __construct()
	{
		parent::__construct();
		ini_set('date.timezone', 'Europe/London');
	}

	function sequence_puller()
	{
		$test = array();
		$topics_no = 8;
		$topics = $this->get_all_topic();

		$test['id'] = $this->setup_test_header();
		$test['total'] = 0;
		for($x=0; $x<=($topics_no - 1); $x++){

			$test['topic'][$x]['id'] 	= $topics[$x]->topic_id;
			$test['topic'][$x]['desc'] 	= $this->get_topc_description($topics[$x]->topic_id);

			$sequence  = $this->get_current_sequence($this->session->userdata('id'),$topics[$x]->topic_id);
			$questions = $this->get_question_structure($topics[$x]->topic_id);

			# determine if the user take the first time test
			if($sequence == 0){

				$ref = 0;
				# run through the sequence structure of all questions in a particular topic
				for($z=0; $z<=(count($questions)-1); $z++){

					# process only the active questions
					if($questions[$z]->status == 1 && $ref < 3){
						$test['quest'][$topics[$x]->topic_id][$z] = $questions[$z];
						$lastquest = $questions[$z]->question_id;
						$ref++;

						#count the questions
						$test['total']++;
					}

					#loop around the sequence structure of questions untill the 3 questions already picked
					if($z == (count($questions)-1) && $ref < 3 ){
						$z = 0;
					}
				}

				#this is where you update your last sequence number for each topic
				$seq = array(
					'user_id' 				=> $this->session->userdata('id'),
					'topic_id' 				=> $topics[$x]->topic_id,
					'last_pulled_question' 	=> $lastquest
				);

				$this->db->insert("test_quiz_sequence", $seq);

			}
			else # determine the user take 2 or more tests
			{
				$ref = 0;
				$flag = 0;

				# run through the sequence structure of all questions in a particular topic
				for($z=0; $z<=(count($questions)-1); $z++){

					# signal the picker to start picking the 3 questions now
					if($questions[$z]->question_id == $sequence){
						$flag = 1;
					}

					# process only the active questions
					if($flag == 1 && $ref < 3 && $questions[$z]->question_id != $sequence && $questions[$z]->status == 1){
						$test['quest'][$topics[$x]->topic_id][$ref] = $questions[$z];
						$lastquest = $questions[$z]->question_id;
						$ref++;

						#count the questions
						$test['total']++;
					}

					#loop around the sequence structure of questions untill the 3 questions already picked
					if($z == (count($questions)-1) && $ref < 3 ){
						$z = 0;
					}
				}

				#this is where you update your last sequence number for each topic
				$seq = array(
					'user_id' 				=> $this->session->userdata('id'),
					'topic_id' 				=> $topics[$x]->topic_id,
					'last_pulled_question' 	=> $lastquest
				);

				$this->db->where("user_id", $this->session->userdata('id'));
				$this->db->where("topic_id", $topics[$x]->topic_id);
				$this->db->update("test_quiz_sequence", $seq);
			}


		}

		#update the test quiz header for how many total number of quetions all in all
		$this->update_testheader($test['id'], $test['total']);

		#record the current set of test
		$this->run_test_recorder($test);

		return $test;
	}

	function dyna_sequence($data)
	{
		//$xtopic = explode("_",$data);

		$test = array();
		$topics = $this->get_dyna_topic(str_replace("_",",",$data));
		$topics_no = count($topics);

		#assign test topic id & description
		for($t=0; $t<=($topics_no - 1); $t++){

			$test['topic'][$t]['id'] 	= $topics[$t]->topic_id;
			$test['topic'][$t]['desc'] 	= $topics[$t]->description;
			$test['topic'][$t]['counter'] = $t;
		}

		$test['id'] = $this->setup_test_header();
		$test['total'] = 0;
		#get 24 questions from all the topics
		$counter = 0;

		for($x=0; $x<24; $x++){


			$sequence  = $this->get_current_sequence($this->session->userdata('id'),$test['topic'][$counter]['id']);
			$questions = $this->get_question_structure($test['topic'][$counter]['id']);

			# determine if the user take the first time test
			if($sequence == 0){

				$ref = 0;
				$lastquest = 0;
				# run through the sequence structure of all questions in a particular topic
				for($z=0; $z<=(count($questions)-1); $z++){

					# process only the active questions
					if($questions[$z]->status == 1 && $ref < 1){
						$test['quest'][$test['topic'][$counter]['id']][$test['topic'][$counter]['counter']] = $questions[$z];
						$lastquest = $questions[$z]->question_id;
						$ref++;
						$test['topic'][$counter]['counter']++;

						#count the questions
						$test['total']++;
					}

					#loop around the sequence structure of questions untill the 1 question already picked
					if($z == (count($questions)-1) && $ref < 1 ){
						$z = 0;
					}
				}

				#this is where you update your last sequence number for each topic
				$seq = array(
					'user_id' 				=> $this->session->userdata('id'),
					'topic_id' 				=> $test['topic'][$counter]['id'],
					'last_pulled_question' 	=> $lastquest
				);

				$this->db->insert("test_quiz_sequence", $seq);

			}
			else
			{

				$ref = 0;
				$flag = 0;
				$lastquest = 0;
				# run through the sequence structure of all questions in a particular topic
				for($z=0; $z<=(count($questions)-1); $z++){

					# signal the picker to start picking the 1 question now
					if($questions[$z]->question_id == $sequence){
						$flag = 1;
					}

					# process only the active questions
					if($flag == 1 && $ref < 1 && $questions[$z]->question_id != $sequence && $questions[$z]->status == 1){
						$test['quest'][$test['topic'][$counter]['id']][$test['topic'][$counter]['counter']] = $questions[$z];
						$lastquest = $questions[$z]->question_id;
						$ref++;
						$test['topic'][$counter]['counter']++;

						#count the questions
						$test['total']++;
					}

					#loop around the sequence structure of questions untill the 3 questions already picked
					if($z == (count($questions)-1) && $ref < 1 ){
						$z = 0;
					}
				}

				#this is where you update your last sequence number for each topic
				$seq = array(
					'user_id' 				=> $this->session->userdata('id'),
					'topic_id' 				=> $test['topic'][$counter]['id'],
					'last_pulled_question' 	=> $lastquest
				);

				$this->db->where("user_id", $this->session->userdata('id'));
				$this->db->where("topic_id", $test['topic'][$counter]['id']);
				$this->db->update("test_quiz_sequence", $seq);
			}

			if($counter == ($topics_no - 1)){
				$counter = 0;
			}else{
				$counter++;
			}
		}


		#update the test quiz header for how many total number of quetions all in all
		$this->update_testheader($test['id'], $test['total']);

		#record the current set of test
		$this->run_test_recorder($test);

		return $test;
	}

	function get_dyna_topic($topic)
	{
		$xtopic = str_replace("on,","",$topic);

		$ftopic = $this->genererate_topic_sequence($xtopic);

		$sql = "select * from test_topic where topic_id in ($ftopic)";
		$query = $this->db->query($sql);

		return $query->result();
	}

	function genererate_topic_sequence($data)
	{
		$data_array = explode(',',$data);
		shuffle($data_array);

		// Check if Individual Account
		if($this->session->userdata('acctype')) {
			$user = $this->session->userdata('id');
			$user_sql = 'test_quiz_header.user_id = ' . $user;
		}
		// Or Check if School Student Account
		else if($this->session->userdata('school_account') && $this->session->userdata('school_user') == 'student') {
			$user = $this->session->userdata('student_id');
			$user_sql = 'test_quiz_header.student_id = ' . $user;
		}

		// Get topics with the least number of questions answered
		if(count($data_array) >= WG_MAX_NUM_QUESTIONS) {
			$topic_ids_sequence = array();
			$sql = '
				SELECT
					topic_list.topic_id,
					topic_list.description AS topic_name,
					topic_list.status AS topic_status,
					(
						SELECT COUNT(test_quiz_items.user_answer)
						FROM
							test_quiz_header
						LEFT JOIN
							test_quiz_items ON test_quiz_header.test_id = test_quiz_items.header_id
						WHERE
							' . $user_sql . '
							AND test_quiz_header.status = 1
							AND test_quiz_items.topic_id = topic_list.topic_id
					) AS num_question_answered
				FROM
					test_topic AS topic_list
				WHERE
					topic_list.topic_id IN(' . implode(', ', $data_array) . ')
				ORDER BY
					num_question_answered
				LIMIT ' . WG_MAX_NUM_QUESTIONS . '
			';
			$query = $this->db->query($sql);
			$result = $query->result_array();
			foreach($result as $k => $v) {
				$topic_ids_sequence[] = $v['topic_id'];
			}
			return implode(',', $topic_ids_sequence);
		}
		// Else, return the passed data
		else{
			return implode(',', $data_array);
		}
	}

	function get_all_subject()
	{
		$datax = array();
		$this->db->select("*");
		$this->db->from("test_subject");
		$this->db->where("status", 1);
		$this->db->order_by("subject_id", "asc");
		$res = $this->db->get()->result();

		foreach($res as $key => $val){
			// Get Tests
			$test_subjects = $this->get_tests_subject($val->subject_id);

			// Get Topics
			$sql_topics = '
				SELECT topic_id
				FROM test_topic
				WHERE
					subject_id = ' . $val->subject_id . '
					AND status = 1
				ORDER BY
					topic_id
			';
			$query_topics = $this->db->query($sql_topics);
			$result_topics = $query_topics->result();
			$running_subject_score = 0;
			$running_subject_count = 0;
			foreach($result_topics as $kTopic => $vTopic) {
				$topic_details = $this->average_topic($vTopic->topic_id);
				if($topic_details['topquest'] >= WG_LEAST_COLOR_CODING_NUM) {
					$running_subject_score += $topic_details['score'];
					$running_subject_count++;
				}
			}

			$details = array(
				'subject_id' => $val->subject_id,
				'description' => $val->description,
				'tests' => $test_subjects['tests'],
				// 'average' => $test_subjects['average']
				'average' => $running_subject_count >= WG_LEAST_SUBJECT_TOPICS_AVERAGE_NUM ? round($running_subject_score / $running_subject_count) . '%' : '--'
			);

			$datax[] = $details;
		}

		return $datax;
	}

	function get_tests_subject($subjectid)
	{
		$datax = array();

		// Check if Individual Account
		if($this->session->userdata('acctype')) {
			$user = $this->session->userdata('id');
			$user_sql = 'a.user_id = ' . $user;
		}
		// Or Check if School Student Account
		else if($this->session->userdata('school_account') && $this->session->userdata('school_user') == 'student') {
			$user = $this->session->userdata('student_id');
			$user_sql = 'a.student_id = ' . $user;
		}
		else {
			return array();
		}

		$SQL = "
				SELECT
				d.subject_id,
				a.test_id,
				a.score_percent,
				d.description as subjects,
				c.description as topics,
				a.date_created
				FROM test_quiz_header a
				left join test_quiz_items b on b.header_id = a.test_id
				left join test_topic c on c.topic_id = b.topic_id
				left join test_subject d on d.subject_id = c.subject_id
				where d.subject_id = ".$subjectid."
				and a.status = 1
				and " . $user_sql . "
				GROUP BY b.header_id
				order by a.test_id desc";

		$query = $this->db->query($SQL);
		$data = $query->result();
		$count = count($data);
		$limit = 0;
		$running_score = 0;
		$test_count = 0;
		if(count($data) > 0 && count($data) >= 5){
			foreach($data as $key => $val){

				if($limit < 5){
					$score = round($val->score_percent);
					$running_score += $score;
					$datax[] = array(
						'test' => 'Test '.$count.': '.date("d/m/y", strtotime($val->date_created)),
						'score' => $score . '%',
						'tcount' => $count,
						'test_id' => $val->test_id
					);
					$count--;
					$limit++;
					$test_count++;
				}
			}
		}

		else if(count($data) > 0 && count($data) < 5){

			foreach($data as $key => $val){
					$score = round($val->score_percent);
					$running_score += $score;
					$limit++;
					$datax[] = array(
						'test' => 'Test '.$limit.': '.date("d/m/y", strtotime($val->date_created)),
						'score' => $score . '%',
						'tcount' => $limit,
						'test_id' => $val->test_id
					);
					$count--;
					$test_count++;


			}

			if($limit < 5){

				for($lm=$limit; $lm<5; $lm++){
					$limit++;
					$datax[] = array(
						'test' => 'Test '.$limit.': Pending',
						'score' => '--',
						'tcount' => $limit
					);

				}

			}
		}

		else{

			for($x=1; $x<=5; $x++){

				$datax[] = array(
						'test' => 'Test '.$x.': Pending',
						'score' => '--',
						'tcount' => $x
					);

					$count--;
					$limit++;
				}

		}

		$data = array(
			'tests' => $datax,
			'average' => $test_count > 0 ? round($running_score / $test_count) . '%' : (count($data) > 0 ? $test_count . '%' : '--')
		);
		return $data;
	}

	function get_check_topics($data)
	{
		$this->db->select("*");
		$this->db->from("test_topic");
		$this->db->where("subject_id", $data['subid']);
		$this->db->where("status", 1);
		$this->db->order_by("topic_id", "asc");
		$res = $this->db->get()->result();
		if(count($res) > 0){


			//$html = '<p style="border-bottom: solid 2px #ccc; padding-bottom: 5px; font-weight: bold;"><input type="checkbox" name="select_all" id="select_all" onClick="selectAllcheck()"> Select All</p>';
			$html = '';
			foreach($res as $key => $val){
				$topic = $this->average_topic($val->topic_id);

				$html .= '<div class="checkbox-row ' . helper_get_score_class($topic['score'], $topic['topquest']) . '">';
	        		$html .= '<label>';
	        			$html .= '<table style="width: 100%;">';
	        				$html .= '<tbody>';

	        					$html .= '<tr>';

	        					// if($this->session->userdata('acctype') == 'PAID-ACCOUNT'){
	        					// 	$html .= '<td width="50%"><input type="checkbox" name="topchoice[]" id="top_'.$val->topic_id.'" class="checkbox1" value='.$val->topic_id.'> '.str_replace(".","",preg_replace('/[0-9]+/', '', $val->description)).'</td>';
        						// }

        						// if($this->session->userdata('acctype') != 'PAID-ACCOUNT' && $val->is_paid == 0){
	        					// 	$html .= '<td width="50%"><input type="checkbox" name="topchoice[]" id="top_'.$val->topic_id.'" class="checkbox1" value='.$val->topic_id.'> '.str_replace(".","",preg_replace('/[0-9]+/', '', $val->description)).'</td>';
        						// }
        						// if($this->session->userdata('acctype') != 'PAID-ACCOUNT' && $val->is_paid == 1){
	        					// 	$html .= '<td width="50%"><input type="checkbox" name="topchoice[]" id="top_'.$val->topic_id.'" class="checkbox1" value='.$val->topic_id.' disabled> '.str_replace(".","",preg_replace('/[0-9]+/', '', $val->description)).'</td>';
        						// }

								if($this->session->userdata('acctype') == 'FREE-ACCOUNT' && $val->is_paid == 1) {
									$html .= '<td width="50%"><input type="checkbox" name="topchoice[]" id="top_'.$val->topic_id.'" class="checkbox1" value='.$val->topic_id.' disabled> '.str_replace(".","",preg_replace('/[0-9]+/', '', $val->description)).'</td>';
								}
								else {
									$html .= '<td width="50%"><input type="checkbox" name="topchoice[]" id="top_'.$val->topic_id.'" class="checkbox1" value='.$val->topic_id.'> '.str_replace(".","",preg_replace('/[0-9]+/', '', $val->description)).'</td>';
								}
									$html .= '<td style="text-align: right; width:12.5%">'.$topic['topquest'].'</td>';
	        						$html .= '<td style="text-align: right; width:12.5%">'.$topic['check'].'</td>';
	        						$html .= '<td style="text-align: right; width:12.5%">'.$topic['wrong'].'</td>';
	        						$html .= '<td style="text-align: right; width:12.5%">'.$topic['percentage'].'</td>';
	        					$html .= '</tr>';

	        				$html .= '</tbody>';
	        			$html .= '</table>';
	        		$html .= '</label>';
	        	$html .= '</div>';
			}
		}else{
			$html = '';
		}

		die(json_encode(array("success" => true, "checktopic" => $html)));
	}

	function average_topic($topicid)
	{
		// Check if Individual Account
		if($this->session->userdata('acctype')) {
			$user = $this->session->userdata('id');
			$user_sql = 'test_quiz_header.user_id = ' . $user;
		}
		// Or Check if School Student Account
		else if($this->session->userdata('school_account') && $this->session->userdata('school_user') == 'student') {
			$user = $this->session->userdata('student_id');
			$user_sql = 'test_quiz_header.student_id = ' . $user;
		}
		else {
			return array();
		}

		$sql = '
			SELECT
				test_quiz_items.quest_answer,
				test_quiz_items.user_answer
			FROM
				test_quiz_header
			LEFT JOIN
				test_quiz_items ON test_quiz_header.test_id = test_quiz_items.header_id
			WHERE
				' . $user_sql . '
				AND test_quiz_header.status = 1
				AND test_quiz_items.topic_id = ' . $topicid . '
		';

		$query = $this->db->query($sql);
		$data = $query->result();
		$score = 0;

		$items = count($data);
		$correct = 0;
		$wrong = 0;
		foreach($data as $key => $val){
			if($val->quest_answer == $val->user_answer){
				$correct++;
			}else{
				$wrong++;
			}
		}

		if($items == 0){
			$questcount = '-';
		}else{
			$questcount = $items;
		}

		if($correct == 0 && $items == 0){
			$check = '-';
		}else{
			$check = $correct;
		}

		if($wrong == 0 && $items == 0){
			$notcheck = '-';
		}else{
			$notcheck = $wrong;
		}

		if($correct == 0 && $items == 0){
			$percent = "-";
		}else{
			// $score = round(number_format((($correct / $items) * 100), 2, '.', ''));
			// if($questcount >= WG_LEAST_COLOR_CODING_NUM) {
			// 	$percent = '<span class="badge percenter ' . helper_get_score_class($score) . '">'. $score . '%</span>';
			// }
			// else {
			// 	$percent = '<span class="badge percenter score-black-bg">'. $score . '%</span>';
			// }

			$sql .= '
				ORDER BY
					test_quiz_header.date_created DESC,
					test_quiz_items.item_id DESC
				-- LIMIT ' . WG_LEAST_COLOR_CODING_NUM . '
			';
			$num_all =  $this->db->query($sql)->num_rows();

			$correct_stmt = 'A.quest_answer = A.user_answer';
			$wrong_stmt = 'A.quest_answer != A.user_answer';

			$sql_correct = '
				SELECT *
				FROM(' . $sql . ') AS A
				WHERE ' . $correct_stmt . '
			';
			$num_correct = $this->db->query($sql_correct)->num_rows();

			$sql_wrong = '
				SELECT *
				FROM(' . $sql . ') AS A
				WHERE ' . $wrong_stmt . '
			';
			$num_wrong = $this->db->query($sql_wrong)->num_rows();

			$score = round((($num_correct / $num_all) * 100));
			$percent = '<span>'. $score . '%</span>';
		}


		$data = array();
		$data['score'] = $score;
		$data['percentage'] = $percent;
		$data['check'] = $check;
		$data['wrong'] = $notcheck;
		$data['topquest'] = $questcount;

		return $data;
	}

	function get_all_topic()
	{
		$this->db->select("*");
		$this->db->from("test_topic");
		$this->db->where("status", 1);
		$this->db->order_by("topic_id", "asc");
		$res = $this->db->get()->result();

		return $res;
	}

	function get_current_sequence($userid,$topicid)
	{
		$num = 0;

		$this->db->select("last_pulled_question");
		$this->db->from("test_quiz_sequence");
		$this->db->where("user_id", $userid);
		$this->db->where("topic_id", $topicid);
		$res = $this->db->get()->result();

		if(count($res) > 0){
			return $res[0]->last_pulled_question;
		}else{
			return $num;
		}
	}

	function get_question_structure($topicid)
	{
		$data = array();
		$index = 0;

		$this->db->select("*");
		$this->db->from("test_question");
		$this->db->where("topic_id", $topicid);
		$this->db->order_by("question_id", "asc");
		$res = $this->db->get()->result();

		# loop1 for round array sequence
		for($x=0; $x<=(count($res) - 1); $x++)
		{
			$data[$index] = $res[$x];
			$index++;
		}

		return $data;

	}

	function setup_test_header()
	{
		// Check if Individual Account
		if($this->session->userdata('acctype')) {
			$user = $this->session->userdata('id');
			$column = 'user_id';
		}
		// Or Check if School Student Account
		else if($this->session->userdata('school_account') && $this->session->userdata('school_user') == 'student') {
			$user = $this->session->userdata('student_id');
			$column = 'student_id';
		}

		$data = array(
			$column 		    => $user,
			'score' 		    => 0,
			'score_percent' => '0',
			'status'		    => 0,
			'date_created'  => date("Y-m-d H:i:s")
		);

		$this->db->insert("test_quiz_header", $data);
		$testid = $this->db->insert_id();

		return $testid;
	}

	function update_testheader($testid, $testnum)
	{
		$data = array(
			'test_number' 		=> $testnum,
		);

		$this->db->where("test_id",$testid);
		$this->db->update("test_quiz_header", $data);

		return true;
	}

	function run_test_recorder($data) {
		$test_id = $data['id'];

		foreach($data['questions'] as $key => $val) {
			$data_insert_question = array(
				'header_id' => $test_id,
				'topic_id' => $val->topic_id,
				'topic_description' => $val->description,
				'question_id' => $val->question_id,
				'quest_image' => $val->quest_image,
				'quest_description' => $val->quest_description,
				'option_text_a' => $val->option_text_a,
				'option_text_b' => $val->option_text_b,
				'option_text_c' => $val->option_text_c,
				'option_text_d' => $val->option_text_d,
				'option_text_e' => $val->option_text_e,
				'quest_answer' => $val->quest_answer,
				'miscon' => $val->miscon
			);

			$this->db->insert('test_quiz_items', $data_insert_question);
		}
	}

	function get_topc_description($topicid)
	{
		$this->db->select("description");
		$this->db->from("test_topic");
		$this->db->where("topic_id",$topicid);
		$res = $this->db->get()->result();

		return $res[0]->description;
	}

	function get_questions($topics) {

		$topic_ids_str = str_replace('_', ',', $topics);
		$topic_ids_str = str_replace('on,', '', $topic_ids_str);
		$topic_ids_str_filtered = $this->genererate_topic_sequence($topic_ids_str);
		$topic_ids_arr = explode(',', $topic_ids_str_filtered);
		$test = array();

		// Set SQL Variables
		$set_sql = '
			SET @rn := 0,
				@ng := NULL;
		';
		$this->db->query($set_sql);

		// SQL
		$sql = '
			SELECT
				*,
				@rn := if(topic_id != @ng, 1, @rn + 1) AS ng,
				@ng := topic_id
			FROM(
				SELECT
					topic.topic_id,
					topic.description,
					question.question_id,
					question.quest_image,
					question.quest_description,
					question.option_text_a,
					question.option_text_b,
					question.option_text_c,
					question.option_text_d,
					question.option_text_e,
					question.quest_answer,
					question.miscon,
					(
						SELECT COUNT(item_id)
						FROM
							test_quiz_items
						LEFT JOIN
							test_quiz_header ON test_quiz_items.header_id = test_quiz_header.test_id
						WHERE
							question_id = question.question_id
							AND test_quiz_header.status = 1
					) AS questions_taken
				FROM
					test_topic topic
				LEFT JOIN
					test_question question on question.topic_id = topic.topic_id
				WHERE
					topic.topic_id IN(' . $topic_ids_str_filtered . ')
				ORDER BY
					topic.topic_id,
					questions_taken
			) AS A
			ORDER BY
				ng,
				topic_id
			LIMIT ' . WG_MAX_NUM_QUESTIONS . ';
		';

		// Query
		$query = $this->db->query($sql);
		$result = $query->result();

		return $result;
	}

	function paid_puller($topics) {

		$test = array();

		// Get Questions
		$questions = $this->get_questions($topics);
		$test['questions'] = $questions;

		// Setup Test Header
		$test['id'] = $this->setup_test_header();

		// Update Test Header
		$this->update_testheader($test['id'], count($questions));

		// Save Questions
		$this->run_test_recorder($test);

		return $test;
	}
#======================================= generate free trial question process ========================

	function free_puller($topics) {

		return $this->get_questions($topics);
	}

#======================================= this area is for generating result ==========================

	function generate_result()
	{
		$testid = $_POST['testid'];
		$quest  = $_POST['data'];

		# update test items

		for($x=0; $x<=(count($quest) - 1); $x++){

			# update the test data in DB
			$datax = array(
				'user_answer' => $quest[$x][1]
			);

			$this->db->where("header_id", $testid);
			$this->db->where("question_id", $quest[$x][0]);
			$this->db->update("test_quiz_items", $datax);

		}

		# update the test header
		$result = $this->test_result($testid);

		$score = 0;
		foreach($result as $key => $val){
			if($val->quest_answer == $val->user_answer){
				$score++;
			}
		}
		$percent = ($score / count($result)) * 100;

		$header = array(
			'score' 		=> $score,
			'score_percent'	=> $percent,
			'status' => 1
		);

		$this->db->where("test_id", $testid);
		$this->db->update("test_quiz_header", $header);

		// // Check if account is a FREE-ACCOUNT
		// // Then save the current time so that we will limit the time the user can view the test results
		// if($this->session->userdata('acctype') == 'FREE-ACCOUNT') {
		// 	$this->session->set_userdata('time_taken_free_test', date('U'));
		// }

		$datas = array("success"=> true, "testid" => $testid);
		die(json_encode($datas));
	}

	function test_result($testid)
	{
		$this->db->select("*");
		$this->db->from("test_quiz_items");
		$this->db->where("header_id",$testid);
		$this->db->order_by("item_id", "asc");
		$res = $this->db->get()->result();

		return $res;
	}

	function generate_free_result($data)
	{
		#data format [question_id, user answer, correct answer]
		#echo "<pre>";
		#print_r($data);
		#die();

		#get test sheet for free trial to be displayed in test result page
		for($x=0; $x<=(count($data)-1); $x++){

			$this->db->select("a.*,b.topic_id,b.description");
			$this->db->from("test_question a");
			$this->db->join("test_topic b", "b.topic_id = a.topic_id", "left");
			$this->db->where("a.question_id",$data[$x][0]);
			$res = $this->db->get()->result();

			$data[$x][2] = $res[0]->quest_answer;
			$data[$x][3] = $res[0]->description;

			$data[$x][4] = $res[0]->quest_image;
			$data[$x][5] = $res[0]->quest_description;
			$data[$x][6] = $res[0]->option_text_a;
			$data[$x][7] = $res[0]->option_text_b;
			$data[$x][8] = $res[0]->option_text_c;
			$data[$x][9] = $res[0]->option_text_d;
			$data[$x][10] = $res[0]->option_text_e;
			$data[$x][11] = $res[0]->miscon;
		}

		return $data;
	}

#=================================================== user account feature =================================

	function get_my_tests()
	{
		$this->db->select("*");
		$this->db->from("test_quiz_header");
		$this->db->where("status", 1);
		$this->db->order_by("date_created", "desc");

		// Check if Individual Account
		if($this->session->userdata('acctype')) {
			$this->db->where("user_id", $this->session->userdata('id'));
		}
		// Or Check if School Student Account
		else if($this->session->userdata('school_account') && $this->session->userdata('school_user') == 'student') {
			$this->db->where("student_id", $this->session->userdata('student_id'));
		}
		// Or Check if School Teacher Account
		// And has a student id field
		else if($this->session->userdata('school_account') && $this->session->userdata('school_user') == 'teacher' && $this->input->get('student_id')) {
			$this->db->where("student_id", $this->input->get('student_id', TRUE));
		}

		$result = $this->db->get()->result();

		foreach($result as $key => $val) {
			// Get Subject Description
			$sql_subject_description = '
				SELECT DISTINCT(subjects.description)
				FROM
					test_quiz_items quiz_items
				LEFT JOIN
					test_topic topics ON quiz_items.topic_id = topics.topic_id
				LEFT JOIN
					test_subject subjects ON topics.subject_id = subjects.subject_id
				WHERE
					quiz_items.header_id = ' . $val->test_id . '
			';
			$query_subject_description = $this->db->query($sql_subject_description);
			$row = $query_subject_description->first_row();
			$val->subject_description = $row->description;

			// Get Number of Topics
			$sql_num_topics = '
				SELECT DISTINCT topic_id
				FROM test_quiz_items
				WHERE header_id = ' . $val->test_id . '
			';
			$query_num_topics = $this->db->query($sql_num_topics);
			$val->num_topics = $query_num_topics->num_rows();

			// Get Number of Questions
			$sql_num_questions = '
				SELECT item_id
				FROM test_quiz_items
				WHERE header_id = ' . $val->test_id . '
			';
			$query_num_questions = $this->db->query($sql_num_questions);
			$val->num_questions = $query_num_questions->num_rows();


			// Get Number of Correct Answers
			$sql_num_correct = '
				SELECT item_id
				FROM test_quiz_items
				WHERE
					header_id = ' . $val->test_id . '
					AND quest_answer = user_answer
			';
			$query_num_correct = $this->db->query($sql_num_correct);
			$val->num_correct = $query_num_correct->num_rows();
		}

		return $result;
	}

	function is_trial_taken()
	{
		$this->db->select("test_count");
		$this->db->from("test_trial_record");
		$this->db->where("user_id", $this->session->userdata('id'));
		$res = $this->db->get()->result();

		if(@$res[0]->test_count > 0){
			return true;
		}else{
			return false;
		}
	}

	function update_trial_record()
	{
		$datax = array(
			'test_count' => 1,
			'date_taken' => @date("Y-m-d H:i:s")
		);

		$this->db->where("user_id", $this->session->userdata('id'));
		$this->db->update("test_trial_record",$datax);

		return true;
	}

	function get_question_details($post_data) {
		$sql = '
			SELECT
				test_quiz_items.quest_answer,
				test_quiz_items.user_answer
			FROM
				test_quiz_header
			LEFT JOIN
				test_quiz_items ON test_quiz_header.test_id = test_quiz_items.header_id
			WHERE
				test_quiz_items.question_id = ' . $post_data['question_id'] . '
			    AND test_quiz_header.status = 1
		';
		$query = $this->db->query($sql);
		$result = $query->result();

		$correct_count = 0;
		$wrong_count = 0;

		if(count($result) > 0) {
			foreach($result as $key => $val) {
				// Check if correct
				if($val->quest_answer == $val->user_answer) $correct_count++;
				else $wrong_count++;
			}
		}

		$data = array(
			'question_id' => $post_data['question_id'],
			'question_count' => count($result),
			'correct_count' => $correct_count,
			'wrong_count' => $wrong_count
		);

		die(json_encode($data));
	}
}
?>
