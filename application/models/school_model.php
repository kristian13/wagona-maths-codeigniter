<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class School_model extends CI_Model {

	// Constructor
	function __construct() {
		parent::__construct();
		ini_set('date.timezone', 'Europe/London');
	}

	// Validate if School Name exists
	function validate_school_name_exists($name) {
		$this->db->select('*');
		$this->db->from('school_list');
		$this->db->where('name', $name);
		$query = $this->db->get();
		return $query->num_rows() > 0 ? false : true;
	}

	// Validate if School Email Address exists
	function validate_school_email_address_exists($email_address) {
		$valid = true;

		// Check in School Accounts
		$this->db->select('*');
		$this->db->from('school_list');
		$this->db->where('email_address', $email_address);
		$query = $this->db->get();
		if($query->num_rows() > 0) $valid = false;

		// Check in User Accounts
		$this->db->select('*');
		$this->db->from('user_accounts');
		$this->db->where('user_email', $email_address);
		$query = $this->db->get();
		if($query->num_rows() > 0) $valid = false;

		return $valid;
	}

	// Validate if Class Name exists
	function validate_class_name_exists($name, $class_id = null) {
		$this->db->select('*');
		$this->db->from('school_class_list');
		$this->db->where('name', $name);
		if(!is_null($class_id)) {
			$this->db->where('class_id !=', $class_id);
		}
		$query = $this->db->get();
		return $query->num_rows() > 0 ? false : true;
	}

	// Validate if Teacher Name exists
	function validate_teacher_name_exists($firstname, $surname, $teacher_id = null) {
		$this->db->select('*');
		$this->db->from('school_teacher_list');
		$this->db->where('firstname', $firstname);
		$this->db->where('surname', $surname);
		if(!is_null($teacher_id)) {
			$this->db->where('teacher_id !=', $teacher_id);
		}
		$query = $this->db->get();
		return $query->num_rows() > 0 ? false : true;
	}

	// Validate if Classes are already taken
	function validate_classes_are_taken($classes, $teacher_id = null) {
		$class_ids = explode(',', $classes);
		$valid = true;
		$classes_taken = array();

		foreach($class_ids as $key => $val) {
			$this->db->select('*');
			$this->db->from('school_teacher_class');
			$this->db->where('class_id', $val);
			if(!is_null($teacher_id)) {
				$this->db->where('teacher_id !=', $teacher_id);
			}
			$query = $this->db->get();

			if($query->num_rows() > 0) {
				$valid = false;

				$row = $query->first_row('array');
				$class_id = $row['class_id'];

				$sql_class = 'SELECT name FROM school_class_list WHERE class_id = ' . $class_id;
				$query_class = $this->db->query($sql_class);
				$row_class = $query_class->first_row('array');
				$classes_taken[] = $row_class['name'];
			}
		}

		return array(
			'valid' => $valid,
			'classes_taken' => $classes_taken
		);
	}

	// Validate if Student Name exists
	function validate_student_name_exists($firstname, $surname, $student_id = null) {
		$this->db->select('*');
		$this->db->from('school_student_list');
		$this->db->where('firstname', $firstname);
		$this->db->where('surname', $surname);
		if(!is_null($student_id)) {
			$this->db->where('student_id !=', $student_id);
		}
		$query = $this->db->get();
		return $query->num_rows() > 0 ? false : true;
	}

	// Validate if Student Username exists
	function validate_student_username_exists($username, $student_id = null) {
		$this->db->select('*');
		$this->db->from('school_student_list');
		$this->db->where('username', $username);
		if(!is_null($student_id)) {
			$this->db->where('student_id !=', $student_id);
		}
		$query = $this->db->get();
		return $query->num_rows() > 0 ? false : true;
	}

	// Get Schools
	function get_schools($school_id = null) {
		$this->db->select('*');
		$this->db->from('school_list');
		$this->db->order_by('name', 'asc');
		if(!is_null($school_id)) {
			$this->db->where('school_id', $school_id);
		}
		$query = $this->db->get();
		return $query->result_array();
	}

	// Insert School
	function insert_school() {
		$data = array(
			'name' => mysql_real_escape_string($this->input->post('name')),
			'address_1' => mysql_real_escape_string($this->input->post('address_1')),
			'address_2' => mysql_real_escape_string($this->input->post('address_2')),
			'address_3' => mysql_real_escape_string($this->input->post('address_3')),
			'admin_contact' => mysql_real_escape_string($this->input->post('admin_contact')),
			'telephone_num' => mysql_real_escape_string($this->input->post('telephone_num')),
			'cellphone_num' => mysql_real_escape_string($this->input->post('cellphone_num')),
			'website_url' => mysql_real_escape_string($this->input->post('website_url')),
			'num_students' => mysql_real_escape_string($this->input->post('num_students')),
			'email_address' => mysql_real_escape_string($this->input->post('email_address')),
			'password' => mysql_real_escape_string(md5($this->input->post('password'))),
			'created_at' => date('Y-m-d H:i:s')
		);
		$this->db->insert('school_list', $data);

		return $this->db->insert_id();
	}

	// Update School
	function update_school() {
		$data = array(
			'name' => mysql_real_escape_string($this->input->post('name')),
			'address_1' => mysql_real_escape_string($this->input->post('address_1')),
			'address_2' => mysql_real_escape_string($this->input->post('address_2')),
			'address_3' => mysql_real_escape_string($this->input->post('address_3')),
			'admin_contact' => mysql_real_escape_string($this->input->post('admin_contact')),
			'telephone_num' => mysql_real_escape_string($this->input->post('telephone_num')),
			'cellphone_num' => mysql_real_escape_string($this->input->post('cellphone_num')),
			'email_address' => mysql_real_escape_string($this->input->post('email_address')),
			'website_url' => mysql_real_escape_string($this->input->post('website_url')),
			'num_students' => mysql_real_escape_string($this->input->post('num_students')),
			'updated_at' => date('Y-m-d H:i:s')
		);
		$this->db->where('school_id', $this->input->post('school_id'));
		$this->db->update('school_list', $data);
	}

	// Get Subjects
	function get_subjects() {
		$this->db->select('*');
		$this->db->from('test_subject');
		$this->db->where('status', '1');
		$this->db->order_by('description', 'asc');
		$query = $this->db->get();
		return $query->result_array();
	}

	// Get Classes
	function get_classes($subject_id = null, $class_id = null, $active_only = true) {
		$this->db->select('*, school_class_list.status AS class_status');
		$this->db->from('school_class_list');
		$this->db->join('test_subject', 'school_class_list.subject_id = test_subject.subject_id');
		$this->db->where('school_class_list.school_id', $this->session->userdata('school_id'));
		if($subject_id && !is_null($subject_id)) {
			$this->db->order_by('test_subject.description', 'asc');
			$this->db->where('school_class_list.subject_id', $subject_id);
		}
		if($class_id && !is_null($class_id)) {
			$this->db->where('school_class_list.class_id', $class_id);
		}
		if($active_only == true) {
			$this->db->where('school_class_list.status', '1');
		}
		$this->db->order_by('school_class_list.status', 'asc');
		$this->db->order_by('school_class_list.name', 'asc');
		$query = $this->db->get();
		return $query->result_array();
	}

	// Insert Class
	function insert_class() {
		$data = array(
			'name' => mysql_real_escape_string($this->input->post('name')),
			'school_id' => $this->session->userdata('school_id'),
			'subject_id' => mysql_real_escape_string($this->input->post('subject_id')),
			'created_at' => date('Y-m-d H:i:s')
		);
		$this->db->insert('school_class_list', $data);
	}

	// Update Class
	function update_class() {
		$data = array(
			'name' => mysql_real_escape_string($this->input->post('name')),
			'subject_id' => mysql_real_escape_string($this->input->post('subject_id')),
			'updated_at' => date('Y-m-d H:i:s')
		);
		$this->db->where('class_id', $this->input->post('class_id'));
		$this->db->update('school_class_list', $data);
	}

	// Update Class Status
	function update_class_status() {
		$data = array(
			'status' => $this->input->post('status') == 'true' ? 1 : 0,
			'updated_at' => date('Y-m-d H:i:s')
		);
		$this->db->where('class_id', $this->input->post('class_id'));
		$this->db->update('school_class_list', $data);
	}

	// Get Teachers
	function get_teachers($teacher_id = null) {
		$this->db->select('teacher_id, firstname, surname, username, password, classes, teacher_status, class_ids');
		$this->db->from('view_teacher_details_list');
		$this->db->where('school_id', $this->session->userdata('school_id'));
		if(!is_null($teacher_id)) {
			$this->db->where('teacher_id', $teacher_id);
		}
		$this->db->order_by('firstname', 'asc');
		$this->db->order_by('surname', 'asc');
		$query = $this->db->get();
		return $query->result_array();
	}

	// Insert Teacher
	function insert_teacher() {
		$firstname = mysql_real_escape_string($this->input->post('firstname'));
		$surname = mysql_real_escape_string($this->input->post('surname'));
		// $username = WG_TEACHER_USERNAME_PREFIX . strtolower(str_replace(' ', '', $firstname)) . '.' . strtolower(str_replace(' ', '', $surname));
		$username = mysql_real_escape_string($this->input->post('username'));
		$password = mysql_real_escape_string($this->input->post('password'));

		$data = array(
			'firstname' => $firstname,
			'surname' => $surname,
			'username' => $username,
			// 'password' => md5(WG_TEACHER_DEFAULT_PASSWORD),
			'password' => $password,
			'created_at' => date('Y-m-d H:i:s')
		);
		$this->db->insert('school_teacher_list', $data);

		$teacher_id = $this->db->insert_id();
		$class_ids = explode(',', $this->input->post('classes'));

		foreach($class_ids as $key => $val) {
			if($val) {
				$teacher_class_data = array(
					'teacher_id' => $teacher_id,
					'class_id' => $val
				);
				$this->db->insert('school_teacher_class', $teacher_class_data);
			}
		}
	}

	// Update Class
	function update_teacher() {
		$data = array(
			'firstname' => mysql_real_escape_string($this->input->post('firstname')),
			'surname' => mysql_real_escape_string($this->input->post('surname')),
			'username' => mysql_real_escape_string($this->input->post('username')),
			'password' => mysql_real_escape_string($this->input->post('password')),
			'updated_at' => date('Y-m-d H:i:s')
		);
		$this->db->where('teacher_id', $this->input->post('teacher_id'));
		$this->db->update('school_teacher_list', $data);

		// Delete first existing data
		$this->db->delete('school_teacher_class', array('teacher_id' => $this->input->post('teacher_id')));

		$class_ids = explode(',', $this->input->post('classes'));

		foreach($class_ids as $key => $val) {
			if($val) {
				$teacher_class_data = array(
					'teacher_id' => $this->input->post('teacher_id'),
					'class_id' => $val
				);
				$this->db->insert('school_teacher_class', $teacher_class_data);
			}
		}
	}

	// Update Teacher Status
	function update_teacher_status() {
		$data = array(
			'status' => $this->input->post('status') == 'true' ? 1 : 0,
			'updated_at' => date('Y-m-d H:i:s')
		);
		$this->db->where('teacher_id', $this->input->post('teacher_id'));
		$this->db->update('school_teacher_list', $data);
	}

	// Get Teachers
	function get_students($student_id = null) {
		$this->db->select('student_id, firstname, surname, class_id, class_name, username, password, student_status');
		$this->db->from('view_student_class_details_list');
		$this->db->where('school_id', $this->session->userdata('school_id'));
		if(!is_null($student_id)) {
			$this->db->where('student_id', $student_id);
		}
		$this->db->order_by('firstname', 'asc');
		$this->db->order_by('surname', 'asc');
		$query = $this->db->get();
		return $query->result_array();
	}

	// Insert Student
	function insert_student() {
		$firstname = mysql_real_escape_string($this->input->post('firstname'));
		$surname = mysql_real_escape_string($this->input->post('surname'));
		// $username = WG_STUDENT_USERNAME_PREFIX . strtolower(str_replace(' ', '', $firstname)) . '.' . strtolower(str_replace(' ', '', $surname));
		$username = mysql_real_escape_string($this->input->post('username'));
		$password = mysql_real_escape_string($this->input->post('password'));
		$class_id = mysql_real_escape_string($this->input->post('class_id'));

		$data = array(
			'firstname' => $firstname,
			'surname' => $surname,
			'username' => $username,
			// 'password' => md5(WG_STUDENT_DEFAULT_PASSWORD),
			'password' => $password,
			'class_id' => $class_id,
			'created_at' => date('Y-m-d H:i:s')
		);
		$this->db->insert('school_student_list', $data);
	}

	// Update Student
	function update_student() {
		$firstname = mysql_real_escape_string($this->input->post('firstname'));
		$surname = mysql_real_escape_string($this->input->post('surname'));
		// $username = WG_STUDENT_USERNAME_PREFIX . strtolower(str_replace(' ', '', $firstname)) . '.' . strtolower(str_replace(' ', '', $surname));
		$username = mysql_real_escape_string($this->input->post('username'));
		$password = mysql_real_escape_string($this->input->post('password'));
		$class_id = mysql_real_escape_string($this->input->post('class_id'));

		$data = array(
			'firstname' => $firstname,
			'surname' => $surname,
			'username' => $username,
			// 'password' => md5(WG_STUDENT_DEFAULT_PASSWORD),
			'password' => $password,
			'class_id' => $class_id,
			'updated_at' => date('Y-m-d H:i:s')
		);
		$this->db->where('student_id', $this->input->post('student_id'));
		$this->db->update('school_student_list', $data);
	}

	// Update Student Status
	function update_student_status() {
		$data = array(
			'status' => $this->input->post('status') == 'true' ? 1 : 0,
			'updated_at' => date('Y-m-d H:i:s')
		);
		$this->db->where('student_id', $this->input->post('student_id'));
		$this->db->update('school_student_list', $data);
	}

	// Get Teacher Classes
	function get_teacher_classes($teacher_id) {
		$this->db->select('class_id, class_name, subject_id, subject_name');
		$this->db->from('view_teacher_class_details_list');
		$this->db->where('teacher_id', $teacher_id);
		$this->db->where('class_status', 1);
		$this->db->order_by('class_name', 'asc');
		$query = $this->db->get();
		return $query->result_array();
	}

	// Get Teacher's Class Average
	function get_teacher_class_average($class_id) {
		$students = $this->get_class_students_average($class_id);
		$running_average = 0;
		$num_numeric_average = 0;
		$average = '';

		if(count($students) > 0) {
			foreach($students as $key => $val) {
				if(is_numeric($val['subject_average']) && $val['tests_taken'] >= WG_LEAST_COLOR_CODING_NUM) {
					$running_average += round($val['subject_average']);
					$num_numeric_average++;
				}
			}
			if($num_numeric_average > 0 && $num_numeric_average >= (count($students) / 2)) {
				$average = round($running_average / $num_numeric_average) . '%';
			}
		}

		return $average;
	}

	// Get Class's Students
	function get_class_students($class_id) {
		$this->db->select('firstname, surname, student_id');
		$this->db->from('view_student_class_details_list');
		$this->db->where('class_id', $class_id);
		$this->db->order_by('surname', 'asc');
		$query = $this->db->get();
		return $query->result_array();
	}

	// Get Class's Students Averages
	function get_class_students_average($class_id, $student_id = null) {
		$sql = '
			SELECT
				firstname, surname, subject_average, tests_taken, student_id
			FROM
				view_student_class_details_list
			WHERE
				class_id = ' . $class_id . '
				' . (!is_null($student_id) ? 'AND student_id = ' .  $student_id : '') . '
			ORDER BY
				CASE
					WHEN tests_taken >= ' . WG_LEAST_COLOR_CODING_NUM . ' THEN 0
					ELSE 1
				END ASC,
				subject_average DESC,
				surname
		';
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	// Get Student Average By Topic Details
	function get_student_average_by_topic_details($class_id, $student_id) {
		$details = $this->get_student_topics_details($class_id, $student_id);
		$running_topics_score = 0;
		$running_topics_count = 0;

		foreach($details as $k => $v) {
			if($v['num_question_answered'] >= WG_LEAST_COLOR_CODING_NUM) {
				$running_topics_score += round(($v['num_correct'] / $v['num_question_answered']) * 100);
				$running_topics_count++;
			}
		}

		$average = $running_topics_count >= WG_LEAST_SUBJECT_TOPICS_AVERAGE_NUM ? round($running_topics_score / $running_topics_count) : '--';

		return $average;
	}

	// Get Student Topics Details
	function get_student_topics_details($class_id, $student_id) {
		$sql = '
			SELECT
				student_list.student_id,
				student_list.firstname,
				student_list.surname,
				subject_list.subject_id,
				subject_list.description AS subject_name,
				subject_list.status AS subject_status,
				topic_list.topic_id,
				topic_list.description AS topic_name,
				topic_list.status AS topic_status,
				(
					SELECT COUNT(test_quiz_items.user_answer)
					FROM
						test_quiz_header
					LEFT JOIN
						test_quiz_items ON test_quiz_header.test_id = test_quiz_items.header_id
					WHERE
						test_quiz_header.student_id = student_list.student_id
						AND test_quiz_header.status = 1
						AND test_quiz_items.topic_id = topic_list.topic_id
				) AS num_question_answered,
				(
					SELECT COUNT(test_quiz_items.user_answer)
					FROM
						test_quiz_header
					LEFT JOIN
						test_quiz_items ON test_quiz_header.test_id = test_quiz_items.header_id
					WHERE
						test_quiz_header.student_id = student_list.student_id
						AND test_quiz_header.status = 1
						AND test_quiz_items.topic_id = topic_list.topic_id
						AND test_quiz_items.user_answer = test_quiz_items.quest_answer
				) AS num_correct
			FROM
				school_student_list AS student_list
			LEFT JOIN
				school_class_list AS class_list ON student_list.class_id = class_list.class_id
			LEFT JOIN
				test_subject AS subject_list ON class_list.subject_id = subject_list.subject_id
			LEFT JOIN
				test_topic AS topic_list ON subject_list.subject_id = topic_list.subject_id
			WHERE
				subject_list.status = 1
				AND topic_list.status = 1
				AND class_list.class_id = ' . $class_id . '
				AND student_id = ' . $student_id . '
		';
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	// Get Teacher Subject Topics
	function get_teacher_subject_topics($subject_id) {
		$this->db->select("*");
		$this->db->from('test_topic');
		$this->db->where('subject_id', $subject_id);
		$this->db->where('status', 1);
		$this->db->order_by('topic_id', 'asc');
		$query = $this->db->get();
		return $query->result_array();
	}

	// Get Teacher Topic's Average Details
	function get_teacher_topic_average_details($topic_id, $class_id) {
		$score = 0;
		$percentage = '-';
		$num_correct = '-';
		$num_wrong = '-';
		$num_questions = '-';
		$sql = '
			SELECT
				test_quiz_items.quest_answer,
				test_quiz_items.user_answer
			FROM
				test_quiz_header
			LEFT JOIN
				test_quiz_items ON test_quiz_header.test_id = test_quiz_items.header_id
			WHERE
				test_quiz_header.student_id IN(SELECT student_id FROM view_student_class_details_list WHERE class_id = ' . $class_id . ')
				AND test_quiz_header.status = 1
				AND test_quiz_items.topic_id = ' . $topic_id . '
		';
		$query = $this->db->query($sql);
		$question_count = $query->num_rows();

		if($question_count > 0) {
			$num_questions = $question_count;

			$correct_stmt = 'A.quest_answer = A.user_answer';
			$wrong_stmt = 'A.quest_answer != A.user_answer';

			$sql_correct = '
				SELECT *
				FROM(' . $sql . ') AS A
				WHERE ' . $correct_stmt . '
			';
			$num_correct = $this->db->query($sql_correct)->num_rows();

			$sql_wrong = '
				SELECT *
				FROM(' . $sql . ') AS A
				WHERE ' . $wrong_stmt . '
			';
			$num_wrong = $this->db->query($sql_wrong)->num_rows();

			$score = round(number_format((($num_correct / $num_questions) * 100), 2, '.', ''));
			$percentage = '<span>'. $score . '%</span>';
		}

		$data = array();
		$data['score'] = $score;
		$data['percentage'] = $percentage;
		$data['num_correct'] = $num_correct;
		$data['num_wrong'] = $num_wrong;
		$data['num_questions'] = $num_questions;

		return $data;
	}

	// Get Student Answered Topic Question Details
	function get_student_answered_topic_question_details($student_id, $topic_id) {
		$num_correct = 0;
		$num_questions = 0;
		$sql = '
			SELECT
				test_quiz_items.quest_answer,
				test_quiz_items.user_answer
			FROM
				test_quiz_header
			LEFT JOIN
				test_quiz_items ON test_quiz_header.test_id = test_quiz_items.header_id
			WHERE
				test_quiz_header.student_id = ' . $student_id . '
				AND test_quiz_header.status = 1
				AND test_quiz_items.topic_id = ' . $topic_id . '
		';
		$query = $this->db->query($sql);
		$num_questions = $query->num_rows();

		if($num_questions > 0) {
			$correct_stmt = 'A.quest_answer = A.user_answer';

			$sql_correct = '
				SELECT *
				FROM(' . $sql . ') AS A
				WHERE ' . $correct_stmt . '
			';
			$num_correct = $this->db->query($sql_correct)->num_rows();
		}

		$data = array();
		$data['num_correct'] = $num_correct;
		$data['num_questions'] = $num_questions;
		$data['percentage'] = $num_questions > 0 ? round(number_format((($num_correct / $num_questions) * 100), 2, '.', '')) . '%' : '--';

		return $data;
	}
}