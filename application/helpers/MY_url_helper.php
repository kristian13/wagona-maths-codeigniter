<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('count_my_test')) {
	function count_my_test() {
		$CI =& get_instance();
		$CI->load->database();

		$CI->db->select("count(*) as alldata");
		$CI->db->from("test_quiz_header");
		$CI->db->where("status", 1);

		// Check if Individual Account
		if($CI->session->userdata('acctype')) {
			$CI->db->where("user_id", $CI->session->userdata('id'));
		}
		// Or Check if School Student Account
		else if($CI->session->userdata('school_account') && $CI->session->userdata('school_user') == 'student') {
			$CI->db->where("student_id", $CI->session->userdata('student_id'));
		}

		$res = $CI->db->get()->result();

		echo $res[0]->alldata;
	}
}

if ( ! function_exists('commulative_average')) {
	function commulative_average() {
		$cnt = 0;
		$total = 0;
		$CI =& get_instance();
		$CI->load->database();

		$CI->db->select("*");
		$CI->db->from("test_quiz_header");
		$CI->db->where("user_id", $CI->session->userdata('id'));
		$CI->db->where("status", 1);
		$CI->db->order_by("test_id", "desc");
		$CI->db->limit(5);
		$res = $CI->db->get()->result();

		if(count($res) > 0){
			foreach($res as $key => $val){
				if($val->score > 0){
					$total = $total + (($val->score / $val->test_number) * 100);
				}
				$cnt++;
			}

			return round(number_format(($total / ($cnt * 100)) * 100, 2, '.',''));
		}else{
			return 0;
		}
	}
}

if ( ! function_exists('subject_average')) {
	function subject_average($subjectid) {
		$CI =& get_instance();
		$CI->load->database();

		$user = $CI->session->userdata('id');

		$SQL = "select a.*,b.header_id,b.topic_id,c.subject_id from test_quiz_header a
				left join test_quiz_items b on a.test_id = b.header_id
				left join test_topic c on c.topic_id = b.topic_id
				where
				a.user_id = ".$user."
				and a.status = 1
				and c.subject_id = ".$subjectid."
				GROUP BY a.test_id";

		$result = $CI->db->query($SQL);
		$data = $result->result_object();
		$datacount = count($data);

		if($datacount > 0){

			$total = 0;

			foreach($data as $key => $val){
				$total = $total + $val->score;
			}

			echo "Average: ".round(number_format(($total / ($datacount * 100)) * 100, 2, '.',''))."%";

		}
		else {
			echo "No Test Taken";
		}
	}
}

// Get Score Background Class
if ( ! function_exists('helper_get_score_bg_class')) {
	function helper_get_score_bg_class($score) {

		$score = preg_replace('/\D/', '', $score);

		// Check if score is a number
		if(!is_numeric($score)) {
			return 'score-black-bg';
		}

		$score = (int)$score;

		if($score < 50) {
			return 'score-red-bg';
		}
		else if($score >= 50 && $score < 75) {
			return 'score-amber-bg';
		}
		else {
			return 'score-green-bg';
		}
	}
}

// Get Score Class
if ( ! function_exists('helper_get_score_class')) {
	function helper_get_score_class($score, $question_count = null) {

		$score = preg_replace('/\D/', '', $score);

		// Check if score is a number
		if(!is_numeric($score)) {
			return 'score-black';
		}

		// Check if $question_count is not null
		// and $question_count is less than the least number of questions taken to be color coded
		if(
			(!is_null($question_count) && !is_numeric($score)) ||
			(!is_null($question_count) && $question_count < WG_LEAST_COLOR_CODING_NUM)
		) {
			return 'score-black';
		}

		$score = (int)$score;

		if($score < 50) {
			return 'score-red';
		}
		else if($score >= 50 && $score < 75) {
			return 'score-amber';
		}
		else {
			return 'score-green';
		}
	}
}

// Get Score Grade
if ( ! function_exists('helper_get_score_grade')) {
	function helper_get_score_grade($score) {

		$score = preg_replace('/\D/', '', $score);

		// Check if score is a number
		if(!is_numeric($score)) {
			return '';
		}

		$score = (int)$score;

		if($score < 30) {
			return 'E';
		}
		else if($score >= 30 && $score < 50) {
			return 'D';
		}
		else if($score >= 50 && $score < 70) {
			return 'C';
		}
		else if($score >= 70 && $score < 80) {
			return 'B';
		}
		else {
			return 'A';
		}
	}
}
?>