<?php $this->load->view('header.php'); ?>

<div id="content" class="content-row">
	<div class="container">
		<h2>You are Forbidden to View this Page</h2>
		<h4>Return to <a href="<?php echo base_url() . 'account/dashboard'?>">Dashboard</a></h4>
	</div>
</div>

<?php $this->load->view('footer.php'); ?>