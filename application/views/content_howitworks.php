<?php $this->load->view('header.php'); ?>

<div class="container">
  <div class="divAbout">
    <p class="about-title">How It Works</p>
    <p>
      The idea of establishing a diagnostic E-learning platform is based on the premise that a medical doctor will never say to a patient, “here take this bunch of tablets, one of them will cure your illness.” Instead the doctor will carry out a diagnosis before prescribing just the right medication for the patient. Wagona Maths works on a similar principle. The platform identifies topics that a student is struggling with so that they can concentrate on those instead of re-studying the whole course. At Wagona Maths we call it “Smart Learning”.
    </p>
    <div class="row" style="margin-top: 50px;">
      <div class="col-md-3">
        <img src="<?php echo base_url() . 'assets/images/howitworks_1.jpg' ?>">
      </div>
      <div class="col-md-9">
        <p class="about-title" style="margin-top: 20px;">Students Landing Page</p>
        <p>
          The student landing page contains a summary of current performance, including BRAG report on the last 5 tests taken and a tracking average percentage mark/grade for the form that the student is registered in. The links on the colour coded percentage marks will take the student to the results page of the selected test. The student can then see exactly which questions they got wrong and why.
        </p>
      </div>
    </div>
    <div class="row" style="margin-top: 50px;">
      <div class="col-md-7">
        <p class="about-title" style="margin-top: 20px;">Taking Tests</p>
        <p>
          Wagona Maths tests are made up of 10 multiple choice questions which are randomly generated from student selected topics and timed to last for 20 minutes. The student can select one or more topics to be included in the test at a given time. Where necessary a web calculator and working space are available for students to use.The platform keeps a record of student progress on all topics in colour coded format (<strong><span class="score-black">B</span><span class="score-red">R</span><span class="score-amber">A</span><span class="score-green">G</span></strong> colours), highlighting areas of weakness.
        </p>
      </div>
      <div class="col-md-5">
        <img src="<?php echo base_url() . 'assets/images/howitworks_2.jpg' ?>">
      </div>
    </div>
    <div class="row" style="margin-top: 50px;">
      <div class="col-md-7">
        <img src="<?php echo base_url() . 'assets/images/howitworks_3.jpg' ?>">
      </div>
      <div class="col-md-5">
        <p style="margin-top: 50px;">
          Student answers are marked automatically and immediately with an option to view correct explanations for every question marked wrong.
        </p>
      </div>
    </div>
    <div class="row" style="margin-top: 50px;">
      <div class="col-md-12">
        <p class="about-title">Test History</p>
        <p>
          Students are free to re-take a topic test an unlimited number of times. The platform will keep tracking progress and update the average percentage mark and grade dynamically. The idea is to turn all topics “Green”. According to the Wagona Maths algorithm, the “greener” the <strong><span class="score-black">B</span><span class="score-red">R</span><span class="score-amber">A</span><span class="score-green">G</span></strong> report, the better the prospects for achieving the predicted grade.
        </p>
      </div>
      <div class="col-md-10 col-md-offset-1">
        <img src="<?php echo base_url() . 'assets/images/howitworks_4.jpg' ?>">
      </div>
      <div class="col-md-12">
        <p>
          From the test history page the student can go back to any past test and view the results page exactly as it was on the day the test was taken. Learning Maths has never been more fun.
        </p>
        <p>
          Wagona Maths makes it quite hard to fail a maths exam, don’t work harder, work smarter. 
        </p>
      </div>
    </div>
  </div>
</div>

<?php $this->load->view('footer.php'); ?>
