<?php $this->load->view('header.php'); ?>

<div id="content" class="content-row">
	<div class="container">
		<div class="panel-group" id="accordion">
		
				<center><h1 class="main_title blue_text">COOKIE POLICY</h1></center>
                <div class="panel-group" id="accordion">
            <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="panel-title">
                  <a data-toggle="collapse" data-parent="#accordion" href="#cookieCollapseOne" title="cookieCollapseOne">
                    Cookie Policy <i class="pull-right collapse_icon glyphicon glyphicon-chevron-down"></i>
                  </a>
                </h4>
              </div>
              <div id="cookieCollapseOne" class="panel-collapse collapse in">
                <div class="panel-body">
                  <h3>What are Cookies</h3>
                  <p>This privacy and cookies policy applies to information we collect from you through cookies when you visit our websites.</p>
                  <p>A "cookie" is a small piece of information/data created and sent by a web page server to be stored on your computer’s hard drive, so it can be read back later from a browser.</p>
                  <p>Cookies may be set by the website you are visiting (‘first party cookies’, for example those used for website analytics) or they may be set by other websites who run content on the page you are viewing (‘third party cookies’, for example when a YouTube video is embedded within a page).</p>
                  <p>We use first party cookies to record website usage. We use this data to help improve the usability of our websites.</p>
                  <p>We don’t sell the information collected by our first party cookies, nor do we disclose the information to third parties, except where required by law (for example to government bodies and law enforcement agencies). Sometime though third party cookies may be set where other parties content is viewable on our websites and we do not see the information they contain.</p>
                  <p class="blue_text">PassUKtest  does not hold any personal data (such as your name, address or any other information which could identify you) in our cookies.</p>
                </div>
              </div>
            </div><!-- panel -->
            <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="panel-title">
                  <a data-toggle="collapse" data-parent="#accordion" href="#cookieCollapseTwo" title="cookieCollapseTwo">
                    What do Cookies do? <i class="pull-right collapse_icon glyphicon glyphicon-chevron-down"></i>
                  </a>
                </h4>
              </div>
              <div id="cookieCollapseTwo" class="panel-collapse collapse">
                <div class="panel-body">
                  <p>Cookies can perform a number of functions</p>
                  <h4>1. Essential cookies</h4>
                  <p>Essential cookies are essential to the operation of the website. For example, some cookies allow the website to identify a registered user. If a registered opts to disable these cookies, the user may not be able to access their account information. This information is not used for advertising on other websites.</p>
                  <h4>2. Performance Cookies</h4>
                  <p>Performance cookies are used to analyse how visitors use the websites and to monitor website performance. This allows us to provide a high quality experience by customising our offering and quickly identifying and fixing any issues that arise. This information is not used for advertising on other websites.</p>
                  <h4>3. Functionality Cookies</h4>
                  <p>Functionality cookies allow a site to remember your preferences. For example, cookies save you the trouble of typing in your username every time you access the site. We keep this information private and is not used for advertising on other websites.</p>
                  <h4>4. Behaviourally Targeted Advertising Cookies</h4>
                  <p>Behaviourally Targeted Advertising cookies allow advertisers to use cookies to serve you with advertisements that are relevant to you and your interests.</p>
                </div>
              </div>
            </div><!-- panel -->
            <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="panel-title">
                  <a data-toggle="collapse" data-parent="#accordion" href="#cookieCollapseThree" title="cookieCollapseThree">
                    1st party cookies <i class="pull-right collapse_icon glyphicon glyphicon-chevron-down"></i>
                  </a>
                </h4>
              </div>
              <div id="cookieCollapseThree" class="panel-collapse collapse">
                <div class="panel-body">
                  <p>We may collect information from the following 1st party cookies on our websites</p>
                </div>
              </div>
            </div><!-- panel -->
            
            <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="panel-title">
                  <a data-toggle="collapse" data-parent="#accordion" href="#cookieCollapseFour" title="cookieCollapseFour">
                    Google Analytics <i class="pull-right collapse_icon glyphicon glyphicon-chevron-down"></i>
                  </a>
                </h4>
              </div>
              <div id="cookieCollapseFour" class="panel-collapse collapse">
                <div class="panel-body">
                  <p>Google Analytics sets “Performance Cookies” to help HarperCollins estimate the number of visitors to the website and the volumes of usage. We use this information to compile reports and help improve our websites.</p>
                  <p>The cookies we use collect information in an anonymous form, including the number of visitors to the site, where visitors have come to the site from and the pages they visited. We do not share the Google Analytics data, not even with Google.</p>
                  <p>The cookies HarperCollins use for this purpose are:</p>
                  <p>More information about Google Analytics use of Cookies can be found on their website<br> (<a href="http://www.google.co.uk/intl/en/analytics/privacyoverview.html">http://www.google.co.uk/intl/en/analytics/privacyoverview.html</a>)</p>
                  <p>To opt out of being tracked by Google Analytics across all websites visit (<a href="http://tools.google.com/dlpage/gaoptout">http://tools.google.com/dlpage/gaoptout</a>)</p>
                </div>
              </div>
            </div><!-- panel -->

            <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="panel-title">
                  <a data-toggle="collapse" data-parent="#accordion" href="#cookieCollapseFive" title="cookieCollapseFive">
                    Consent <i class="pull-right collapse_icon glyphicon glyphicon-chevron-down"></i>
                  </a>
                </h4>
              </div>
              <div id="cookieCollapseFive" class="panel-collapse collapse">
                <div class="panel-body">
                  <p>By continuing to use any of PassUKtest’s websites you agree that we can place the cookies described above on your computer for the reasons described.</p>
                </div>
              </div>
            </div><!-- panel -->

            <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="panel-title">
                  <a data-toggle="collapse" data-parent="#accordion" href="#cookieCollapseSix" title="cookieCollapseSix">
                    How to manage cookies <i class="pull-right collapse_icon glyphicon glyphicon-chevron-down"></i>
                  </a>
                </h4>
              </div>
              <div id="cookieCollapseSix" class="panel-collapse collapse">
                <div class="panel-body">
                  <p>If you do disable our cookies you may find that certain sections of our website do not work. For example, you may have difficulties logging in or viewing articles. If you wish to disable cookies then the following links will be helpful:</p>
                  <p>Controlling cookie collection - <a href="http://www.aboutcookies.org/Default.aspx?page=1">http://www.aboutcookies.org/Default.aspx?page=1</a>
                  <br>
                    Deleting cookies - <a href="http://www.aboutcookies.org/Default.aspx?page=2">http://www.aboutcookies.org/Default.aspx?page=2</a>
                  </p>
                </div>
              </div>
            </div><!-- panel -->

          </div>
	</div>
</div>

<?php $this->load->view('footer.php'); ?>