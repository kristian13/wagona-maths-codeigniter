<?php $this->load->view('header.php'); ?>
<?php $this->load->view("account/account_menu_launch.php"); ?>

<div id="content" class="fader content-row">
	<div class="container">
	
		<div class="row">
			<div class="col-md-12">
				<div class="title">
				<p align="right" id="result"></p>

					<?php if($this->session->userdata('acctype') != 'PAID-ACCOUNT'){ 

						echo '<p align="left"><span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span> If you want to track your scores, subscribe to the platfrom or upgrade your account by clicking the upgrade button above..</span></p>';

					} ?>
				
				</div>
			</div>
		</div>
		
		<div class="flexslider">
			<ul class="slides">
				<?php foreach($subjects as $key => $val): ?>
					<?php if((count($subjects) > 4 && $key % 4 == 0) || (count($subjects) <= 4 && $key % 2 == 0)): ?>
					<li>
						<div class="row">
					<?php endif; ?>

						<div class="col-md-3">
							<div class="rect-el dasbB lgray-bg gridbox fader">
								<p class="thumb-title whitebox"><?php echo $val['description']; ?></p>

								<?php if(is_numeric(preg_replace('/\D/', '', $val['average']))): ?>
								<div class="badger subj-ave-flip fx-slow" data-toggle="tooltip" title="This is your predicted exam grade. If there is a blank circle it means that you have not taken adequate different topic tests. Keep practicing!">
									<div class="flipper">
										<div class="front <?php echo helper_get_score_bg_class($val['average']); ?>"><?php echo $val['average']; ?></div>
										<div class="back <?php echo helper_get_score_bg_class($val['average']); ?>"><?php echo helper_get_score_grade($val['average']); ?></div>
									</div>
								</div>
								<?php else: ?>
								<div class="badger subj-ave fx-slow <?php echo helper_get_score_bg_class($val['average']); ?>"  data-toggle="tooltip" title="This is your predicted exam grade. If there is a blank circle it means that you have not taken adequate different topic tests. Keep practicing!"><?php echo $val['average']; ?></div>
								<?php endif; ?>

								<?php if($this->session->userdata('acctype') == 'PAID-ACCOUNT'): ?>
									<?php foreach($val['tests'] as $a => $tester): ?>
									<p class="thumb-test-lister">
										<b><?php echo $tester['test']; ?> <span class="dater"></span></b>

										<?php if(array_key_exists('test_id', $tester)): ?>
										<a href="<?php echo base_url() ?>account/test_result/<?php echo $tester['test_id'] ?>">
											<span class="badger right fx-slow <?php echo helper_get_score_bg_class($tester['score']); ?>"><?php echo $tester['score']; ?></span>
										</a>
										<?php else: ?>
										<span class="badger right fx-slow <?php echo helper_get_score_bg_class($tester['score']); ?>"><?php echo $tester['score']; ?></span>
										<?php endif; ?>
									</p>
									<?php endforeach; ?>
								<?php endif; ?>
								<button  type="button" class="btn btn-primary thumb-button"  onClick="viewInstruction(<?php echo $val['subject_id']; ?>, '<?php echo $val['description']; ?>')">
									Take a Test
								</button>
							</div>
						</div>

					<?php if((count($subjects) > 4 && $key % 4 == 3) || (count($subjects) <= 4 && $key % 2 == 1)): ?>
						</div>
					</li>
					<?php endif; ?>
				<?php endforeach; ?>
			</ul>
		</div>
	</div>
</div>

<div class="modal fade topiclaucher" aria-hidden="false">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 id="top-title" class="modal-title red-col">Form 4 Maths</h4>
        <a class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></a>
      </div>
      <div class="modal-body no-top">

        <div role="tabpanel">

          <!-- Nav tabs -->
          <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#selectTopics" aria-controls="home" role="tab" data-toggle="tab">Select Topic(s)</a></li>
            <li role="presentation"><a href="#instructs" aria-controls="profile" role="tab" data-toggle="tab">Instructions</a></li>
            <li role="presentation">
            	<a href="#bragColours" aria-controls="profile" role="tab" data-toggle="tab">
            	<strong>
            		<span class="score-black">B</span><span class="score-red">R</span><span class="score-amber">A</span><span class="score-green">G</span>
            	</strong>
				&nbsp;Colours
            	</a>
            </li>
          </ul>

          <!-- Tab panes -->
          <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="selectTopics">
              <p class="thumb-title mb-20" style="margin-top:25px;">Select Topic(s) to include in the Test</p>
              <p style="border-bottom: solid 2px #ccc; padding-bottom: 10px; font-weight: bold; margin: 0 0 0 17px"><input type="checkbox" onclick="selectAllcheck()" id="select_all" name="select_all"> Select All</p>
              <label class="origins"> 
                <table style="width: 100%;">
                  <tbody>
                    <tr>
                      <td width="50%">&nbsp;</td>
                      <td style="text-align: right; width:12.5%"><span class="badger score-black-bg green-tooltip" data-toggle="tooltip" data-placement="top" title="Questions you attempted for topic">Q</span></td>
                      <td style="text-align: right; width:12.5%"><span class="badger score-black-bg green-tooltip circleCorrect fa fa-check" data-toggle="tooltip" data-placement="top" title="Questions you answered correctly for topic"></span></td>
                      <td style="text-align: right; width:12.5%"><span class="badger score-black-bg green-tooltip circleCorrect fa fa-times" data-toggle="tooltip" data-placement="top" title="Questions you answered incorrectly for topic"></span></td>
                      <td style="text-align: right; width:12.5%"><span class="badger score-black-bg green-tooltip" data-toggle="tooltip" data-placement="top" title="This is a moving average %age, and not ✓/Q %age calculation">%</span></td>
                    </tr>
                  </tbody>
                </table>  
              </label>
              <div id="check-topic">
              
                
              </div>
            </div>
            
			<div role="tabpanel" class="tab-pane" id="instructs">
				<div class="container">
					<div class="row">
						<div class="col-md-12" type="button">
							<div class="rect-el-full rect-el lgray-bg removepad remarg">
								<p><i class="fa fa-th-list sky-col"></i>This test contains 10 multiple choice questions.</p>
								<p><i class="fa fa-check-circle green-col"></i>There is one correct answer for each question.</p>
								<p><i class="fa fa-clock-o yellow-col"></i>You have 20 minutes to complete this test.</p>
								<p><i class="fa fa-list-ol violet-col"></i>Make sure you attempt all the questions.</p>
								<p><i class="fa fa-arrow-left black-col"></i>Do not use the browser back button while taking this test.</p>
								<p><i class="fa fa-circle-o-notch babyblue-col"></i>The timer of the test will not stop once the test starts.</p>
								<p><i class="fa fa-windows blue-col"></i>Modern web browsers improve the test experience.</p>
								<p><i class="fa fa-font lightgreen-col"></i>The test questions are in English language.</p>
								<p><i class="fa fa-calculator pink-col"></i>Where necessary use our web calculator at the top of the question.</p>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div role="tabpanel" class="tab-pane" id="bragColours">
				<div class="container">
					<div class="row">
						<div class="col-md-12" type="button">
							<div class="rect-el-full rect-el lgray-bg removepad remarg">
								<p>Explanation of <strong><span class="score-black">B</span><span class="score-red">R</span><span class="score-amber">A</span><span class="score-green">G</span></strong> colours for topics</p>
								<p>
									<span class="score-black"><strong>Black</strong> - Insufficient questions attempted to assess topic knowledge.</span><br>
									<span class="score-red"><strong>Red</strong> - Topic of high concern requiring urgent attention (0%-49%).</span><br>
									<span class="score-amber"><strong>Amber</strong> - Topic of medium concern requiring moderate attention (50% - 74%).</span><br>
									<span class="score-green"><strong>Green</strong> - Topic of low concern requiring little attention (75%-100%).</span>
								</p>
								<p>The Wagona Maths algorithm encourages the student to practise regularly. Colours may change to reflect inconsistency in revising topics.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
          </div>
        </div>


        
      </div>
      <div class="modal-footer">
      	<?php if($this->session->userdata('acctype') == 'PAID-ACCOUNT'){ ?>
				<a class="btn btn-dblue btn-lg btn-box btn-primary buttcenter button-rounded" onclick="paidClick()" href="javascript: void(0)"><span class="fa fa-file-text-o margin-right"></span>Start the Test</a>
		<?php }else if($this->session->userdata('acctype') == 'FREE-ACCOUNT'){ ?>
				<a class="btn btn-dblue btn-lg btn-box btn-primary buttcenter button-rounded" onclick="freeClick()" href="javascript: void(0)"><span class="fa fa-file-text-o margin-right"></span>Start the Test</a>
		<?php } ?>	        	
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div>

<!-- Modal -->
<div class="modal fade" id="predicto" tabindex="-1" role="dialog" aria-labelledby="myPredicto">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <div class="modal-body gaugeContent">
        <h2>Exam Grade Predictor</h2>
        <div class="gaugeContainer">
          <img class="rotator" src="<?php echo base_url(); ?>assets/images/predictor.png">
          <img src="<?php echo base_url(); ?>assets/images/gaugeLegend.png">
        </div>
         <p>The circle badge at the centre of the subject is the predicted examination %age and grade. If the circle is 'black' it means that you have not taken enough different topic tests. Keep practicing!</p>
      </div>
      <!--<div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>-->
    </div>
  </div>
</div>

<?php $this->load->view('footer.php'); ?>

	<?php if($this->session->userdata('acctype') == 'FREE-ACCOUNT'){ ?>
	<script>
	
		function submitForm(){
		
			$("#result").html('<i><img src="<?php echo base_url(); ?>assets/images/upload.gif"> Please wait, page redirecting to PayPal...</i>');	
		
			$.post( url + 'site/upgrade_account',
			{ datax: $("form#regform").serialize() },
				function(data){
					if(data.success){		
						window.location.href=data.link;				
					}else{				
						alert(data.msg);
						$("#result").html('<button type="button" onClick="submitForm()" class="btn btn-block btn-dblue btn-lg btn-box btn-primary"><img src="<?php echo base_url(); ?>assets/images/cards.png" style="width: 200px;">&nbsp;&nbsp;&nbsp;SUBMIT & PAY</button>');
						return;
					}			    
			},"json");	
				
		}
		
	</script>
	<?php } ?>

	<script>
	
		function selectAllcheck() {
				var checked = $('#select_all').is(":checked");
				$('.checkbox1').each(function() {
					if(!this.disabled) {
						this.checked = checked;
					}
				});
		}
		
		function viewInstruction(suid, desc){
			
			var btndesc = "'" + desc + "'";
			var btnact = 'onClick="viewTpoics('+ suid +','+ btndesc +')"';		
			var btntest = '<a href="javascript: void(0)" data-toggle="modal" data-dismiss="modal" class="btn btn-block btn-dblue btn-lg btn-box btn-primary" '+ btnact +'><span class="fa fa-pencil-square-o m-icon-sm"></span>Start the Test</a>';
			
			$('.topiclaucher').modal('show');
			$('#top-title').html(desc);
			$("#check-topic").html('<i><img src="<?php echo base_url(); ?>assets/images/upload.gif"> Loading...</i>');	
			
			$.post( url + 'account/get_check_topics',
			{ subid: suid },
				function(data){
					if(data.success){		
						$('#check-topic').html(data.checktopic);				
					}else{				
						$('#check-topic').html('Failed to load tpoics, please try again.');	
					}			    
			},"json");	
		}
		
		function freeClick(){
		
			urltest = '<?php echo base_url(); ?>account/test_free_quiz';
			
			var checkedValues = $('input:checkbox:checked').map(function() {
			    return this.value;
			}).get().join('_');
			
			if(checkedValues.replace('on', '') != ''){
				$('#topic-laucher').modal('hide');
				window.open(urltest + '/' + checkedValues, '_blank');
			}else{
				alert('Please at least one topic to proceed.'); return;
			}		
		}
		
		function paidClick(){
			urltest = '<?php echo base_url(); ?>account/test_questions';	
			
			var checkedValues = $('input:checkbox:checked').map(function() {
			    return this.value;
			}).get().join('_');
			
					
			if(checkedValues.replace('on', '') != ''){
				$('#topic-laucher').modal('hide');
				window.open(urltest + '/' + checkedValues, '_blank');
			}else{
				alert('Please select at least 1 topic to proceed.'); return;
			}
		}
		
		
		
	</script>

<script>
$(window).load(function() {
	$('.flexslider').flexslider({
		animation: 'slide',
		slideshow: false,
		controlNav: false,
		startAt: 1,
		prevText: "<span class='slide-arrow glyphicon glyphicon-chevron-left'></span>",
		nextText: "<span class='slide-arrow glyphicon glyphicon-chevron-right'></span>"
	});

	// Flip Subject Average every 2 seconds
	setInterval(function() {
		$('.badger.subj-ave-flip').toggleClass('flip');
	}, 5000);
});
</script>
	</body>
</html>

