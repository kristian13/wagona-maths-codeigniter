<?php $this->load->view('header.php'); ?>
<?php $this->load->view("account/account_menu_launch.php"); ?>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/r/bs/dt-1.10.9,r-1.0.7/datatables.min.css"/>
<style>
	.pagination>li>a,
	.pagination>li>span,
	.pagination>li>a:hover,
	.pagination>li>span:hover,
	.pagination>li>a:focus,
	.pagination>li>span:focus {
		color: #319208;
	}
	.pagination>.active>a,
	.pagination>.active>span,
	.pagination>.active>a:hover,
	.pagination>.active>span:hover,
	.pagination>.active>a:focus,
	.pagination>.active>span:focus {
		color: #FFFFFF;
		background-color: #319208;
		border-color: #319208;
		cursor: default;
	}
</style>
<div id="content" class="content-row">
	<div class="container">
		<?php if($this->session->userdata('school_account') && $this->session->userdata('school_user') == 'teacher'): ?>
		<div class="row mb-20">
			<div class="col-md-4 col-md-offset-2">
				<span class="school-account-test-details"><?php echo $student_name ?> <?php echo $class_name ?></span>
			</div>
			<div class="col-md-6">
				<span class="school-account-test-details">Moving Average Percentage - <span class="<?php echo helper_get_score_class($average) ?>"><?php echo $average ?></span></span>
			</div>
		</div>
		<?php endif; ?>

		<table class="table table-striped table-bordered" id="tests-table">
			<thead>
				<tr>
					<th>Date Taken</th>
					<th>Time of Test</th>
					<th>Subject</th>
					<th>Number of Topics</th>
					<th>Number of Questions</th>
					<th>Correct Answers</th>
					<th>% Score</th>
					<th>View</th>
				</tr>
			</thead>
			<tbody>
			<?php foreach($alltest as $key => $val): ?>
			<tr>
				<td class="text-center"><?php echo date('M d, Y', strtotime($val->date_created)) ?></td>
				<td class="text-center"><?php echo date('h:i a', strtotime($val->date_created)) ?></td>
				<td class="text-center"><?php echo $val->subject_description ?></td>
				<td class="text-center"><?php echo $val->num_topics ?></td>
				<td class="text-center"><?php echo $val->num_questions ?></td>
				<td class="text-center"><?php echo $val->num_correct ?></td>
				<td class="text-center <?php echo helper_get_score_class(round($val->score_percent)) ?>">
					<strong><?php echo round($val->score_percent) ?> %</strong>
				</td>
				<td class="text-center">
					<a class="<?php echo helper_get_score_class(round($val->score_percent)) ?>" href="<?php echo base_url() . 'account/test_result/' . $val->test_id ?>" target="_blank">
						<span class="glyphicon glyphicon-search"></span>
					</a>
				</td>
			</tr>
			<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</div>
<?php $this->load->view('footer.php'); ?>
<script type="text/javascript" src="https://cdn.datatables.net/r/bs/dt-1.10.9,r-1.0.7/datatables.min.js"></script>
<script>
	$(document).ready(function() {
		$('#tests-table').DataTable({
			dom:
				"<'row'<'col-sm-6'l><'col-sm-6'f>>" +
				"<'row'<'col-sm-12'tr>>" +
				"<'row'<'col-sm-12'p>>",
			pagingType: 'full_numbers',
			language: {
				paginate: {
					first: '&laquo;',
					previous: '&lsaquo;',
					next: '&rsaquo;',
					last: '&raquo;'
				}
			},
			order: [
				[0, 'desc'], // Date Taken
				[1, 'desc']  // Time of Test
			],
			columnDefs: [
				{orderable: false, targets: 7} // Disable Ordering in View Column
			],
			initComplete: function(settings, json) {
				$('.dataTables_paginate').css('text-align', 'center');
				// $('.pagination > li > a').css('color', '#319208');
				// $('.pagination > li.active > a').css({
				// 	color: '#FFFFFF',
				// 	backgroundColor: '#319208',
				// 	borderColor: '#319208'
				// });
			}
		});
	});
</script>