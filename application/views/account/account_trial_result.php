<?php $this->load->view('header.php'); ?>
<?php 

@session_start(); 
$test = $_SESSION['free_quiz']; 

$score = 0;
for($x=0; $x<=(count($test) -1); $x++){
	if($test[$x][1] ==$test[$x][2]){
		$score++;
	}
}

$percent = round(($score / count($test)) * 100);

?>
<script>
var topic_desc = new Array();
var test_total = <?php echo count($test); ?>;
</script>

<link href="<?php echo base_url(); ?>assets/jquery-ui/jquery-ui.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/timeTo/timeTo.css">
<link href="<?php echo base_url(); ?>assets/BookBlock/css/bookblock.css" rel="stylesheet">
<script src="<?php echo base_url(); ?>assets/BookBlock/js/modernizr.custom.js"></script>

<div id="content1" class="content-row">
  <div class="rate container"></div>
	<div class="container question-container rate">
		<!--<div class="row pt-20 question-header">
			<div class="col-md-12">
				<div id="logo">
					<img title="Life in the UK" alt="Life in the UK" src="images/logo.png">
				</div>
			</div>
		</div>-->
    <div class="row"><div class="col-md-12 cter"><h2 class="lineheight">Your Score is:<span class="badger"><?php echo $percent; ?>%</span></h2></div></div>
    <div class="row search-row">
      <div class="col-md-4 col-md-offset-4">
        <form class="s-form">
          <div class="form-group">
            <input type="text" class="form-control search-box" placeholder="search..">
          </div>
        </form>
      </div>
    </div>
		<div class="row">
      <div class="col-md-12">
        <h3 class="title-margin questionTitle"> <span class="hide-show-tools toggler" data-toggle=".question-header"><span class="fa fa-chevron-circle-up"></span></span> <font id="topic_label">Topic: <?php echo str_replace(".","",preg_replace('/[0-9]+/', '', $test[0][3])); ?></font></h3>
        <div class="col-md-12 toggle pyramidAlign">
          <ul class="menu question-menu under-title text-center menu-spread question-header">
            <!--<li class="tooltip-bottom" data-toggle="tooltip" title="Settings"><a href="#" class="toggler" data-toggle=".question-settings"><span class="fa fa-list"></span></a></li>-->
            <li class="tooltip-bottom" data-toggle="tooltip" title="Calculator"><a href="#" onclick="showCalculator()" class="toggler"><span class="fa fa-calculator"></span></a></li>
            <li class="tooltip-bottom" data-toggle="tooltip" title="Workspace"><a href="#" onclick="showWorkspace()" class="toggler"><span class="fa fa-pencil-square-o"></span></a></li>
            <li class="tooltip-bottom" data-toggle="tooltip" title="Flag a question"><a href="#" class="flag-item"><span class="fa fa-flag"></span></a></li>
            <li class="tooltip-bottom" data-toggle="tooltip" title="Statistics"><a href="#" id="btn-load-piechart" data-toggle="modal" data-target="#statistics"><span class="fa fa-bar-chart-o"></span></a></li>
            <li class="tooltip-bottom" data-toggle="tooltip" title="Mute page turning sound"><a href="#" onclick="volumeUp()" class="widthFix"><span class="fa fa-volume-up"></span><span class="fa fa-volume-off hide red-col"></span></a></li>
            <!--<li class="tooltip-bottom" data-toggle="tooltip" title="Search"><a href="#" class="toggler" data-toggle=".search-row"><span class="fa fa-search"></span></a></li>-->
            <li class="tooltip-bottom" data-toggle="tooltip" title="Share"><a href="#" data-toggle="modal" data-target="#share-modal"><span class="fa fa-share-alt"></span></a></li>
            <li class="tooltip-bottom" data-toggle="tooltip" title="Hide/Show timer"><a href="#" class="hide-show-clock"><span class="fa fa-clock-o"></span></a></li>
            <li class="time-margin stopped"><a href="#"><span class="question-timer done" style="color: #319208">00:00</span></a></li>
          </ul>
        </div>
				<div class="question-title center-txt col-md-12">
          
					<span class="pagination-arrow" id="bb-nav-prev"><span class="fa fa-angle-left"></span></span>
					<!-- <span class="pagination-arrow hidden-lg hidden-md" id="bb-nav-next"><span class="fa fa-angle-right"></span></span> -->
					<ul class="pagination s-pagination question-pagination">
						<?php $z=1; for($x=0; $x<=(count($test)-1); $x++){ 
						
							if($z == 1){
								if($test[$x][1] != $test[$x][2]){ $clabel = 'wrong'; $ccheck = 'times'; }else{ $clabel = 'correct'; $ccheck = 'check'; }
								echo '<li class="current firstList"><a class="digit1" href="#">'.$z.'<span class="pagination-label pagination-label-flag"><span class="fa fa-flag"></span></span><span class="pagination-label pagination-label-'.$clabel.'"><span class="fa fa-'.$ccheck.' digit2"></span></span></a></li>';
							}	
							
							if($z > 1 && $z < 10){
								if($test[$x][1] != $test[$x][2]){ $clabel = 'wrong'; $ccheck = 'times'; }else{ $clabel = 'correct'; $ccheck = 'check'; }
								echo '<li><a class="digit1" href="#">'.$z.'<span class="pagination-label pagination-label-flag"><span class="fa fa-flag"></span></span><span class="pagination-label pagination-label-'.$clabel.'"><span class="fa fa-'.$ccheck.' digit2"></span></span></a></li>';
							}
							
							if($z > 9 && $z < count($test)){
								if($test[$x][1] != $test[$x][2]){ $clabel = 'wrong'; $ccheck = 'times'; }else{ $clabel = 'correct'; $ccheck = 'check'; }
								echo '<li><a href="#">'.$z.'<span class="pagination-label pagination-label-flag"><span class="fa fa-flag"></span></span><span class="pagination-label pagination-label-'.$clabel.'"><span class="fa fa-'.$ccheck.' digit2"></span></span></a></li>';
							}
							
							if($z == count($test)){
								if($test[$x][1] != $test[$x][2]){ $clabel = 'wrong'; $ccheck = 'times'; }else{ $clabel = 'correct'; $ccheck = 'check'; }
								echo '<li class="lastList"><a href="#">'.$z.'<span class="pagination-label pagination-label-flag"><span class="fa fa-flag"></span></span><span class="pagination-label pagination-label-'.$clabel.'"><span class="fa fa-'.$ccheck.' digit2"></span></span></a></li>';
							}
							
							$z++;
						}
					?>
					</ul>
					<span class="pagination-arrow hidden-xs hidden-sm" id="bb-nav-next"><span class="fa fa-angle-right"></span></span>
				</div>
			</div>
		</div>
		<div class="row question-main">
			<div class="sideBtn">
		        <div class="btter" data-toggle="tooltip" data-placement="right" title="Explanation">
		          <button type="button" data-toggle="modal" data-target="#explanation-modal" class="btter expl btn btn-lg btn-block btn-dgreen btn-small"><span class="fa fa-check-square-o m-icon-sm"></span></button>
		          <button type="button" data-toggle="modal" data-target="#video-modal" class="btter vid btn btn-lg btn-block btn-dgreen btn-small"><span class="fa fa-youtube-play m-icon-sm"></span></button>
		          <button href="#" data-toggle="modal" data-target="#startnewTest" class="btter stnew btn btn-lg btn-block btn-dgreen btn-small"><span class="fa fa-pencil-square-o m-icon-sm"></span></button>
		        </div>
		      </div>
			<div id="bb-bookblock" class="bb-bookblock">
				
			<?php 
        $z=1; 
        for($x = 0; $x < count($test); $x++) { 
          $current_topicdesc  = str_replace(".","",preg_replace('/[0-9]+/', '', $test[$x][3]));
      ?>
        <script>
          topic_desc[<?php echo $x + 1; ?>] = "<?php echo $current_topicdesc; ?>";
        </script>
				<div class="question-item bb-item">
					<div class="col-md-7">
						<div class="question-text">
							<span class="question-label"><?php echo $z; ?></span>
							<p><?php echo trim($test[$x][5]); ?></p>
							<div class="question-illustration">
							<?php if(strlen($test[$x][4]) > 0){ ?>
								<img src="<?php echo base_url().'admin/data/test_quest/'.$test[$x][4]; ?>" />
							<?php } ?>
							</div>
						</div>
					</div>
					<div class="col-md-5 borx">
						<div class="question-choice" style="padding-left: 15px;">
							<form>
								<div class="radio">
						      <label>
						      							      	
						      	<?php if($test[$x][2] == 'a'){echo '<span class="choice-stat choice-stat-correct"><span class="fa fa-check"></span></span>';} ?>	
						      	<?php if($test[$x][1] == 'a' && $test[$x][2] != 'a' ){echo '<span class="choice-stat choice-stat-wrong"><span class="fa fa-times"></span></span>';} ?>						      		
						      	
						        <input type="radio" class="question-choice-radio" name="question-choice-radio" data-prev="0">
						        <span class="question-choice-letter <?php if($test[$x][1] == 'a'){echo 'hover';} ?>">A</span><span class="question-choice-text"><?php echo strip_tags($test[$x][6]); ?></span>
						      </label>
						     </div>
						     <div class="radio">
						      <label>
						      
						      	<?php if($test[$x][2] == 'b'){echo '<span class="choice-stat choice-stat-correct"><span class="fa fa-check"></span></span>';} ?>	
						      	<?php if($test[$x][1] == 'b' && $test[$x][2] != 'b' ){echo '<span class="choice-stat choice-stat-wrong"><span class="fa fa-times"></span></span>';} ?>						      		
						      	
						        <input type="radio" class="question-choice-radio" name="question-choice-radio" data-prev="0">
						        <span class="question-choice-letter <?php if($test[$x][1] == 'b'){echo 'hover';} ?>">B</span><span class="question-choice-text"><?php echo strip_tags($test[$x][7]); ?></span>
						      </label>
						     </div>
						     <div class="radio">
						      <label>
						      
						      	<?php if($test[$x][2] == 'c'){echo '<span class="choice-stat choice-stat-correct"><span class="fa fa-check"></span></span>';} ?>	
						      	<?php if($test[$x][1] == 'c' && $test[$x][2] != 'c' ){echo '<span class="choice-stat choice-stat-wrong"><span class="fa fa-times"></span></span>';} ?>						      		
						      	
						        <input type="radio" class="question-choice-radio" name="question-choice-radio" data-prev="0">
						        <span class="question-choice-letter <?php if($test[$x][1] == 'c'){echo 'hover';} ?>">C</span><span class="question-choice-text"><?php echo strip_tags($test[$x][8]); ?></span>
						      </label>
						     </div>
						     <div class="radio">
						      <label>
						      
						      	<?php if($test[$x][2] == 'd'){echo '<span class="choice-stat choice-stat-correct"><span class="fa fa-check"></span></span>';} ?>	
						      	<?php if($test[$x][1] == 'd' && $test[$x][2] != 'd' ){echo '<span class="choice-stat choice-stat-wrong"><span class="fa fa-times"></span></span>';} ?>						      		
						      	
						        <input type="radio" class="question-choice-radio" name="question-choice-radio" data-prev="0">
						        <span class="question-choice-letter <?php if($test[$x][1] == 'd'){echo 'hover';} ?>">D</span><span class="question-choice-text"><?php echo strip_tags($test[$x][9]); ?></span>
						      </label>
						     </div>
						     <div class="radio">
						      <label>
						      
						      	<?php if($test[$x][2] == 'e'){echo '<span class="choice-stat choice-stat-correct"><span class="fa fa-check"></span></span>';} ?>	
						      	<?php if($test[$x][1] == 'e' && $test[$x][2] != 'e' ){echo '<span class="choice-stat choice-stat-wrong"><span class="fa fa-times"></span></span>';} ?>						      		
						      	
						        <input type="radio" class="question-choice-radio" name="question-choice-radio" data-prev="0">
						        <span class="question-choice-letter <?php if($test[$x][1] == 'e'){echo 'hover';} ?>">E</span><span class="question-choice-text"><?php echo strip_tags($test[$x][10]); ?></span>
						      </label>
						     </div>
							</form>
						</div>
					</div>
				</div>
			<?php $z++; } ?>
			
			</div>
			<nav class="bookblock-nav hidden">
        <span class="bb-current">1</span>
        <span>2</span>
        <span>3</span>
        <span>4</span>
        <span>5</span>
        <span>6</span>
        <span>7</span>
        <span>8</span>
        <span>9</span>
        <span>10</span>
        <span>11</span>
        <span>12</span>
        <span>13</span>
        <span>14</span>
        <span>15</span>
        <span>16</span>
        <span>17</span>
        <span>18</span>
        <span>19</span>
        <span>20</span>
        <span>21</span>
        <span>22</span>
        <span>23</span>
        <span>24</span>
      </nav>
		</div>

    <div class="question-settings">
      <div class="header-block clearfix">
        <div clss="row">
          <div class="col-md-8">
            <h4 class="header-block-title ib"><span class="fa fa-cog m-icon-sm"></span>Settings</h4>
          </div>
          <div class="col-md-4">
            <div class="right-txt">
              <a class="close-el" data-close=".question-settings">x</a>
            </div>
          </div>
        </div>
      </div>
      <div class="settings-content">
        <div class="row">
          <div class="col-md-12">
            <div class="inner-tab">
              <div class="inner-tab-header clearfix">
                <h5><span class="fa fa-edit m-icon-sm"></span>Configure your settings</h5>
                <ul class="nav nav-tabs pull-right right-txt">
                  <li class="active"><a data-toggle="tab" role="tab" href="#mode-tab"><span class="fa fa-th-large m-icon-sm"></span>Mode</a></li>
                  <li class=""><a data-toggle="tab" role="tab" href="#reset-tab"><span class="fa fa-refresh m-icon-sm"></span>Reset</a></li>
                  <li class=""><a data-toggle="tab" role="tab" href="#report-tab"><span class="fa fa-bar-chart-o m-icon-sm"></span>Report</a></li>
                </ul>
              </div>
              <div class="tab-content">
                <div id="mode-tab" class="tab-pane active">
                  <div class="center-txt">
                    <div class="mb-30">
                      <h3 class="ubuntu_400 blue-col title-border"><span class="fa fa-th-large m-icon-sm"></span>Choose the Learning Mode you want to learn</h3>
                    </div>
                    <div class="btn-group">
                      <button type="button" class="btn btn-lg btn-dblue"><span class="fa fa-clock-o m-icon-sm"></span>Timed</button>
                      <button type="button" class="btn btn-lg btn-dblue"><span class="fa fa-clock-o m-icon-sm"></span>Not timed</button>
                    </div>
                  </div>
                </div>
                <div id="reset-tab" class="tab-pane">
                  <div class="center-txt">
                    <div class="mb-30">
                      <h3 class="ubuntu_400 blue-col title-border"><span class="fa fa-refresh m-icon-sm"></span>Do you want to restart the test?</h3>
                    </div>
                    <div class="btn-group">
                      <button type="button" class="btn btn-lg btn-dblue"><span class="fa fa fa-check m-icon-sm"></span>Yes</button>
                      <button type="button" class="btn btn-lg btn-dblue"><span class="fa fa fa-times m-icon-sm"></span>No</button>
                    </div>
                  </div>
                </div>
                <div id="report-tab" class="tab-pane">
                  <div class="center-txt">
                    <div class="mb-30">
                      <h3 class="ubuntu_400 blue-col title-border"><span class="fa fa-bar-chart-o m-icon-sm"></span>Report</h3>
                    </div>
                    <div class="table-pad table-pad-gray">
                      <table class="table table-responsive s-table">
                        <tr>
                          <td class="left-txt"><strong>Subject Title</strong></td>
                          <td><span class="red-col">July-21</span></td>
                          <td><span class="red-col">July-23</span></td>
                          <td><span class="red-col">July-24</span></td>
                          <td><span class="red-col">July-27</span></td>
                          <td><span class="red-col">July-30</span></td>
                          <td><strong>Average</strong></td>
                        </tr>
                        <tr>
                          <td class="left-txt">Monarchy: Government and Politics</td>
                          <td>40%</td>
                          <td>40%</td>
                          <td>40%</td>
                          <td>40%</td>
                          <td>40%</td>
                          <td><strong>40%</strong></td>
                        </tr>
                        <tr>
                          <td class="left-txt">Geography & History</td>
                          <td>40%</td>
                          <td>40%</td>
                          <td>40%</td>
                          <td>40%</td>
                          <td>40%</td>
                          <td><strong>40%</strong></td>
                        </tr>
                        <tr>
                          <td class="left-txt">Culture, Customs & Celebrations</td>
                          <td>40%</td>
                          <td>40%</td>
                          <td>40%</td>
                          <td>40%</td>
                          <td>40%</td>
                          <td><strong>40%</strong></td>
                        </tr>
                        <tr>
                          <td class="left-txt">International Relations</td>
                          <td>40%</td>
                          <td>40%</td>
                          <td>40%</td>
                          <td>40%</td>
                          <td>40%</td>
                          <td><strong>40%</strong></td>
                        </tr>
                        <tr>
                          <td class="left-txt">Religions & Beliefs</td>
                          <td>40%</td>
                          <td>40%</td>
                          <td>40%</td>
                          <td>40%</td>
                          <td>40%</td>
                          <td><strong>40%</strong></td>
                        </tr>
                        <tr>
                          <td class="left-txt">Law, Rules & Regulations</td>
                          <td>40%</td>
                          <td>40%</td>
                          <td>40%</td>
                          <td>40%</td>
                          <td>40%</td>
                          <td><strong>40%</strong></td>
                        </tr>
                        <tr>
                          <td class="left-txt">Employment, Work & Benefits</td>
                          <td>40%</td>
                          <td>40%</td>
                          <td>40%</td>
                          <td>40%</td>
                          <td>40%</td>
                          <td><strong>40%</strong></td>
                        </tr>
                      </table>
                      <h3 class="right-txt mb-0"><small class="ubuntu_400">Overall Average:</small> <span class="red-col">60%</span></h3>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
	</div><!-- container -->
</div>

<?php $this->load->view('footer.php'); ?>

<!-- modals -->

  <div class="modal fade" id="startnewTest" role="dialog">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <p class="questionPick">Are you sure you want to quit the results page and start a new test?</p>
        </div>
        <div class="modal-footer choiceYesNo">
          <a href="<?php echo base_url(); ?>account/dashboard" class="btn btn-success">Yes</a>
          <a class="btn btn-danger" data-dismiss="modal">No</a>
        </div>
      </div>
    </div>
  </div>


<div id="explanation-modal" class="modal fade s-modal in">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <a data-dismiss="modal" class="close"><span aria-hidden="true">×</span><span class="sr-only">Close</span></a>
        <h3 class="cter"><i class="fa fa-check-square-o marg-n35"></i>Explanation</h3>
      </div> 
      <div class="modal-body">
        <div class="explanation-row">
          <span class="question-choice-letter" id="miscon-letter">A</span>
          <span class="question-choice-text xplainSmall black-col" id="text-optn"></span>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div style="display: block;" class="feedback-item well" id="feedback-item-1">
              <p id="miscon-desc"></p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div id="video-modal" class="modal fade s-modal in">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <a data-dismiss="modal" class="close"><span aria-hidden="true">×</span><span class="sr-only">Close</span></a>
        <h3><span class="fa fa-share-alt m-icon-sm"></span>Video</h3>
      </div> 
      <div class="modal-body">
          <iframe id="yt_video" width="560" height="315" src="https://www.youtube.com/embed/SkfcqLrU0fY?enablejsapi=1" frameborder="0" allowfullscreen></iframe>
      </div>
    </div>
  </div>
</div>

<div id="timer-modal" class="modal fade s-modal in">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <a data-dismiss="modal" class="close"><span aria-hidden="true">×</span><span class="sr-only">Close</span></a>
      	<div class="success-modal-body center-txt">
      		<span class="circ-icon circ-icon-lg gray-bg">
				<span class="fa fa-clock-o"></span>
			</span>
			<h1 class="museo300 border-bottom m-0 mb-20 pb-10"><big>Time's Up!</big></h1>
			<p class="mb-30">
				<big>You have reached the time limit set for the Test. Continue to view your result.</big>
			</p>
			<a class="btn btn-dblue btn-lg btn-box btn-primary" href="#">Continue</a>
      	</div>
      </div>
    </div>
  </div>
</div>

<div id="share-modal" class="modal fade s-modal in">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <a data-dismiss="modal" class="close"><span aria-hidden="true">×</span><span class="sr-only">Close</span></a>
        <h3><span class="fa fa-share-alt m-icon-sm"></span>Share</h3>
      </div> 
      <div class="modal-body">
        <ul class="rrssb-buttons clearfix">
          <li class="email">
              <a href="mailto:?subject=Wagona Maths&amp;body=<?php echo $url; ?>">
                  <span class="icon">
                      <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" width="28px" height="28px" viewBox="0 0 28 28" enable-background="new 0 0 28 28" xml:space="preserve"><g><path d="M20.111 26.147c-2.336 1.051-4.361 1.401-7.125 1.401c-6.462 0-12.146-4.633-12.146-12.265 c0-7.94 5.762-14.833 14.561-14.833c6.853 0 11.8 4.7 11.8 11.252c0 5.684-3.194 9.265-7.399 9.3 c-1.829 0-3.153-0.934-3.347-2.997h-0.077c-1.208 1.986-2.96 2.997-5.023 2.997c-2.532 0-4.361-1.868-4.361-5.062 c0-4.749 3.504-9.071 9.111-9.071c1.713 0 3.7 0.4 4.6 0.973l-1.169 7.203c-0.388 2.298-0.116 3.3 1 3.4 c1.673 0 3.773-2.102 3.773-6.58c0-5.061-3.27-8.994-9.303-8.994c-5.957 0-11.175 4.673-11.175 12.1 c0 6.5 4.2 10.2 10 10.201c1.986 0 4.089-0.43 5.646-1.245L20.111 26.147z M16.646 10.1 c-0.311-0.078-0.701-0.155-1.207-0.155c-2.571 0-4.595 2.53-4.595 5.529c0 1.5 0.7 2.4 1.9 2.4 c1.441 0 2.959-1.828 3.311-4.087L16.646 10.068z"/></g></svg>
                  </span>
                  <span class="text">email</span>
              </a>
          </li>
          <li class="facebook">
              <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $url; ?>" class="popup">
                  <span class="icon">
                      <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="28px" height="28px" viewBox="0 0 28 28" enable-background="new 0 0 28 28" xml:space="preserve">
                          <path d="M27.825,4.783c0-2.427-2.182-4.608-4.608-4.608H4.783c-2.422,0-4.608,2.182-4.608,4.608v18.434
                              c0,2.427,2.181,4.608,4.608,4.608H14V17.379h-3.379v-4.608H14v-1.795c0-3.089,2.335-5.885,5.192-5.885h3.718v4.608h-3.726
                              c-0.408,0-0.884,0.492-0.884,1.236v1.836h4.609v4.608h-4.609v10.446h4.916c2.422,0,4.608-2.188,4.608-4.608V4.783z"/>
                      </svg>
                  </span>
                  <span class="text">facebook</span>
              </a>
          </li>
          <li class="twitter">
              <a href="http://twitter.com/home?status=Life%20in%20the%20UK%20<?php echo $url; ?>" class="popup">
                  <span class="icon">
                      <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                           width="28px" height="28px" viewBox="0 0 28 28" enable-background="new 0 0 28 28" xml:space="preserve">
                      <path d="M24.253,8.756C24.689,17.08,18.297,24.182,9.97,24.62c-3.122,0.162-6.219-0.646-8.861-2.32
                          c2.703,0.179,5.376-0.648,7.508-2.321c-2.072-0.247-3.818-1.661-4.489-3.638c0.801,0.128,1.62,0.076,2.399-0.155
                          C4.045,15.72,2.215,13.6,2.115,11.077c0.688,0.275,1.426,0.407,2.168,0.386c-2.135-1.65-2.729-4.621-1.394-6.965
                          C5.575,7.816,9.54,9.84,13.803,10.071c-0.842-2.739,0.694-5.64,3.434-6.482c2.018-0.623,4.212,0.044,5.546,1.683
                          c1.186-0.213,2.318-0.662,3.329-1.317c-0.385,1.256-1.247,2.312-2.399,2.942c1.048-0.106,2.069-0.394,3.019-0.851
                          C26.275,7.229,25.39,8.196,24.253,8.756z"/>
                      </svg>
                  </span>
                  <span class="text">twitter</span>
              </a>
          </li>
      </ul>
      </div>
      <audio id="audio" style="display:none;" src="<?php echo base_url(); ?>assets/turnpage.mp3" >
    </div>
  </div>
</div>

<div class="modal" id="firstPageWarning" role="dialog">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title col-white">Wagona Says:</h4>
      </div>
      <div class="modal-body">
        <p class="questionPick">This is the first page</p>
      </div>
      <div class="modal-footer choiceYesNo">
        <a href="#" data-dismiss="modal" class="btn btn-success">OK</a>
      </div>
    </div>
  </div>
</div>

<div class="modal" id="lastPageWarning" role="dialog">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Wagona Says:</h4>
      </div>
      <div class="modal-body">
        <p class="questionPick">This is the last page</p>
      </div>
      <div class="modal-footer choiceYesNo">
        <a href="#" data-dismiss="modal" class="btn btn-success">OK</a>
      </div>
    </div>
  </div>
</div>

<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/rrssb/css/rrssb.css">
<script type="text/javascript" src="<?php echo base_url(); ?>assets/timeTo/jquery.timeTo.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/BookBlock/js/jquerypp.custom.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/BookBlock/js/jquery.bookblock.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/rrssb/js/rrssb.min.js"></script>

<script type="text/javascript">

  var counter = 0;
	
  var miscon = [
	<?php
		$index = 1;
		for($x=0; $x<=(count($test) - 1); $x++){
			
			if($test[$x][2] == 'a'){$ansdesc = $test[$x][6];}
			if($test[$x][2] == 'b'){$ansdesc = $test[$x][7];}
			if($test[$x][2] == 'c'){$ansdesc = $test[$x][8];}
			if($test[$x][2] == 'd'){$ansdesc = $test[$x][9];}
			if($test[$x][2] == 'e'){$ansdesc = $test[$x][10];}
			
			echo '["'.addslashes($test[$x][11]).'", "'.strtoupper($test[$x][2]).'", "'.strip_tags($ansdesc).'", "'.str_replace(".","",preg_replace('/[0-9]+/', '', $test[$x][3])).'"],';
			$index++;
		}
	?>];
	
	$('#miscon-desc').html(miscon[0][0]);
	$('#text-optn').html(miscon[0][2]);
	$('#miscon-letter').html(miscon[0][1]);
	$('#dyna-topic').html(miscon[0][3]);

	function play(){
		var audio = document.getElementById("audio");
		audio.play();
	}

	function mark() {
		$(".question-pagination > li.current").addClass('marked');
	}

    //flag a question
    $('.flag-item').click(function(){
      $('.question-pagination > li.current').toggleClass('flagged');
    });

    //share
    $('#share-modal').on('shown.bs.modal', function (e) {
      $(window).resize();
    });
</script>

<script>
  var Page = (function() {
    var $is_flipping = false;
    var $bookblock = $('#bb-bookblock');
    var $nav_next = $('#bb-nav-next');
    var $nav_prev = $('#bb-nav-prev');
    var $nav = $('.question-pagination > li > a');
    var $topic_label = $('#topic_label');
    var $current_page = 1;

    // Initalize Bookblock
    var init = function() {
      console.log('PAGE');
      $('#bb-bookblock').bookblock({
        speed: 1000,
        shadowSides: 0.8,
        shadowFlip: 0.4,
        onBeforeFlip: function() {
          $is_flipping = true;
        },
        onEndFlip: function() {
          $is_flipping = false;
          $topic_label.html("Topic: " + topic_desc[$current_page]);
        }
      });

      initEvents();
    };

    // Initialize Events
    var initEvents = function() {

      var $slides = $bookblock.children();

      // Keyboard Events
      $(document).keydown(function(e) {
        var keyCode = e.keyCode || e.which,
            arrow = {
              left: 37,
              right: 39
            };

        switch (keyCode) {
          case arrow.left:
            pagePrev();
            break;
          case arrow.right:
            pageNext();
            break;
        }
      });

      // Navigation Events
      $nav_next.on('click touchstart', function() {
        pageNext();
      });
      $nav_prev.on('click touchstart', function() {
        pagePrev();
      });
      $nav.each(function(i) {
        $(this).on('click touchstart', function() {
          var position = $(this).parent('li').index() + 1;
          var $dot = $(this);
          pageJump(position, $nav.parent('li'), $dot.parent('li'));
        });
      });

      // Swipe Events
      $slides.on( {
        'swipeleft': function(e) {
          pageNext();
        },
        'swiperight': function(e) {
          pagePrev();
        }
      });
    };

    // Go to Next Page
    var pageNext = function() {
      // Check if it's currently flipping
      if($is_flipping) {
        return false;
      }

      // Set Current Page
      if($current_page < test_total) {
        $current_page++;
      }
      else {
        return false;
      }

      $bookblock.bookblock('next');
      $(".question-pagination > li.current").removeClass('current').next().addClass('current');
      playSound();

      return false;
    };

    // Go to Previous Page
    var pagePrev = function() {
      // Check if it's currently flipping
      if($is_flipping) {
        return false;
      }

      // Set Current Page
      if($current_page > 1) {
        $current_page--
      }
      else {
        return false;
      }

      $bookblock.bookblock('prev');
      $(".question-pagination > li.current").removeClass('current').prev().addClass('current');
      playSound();

      return false;
    }

    // Go to Page Number
    var pageJump = function(position, nav, dot) {
      // Check if it's currently flipping
      if($is_flipping) {
        return false;
      }

      // Set Current Page
      if($current_page != position) {
        $current_page = position;
      }
      else {
        return false;
      }

      $bookblock.bookblock('jump', position);
      nav.removeClass('current');
      dot.addClass('current');
      playSound();

      return false;
    }

    // Play Flipping Page Sound
    var playSound = function() {
      var audio = document.getElementById("audio");
      audio.play();
    };

    return {
      init: init
    }
  })();
</script>

<!-- workspace drawing using mouse -->

<script type="text/javascript">
  $(window).resize(function(){
    $('#bb-bookblock').css('width','auto');
  });
  Page.init();
  $('[data-toggle="tooltip"]').tooltip();

  // Youtube Video
  var tag = document.createElement('script');
  tag.src = "https://www.youtube.com/iframe_api";
  var firstScriptTag = document.getElementsByTagName('script')[0];
  firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

  var yt_video;
  function onYouTubeIframeAPIReady() {
    yt_video = new YT.Player('yt_video');
  }

  $('#video-modal').on('shown.bs.modal', function (e) {
    yt_video.playVideo();
  });
  $('#video-modal').on('hide.bs.modal', function (e) {
    yt_video.stopVideo();
  });
</script>