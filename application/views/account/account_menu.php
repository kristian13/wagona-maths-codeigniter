	
	<div class="container navi">
		<nav role="navigation" class="navbar navbar-default navbar-static-top" style="background: #e6e6e6; border-radius:30px;">
			<div style="background: #e6e6e6; border-radius:30px;" class="container navlister">
			  <!-- <p class="navbar-text"><img src="<?php echo base_url(); ?>assets/images/pieBlue.png"><a href="<?php echo base_url(); ?>account/dashboard">Dashboard</a></p> -->
			  <p class="navbar-text"><img src="<?php echo base_url(); ?>assets/images/hcircle.png"><a href="#" data-toggle="modal" data-target="#predicto">Exam Predictor</a></p>
			  <?php 
			  	$commulative_average = commulative_average();
			  	if($this->session->userdata('acctype') == 'PAID-ACCOUNT'): 
			  ?>
			  <p class="navbar-text"><img src="<?php echo base_url(); ?>assets/images/greenPad.png"> <a href="<?php echo base_url(); ?>account/account_mytests">Tests History <span class="badger takenTest"><?php count_my_test(); ?></span></a></p>
			  <p class="navbar-text"><img class="margin-right" src="<?php echo base_url(); ?>assets/images/checklist.png">Last 5 Average <span class="red-bg badger <?php echo helper_get_score_bg_class($commulative_average); ?>"> <?php echo $commulative_average . '%'; ?> </span</p>
			  <?php endif; ?>
			  
			  <p class="navbar-text"><img src="<?php echo base_url(); ?>assets/images/target.png"> Minimum Target <span class="badger sticker score-green-bg">75%</span></p>
			  
			  <div style="margin-top: 7px;" class="btn-group navbar-right">
				  
			  	  <button data-toggle="dropdown" class="btn btn-primary dropdown-toggle" type="button">
				    <span class="glyphicon glyphicon-user"></span> <?php echo $this->session->userdata('email'); ?> <span class="caret"></span>
				  </button>
				  
				  <ul role="menu" class="dropdown-menu">
				    <li><a href="#">Profile</a></li>
				    <li class="divider"></li>
				    <li><a href="<?php echo base_url(); ?>site/logout">Logout</a></li>
				  </ul>
			  </div>
			  
			  <?php if($this->session->userdata('acctype') == "FREE-ACCOUNT"){ ?>
			  <div class="btn-group navbar-right" style="margin-top: 7px; margin-right: 10px;">
					
					<form id="regform" action="<?php echo base_url(); ?>site/upgrade_account" method="post">
					 
					  	<input type="hidden" name="cmd" value="_xclick" /> 
					    <input type="hidden" name="no_note" value="1" />
					    <input type="hidden" name="lc" value="UK" />
					    <input type="hidden" name="currency_code" value="GBP" />
					    <input type="hidden" name="bn" value="PP-BuyNowBF:btn_buynow_LG.gif:NonHostedGuest" />
					    
						<button type="button" onClick="submitForm()" class="btn btn-success"><span class="glyphicon glyphicon-signal"></span> Upgrade Account Now </button>
						  
					</form>

			 </div>
			 <?php } ?>
			 
			</div>
		</nav>
	</div>