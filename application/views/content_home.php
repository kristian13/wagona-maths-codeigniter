<?php $this->load->view('header.php'); ?>

	<div id="content" class="content-row animated2">

	<div class="thumb container">
		<div class="row">
			<div class="col-md-12">
				<div class="row">
					<div class="col-md-4 col-sm-4">
						<div class="gray-box active mob-mb-20 redirect-link" data-link="<?php echo base_url(); ?>site/pricing">
							<div class="gb-badge">
								<img class="thumbcard" src="<?php echo base_url(); ?>assets/images/check.gif"/>
							</div>
							<div class="gb-title">
								<h2><div id="example2">Free Trial</div></h2>
                </h2>
							</div>
              <div class="row heightfix">
                <div class="col-md-12 col-sm-12 col-xs-12">
                  <a class="btn thumbtn btn-dred">Try Now</a>
                </div>
              </div>
						</div>
					</div>
					<div class="col-md-4 col-sm-4">
						<div class="gray-box mob-mb-20 redirect-link" data-link="<?php echo base_url(); ?>site/pricing">
							<div class="gb-badge">
								<img class="thumbcard" src="<?php echo base_url(); ?>assets/images/dollar.gif"/>
							</div>
							<div class="gb-title">
                <h2>Special Offer</h2>
							</div>
              <div class="row heightfix">
                <div class="col-md-12 col-sm-12 col-xs-12">
                  <a class="btn thumbtn btn-dred">Try Now</a>
                </div>
              </div>
						</div>
					</div>
					<div class="col-md-4 col-sm-4 last">
						<div class="gray-box mob-mb-20">
							<div class="gb-badge">
								<img class="thumbcard" src="<?php echo base_url(); ?>assets/images/thumbs.gif"/>
							</div>
							<div class="gb-title">
								<h2>Our Guarantee</h2>
							</div>
              <div class="row heightfix">
                <div class="col-md-12 col-sm-12 col-xs-12">
                  <a class="btn thumbtn btn-dred">Try Now</a>
                </div>
              </div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>	

	<?php $this->load->view('footer.php'); ?>
	
	<script type="text/javascript">
		$(window).load(function(){
		  $('.home-slider').flexslider({
		    animation: "fade",
		    prevText: '<span class="slide-arrow glyphicon glyphicon-chevron-left"></span>',
		    nextText: '<span class="slide-arrow glyphicon glyphicon-chevron-right"></span>',
		    slideshowSpeed: 10000,
		    start: function(slider){
		      $('body').removeClass('loading');
		    },
		    after: function(slider) {
		     if ($('.slides > li').hasClass('flex-active-slide')) {
		     $('.flex-active-slide .boybnner').addClass('fadeInRight');
		     $('.flex-active-slide .bagbnner').addClass('fadeIn');
		    $('.flex-active-slide .imagebnner').addClass('fadeInRight');
		      }
		    }  
		  });
		
		   // $('#flexslider').flexslider({
		   //     easing: "swing",  
		   //     animation: "slide",
		   //     animationSpeed: 600,
		   //     startAt: 0,
		   //     initDelay: 0,
		   //     controlNav: true,
		   //     directionNav: true,
		   //     pausePlay: true,
		   //     pauseText: 'Pause',
		   //     playText: 'Play'
		   // }); 
		
		  //gray box switch actives for 2 seconds
		  var boxCount = 1;
		  var boxInterval = 0;
		  var boxSize = $('.gray-box').size();
		  $('.gray-box').each(function(){
		  	$(this).attr('data-count',boxCount);
		  	boxCount++;
		  });
		
		  setInterval(function(){
		    boxInterval++
		  	if(boxInterval <= boxSize){
		  		$('.gray-box').removeClass('active');
		  		$('div[data-count="'+boxInterval+'"]').addClass('active');
		  	}
		  	else{
		  		boxInterval = 1;
		      $('.gray-box').removeClass('active');
		      $('div[data-count="'+boxInterval+'"]').addClass('active');
		  	}
		  },4000);
		
		});
		
		
		
		$(document).ready(function(){
		
		  if ($('.slides > li').hasClass('flex-active-slide')) {
		  $('.flex-active-slide .boybnner').addClass('fadeInRight');
		
		}
		//	$('body').prepend('<div class="video-background"></div>');
		//	$('.video-background').videobackground({
		//		videoSource: [['videos/uk-flag.mp4', 'video/mp4'],
		//			['videos/uk-flag.webm', 'video/webm'], 
		//			['videos/uk-flag.ogv', 'video/ogg']], 
		//		controlPosition: '#main',
		//		loop: true,
		//		autoplay: true,
		//		poster: '',
		//		loadedCallback: function() {
		//			$(this).videobackground('mute');
		//		}
		//	});
		});
		
		$('.fraction').each(function(key, value) {
		    $this = $(this)
		    var split = $this.html().split("/")
		    if( split.length == 2 ){
		        $this.html('<div class="absolute"><span class="top">'+split[0]+'</span><span class="bottom">'+split[1]+'</span></div>');
		    }    
		});
		
		
	</script>
	
  </body>
</html>


   