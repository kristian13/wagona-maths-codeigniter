<?php $this->load->view('header.php'); ?>

<div class="container">
  <div class="divAbout">
    <p class="about-title">About Us</p>
    <p>
      Zimbabweans are well known all over the world for being highly educated and smart. Teaching and learning of mathematics is a big
      part of this matrix. In order to maintain our position as a leading nation in education, we need to provide resources to our children and move with the times. We are offering a unique and highly promising opportunity for children to learn Maths the way it has never been taught before — one that can equal (or even surpass) that of a traditional Mathematics curriculum. We seek to debunk the myth that quality education has to be expensive. One of our key values is that education has to be affordable, enjoyable, challenging and rewarding. Our platform is not a substitute for teachers but compliments the great work that they are doing in schools sometimes with very limited resources. Wagona Maths can host thousands of subscribers who should be able to log onto the platform, take tests and get instant feedback.
    </p>
    <div class="rect-el-full lgray-bg removepad remarg">
      <p>
        <p class="about-title">Our Ethos</p>
        <p>
          African Education by African Educators (ASAP - African Solutions to African Problems)
          <ul>
            <li>We believe as Africans we need to take charge of our destiny by crafting our own education curricula which reflects our values, morals and aspirations.</li>
            <li>We believe Maths is more than a subject. It is an essential tool in honing a child’s critical thinking skills.</li>
            <li>We also believe that every child deserves the right to mathematical education wherever they may be.</li>
            <li>By harnessing the power of technology and marrying it with the discipline of Mathematics, we can strengthen a child’s chance to a secure, successful and thriving future.</li>
            <li>Expensive education does not always equate to quality education. This is why we dedicated ourselves to making a difference by providing an alternative learning and teaching platform called Wagona Maths.</li>
            <li>With the help of modern technology and teaching experts, we have contributed to the way e-learning works making it effective and affordable.</li>
          </ul>
        </p>
      </p>
    </div>
    <div class="rect-el-full lgray-bg removepad remarg">
      <p>
        <p class="about-title">Our Mission</p>
        <p>
          <ul>
            <li>To provide Zimbabwean children with a chance to become masters of Mathematics through the use of Wagona Maths — our very own online platform that exhaustively teaches the subject in a fresher and much more effective way.</li>
            <li>To give Zimbabwean children the gift of an educational platform that is both compelling and affordable.</li>
            <li>To offer Zimbabwean educators an opportunity to engage their students in a more effective and efficient way.</li>
          </ul>
        </p>
      </p>
    </div>
  </div>
</div>

<?php $this->load->view('footer.php'); ?>