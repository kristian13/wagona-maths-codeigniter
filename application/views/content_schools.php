<?php $this->load->view('header.php'); ?>

<div class="container">
  <div class="divAbout">
    <p class="about-title">Schools</p>
    <p>
      Wagona Maths is a subscription website specifically designed for teaching and learning of mathematics in Zimbabwe. Aptly called “Wagona Maths” which means you have mastered mathematics, the E-learning platform is the first of its kind because it is specifically designed for the Zimbabwean curriculum. It provides a fully interactive online learning resource that is available 24 hours a day, 7 days a week from school or from home.
    </p>
    <div class="row" style="margin-top: 50px;">
      <div class="col-md-12">
        <p class="about-title">What Sets Us Apart?</p>
        <img src="<?php echo base_url() . 'assets/images/schools_tab_1.jpg' ?>" style="float: left; margin-right: 50px;">
        <p style="margin-top: 70px;">
          There are many factors that set us apart from other E-learning platform providers and from the traditional classroom setting. Among these are our <strong><span class="score-black">B</span><span class="score-red">R</span><span class="score-amber">A</span><span class="score-green">G</span> System and Mathematics Diagnostic Tool</strong>. The BRAG System is our very own creation that gives real time data on every child’s performance and tracks progress, highlighting areas of concern. The Mathematics Diagnostic Tool is Wagona Maths’ assessment tool that identifies the weaknesses and strengths of every student. Using these tools, Wagona Maths is able to prescribe the right ways to resolve the areas of weakness whilst at the same time consolidating strong ones.
        </p>
      </div>
    </div>
    <div class="row" style="margin-top: 50px;">
      <div class="col-md-12">
        <p class="about-title">Merging Technology and Education</p>
        <p>
          Wagona Maths is an excellent accompaniment and complement to the conventional class set-up. It is a powerful tool to build on what is being taught in school. Our core philosophy is that, given the proper environment, every child is capable of learning and getting good results in maths. On this principle, we have built the ultimate focused learning environment, free of distractions and packed with meaningful challenges: a place where every moment is spent learning. Best of all, thanks to Wagona’s tracking tools, you will never miss a second of your students' exciting learning journey. We can give you information that matters, i.e exactly which questions your students struggled with, how they are progressing over time and how much knowledge they have genuinely understood and retained.
        </p>
        <p>
          Wagona maths offers learning tools for students as well as comprehensive management and reporting tools for head teachers/administrators, teachers and parents.
        </p>
      </div>
    </div>
    <div class="row" style="margin-top: 50px;">
      <div class="col-md-12">
        <p class="about-title">Head Teachers</p>
        <p>
          Think of our administrator account as your school's Wagona Maths control desk. The account can be used for managing teacher and student access to Wagona Maths, as well as for monitoring usage throughout the school year. Our admin account also offers a wide range of reporting tools such as markbooks and individual student reports. We offer detailed reports covering areas like performance, improvement and curriculum mastery. You can view information at the school, year, classroom or individual level.
        </p>
        <p>
          Wagona maths offers learning tools for students as well as comprehensive management and reporting tools for head teachers/administrators, teachers and parents.
        </p>
      </div>
    </div>
    <div class="row" style="margin-top: 30px;">
      <div class="col-md-6">
        <img src="<?php echo base_url() . 'assets/images/schools_tab_2.jpg' ?>" style="height: 300px;">
      </div>
      <div class="col-md-6">
        <img src="<?php echo base_url() . 'assets/images/schools_tab_3.jpg' ?>" style="height: 300px;">
      </div>
    </div>
    <div class="row" style="margin-top: 30px;">
      <div class="col-md-6">
        <p class="about-title">As a Head Teacher, Wagona Maths enables you to…</p>
        <ul>
          <li>Differentiate learning with a truly personalised learning journey for every student</li>
          <li>Access full visibility on the progress and attainment in maths at a school, class and student levels</li>
          <li>Give your teachers access to robust curriculum-aligned teaching, planning and reporting tools, allowing them to concentrate on delivering good results</li>
          <li>Real-time attainment and progress reports for your school data requirements from the Ministry</li>
          <li>Use innovative technology provided by Wagona Maths to give every child the individual attention they deserve in order to fulfil their potential.</li>
          <li>Activate and de-activate any sub-account for various reasons including non-payment of fees</li>
        </ul>
      </div>
      <div class="col-md-6">
        <p class="about-title">As a teacher, Wagona Maths enables you to…</p>
        <ul>
          <li>Deliver differentiated learning for every student, in a fun and engaging way</li>
          <li>Access real-time mark books/reports on each student’s progress, ability and usage </li>
          <li>Support students in revision and exam preparation with powerful whole-class, group and one-to-one teaching tools</li>
          <li>Wagona Maths reports are not just about numbers. They encapsulate the performance of your class as a whole and of every student, enabling you to provide individual	ised and perfectlyt tailored lesson plans. They provide you with insight into their effort, improvement over time and trouble spots.</li>
        </ul>
      </div>
      <div class="col-md-12">
        With an individual account for every child, your students can progress in maths wherever they are – in the classroom, at home or anywhere with access to the internet. It’s just <strong>like having a teaching assistant</strong> for every child in class and a personal tutor for every child at home.
      </div>
    </div>
    <div class="row" style="margin-top: 30px;">
      <div class="col-md-6">
        <img src="<?php echo base_url() . 'assets/images/schools_tab_4.jpg' ?>">
      </div>
      <div class="col-md-6">
        <ul>
          <li>Student answers are marked automatically and immediately, providing instant feedback to students, teachers and parents.</li>
          <li>The platform isolates topics and areas that the student is struggling with, thereby creating opportunities for individualised learning.</li>
          <li>Multiple choice questions are designed to capture misconceptions, with relevant tutorials embedded to address them.</li>
        </ul>
      </div>
    </div>
    <div class="row" style="margin-top: 30px;">
      <div class="col-md-12">
        <p>
          The teacher can get a summary view of student performance in all his/her classes, as well as dynamic class average percentages.
        </p>
        <img src="<?php echo base_url() . 'assets/images/schools_tab_5.jpg' ?>">
        <p style="margin-top: 30px;">
          Don’t work harder, work smarter with Wagona Maths.
        </p>
      </div>
    </div>
  </div>
</div>

<?php $this->load->view('footer.php'); ?>
