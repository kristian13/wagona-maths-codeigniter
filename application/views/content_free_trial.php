<?php $this->load->view('header.php'); ?>

<div id="content" class="content-row">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="title">
					<h2 class="red-col">Create your <span class="green-col">account</span></h2>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6 col-md-offset-3">
				<form class="form-horizontal s-form">
					<div class="form-group">
						<span class="req-field red-col">*</span>
						<div class="col-md-6">
							<input type="text" class="form-control" placeholder="First Name" />
						</div>
						<div class="col-md-6">
							<input type="text" class="form-control" placeholder="Surname" />
						</div>
					</div>
					<div class="form-group">
						<span class="req-field red-col">*</span>
						<div class="col-md-12">
							<input type="text" class="form-control" placeholder="Username">
						</div>
					</div>
					<div class="form-group">
						<span class="req-field red-col">*</span>
						<div class="col-md-12">
							<input type="password" class="form-control" placeholder="Password">
						</div>
					</div>
					<div class="form-group">
						<span class="req-field red-col">*</span>
						<div class="col-md-12">
							<input type="password" class="form-control" placeholder="Confirm Password">
						</div>
					</div>
					<div class="form-group">
						<span class="req-field red-col">*</span>
						<div class="col-md-12">
							<input type="text" class="form-control" placeholder="Nationality">
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-12">
							<label>Have you already booked your test?</label>
							<select class="form-control">
								<option value="">(Optional)</option>
								<option value="">Yes</option>
								<option value="">No</option>
							</select>
						</div>
					</div>
					<div class="form-group mb-30">
						<div class="col-md-12">
							<label>Date of your test</label>
						</div>
						<div class="col-md-4">
							<select class="form-control">
								<option value="">January</option>
								<option value="">February</option>
								<option value="">March</option>
								<option value="">April</option>
								<option value="">May</option>
								<option value="">June</option>
								<option value="">July</option>
								<option value="">August</option>
								<option value="">September</option>
								<option value="">October</option>
								<option value="">November</option>
								<option value="">December</option>
							</select>
						</div>
						<div class="col-md-4">
							<select class="form-control">
								<option value="">01</option>
								<option value="">02</option>
								<option value="">03</option>
								<option value="">04</option>
								<option value="">05</option>
								<option value="">06</option>
								<option value="">07</option>
								<option value="">08</option>
								<option value="">09</option>
								<option value="">10</option>
								<option value="">11</option>
								<option value="">12</option>
								<option value="">13</option>
								<option value="">14</option>
								<option value="">15</option>
								<option value="">16</option>
								<option value="">17</option>
								<option value="">18</option>
								<option value="">19</option>
								<option value="">20</option>
								<option value="">21</option>
								<option value="">22</option>
								<option value="">23</option>
								<option value="">24</option>
								<option value="">25</option>
								<option value="">26</option>
								<option value="">27</option>
								<option value="">28</option>
								<option value="">29</option>
								<option value="">30</option>
								<option value="">37</option>
							</select>
						</div>
						<div class="col-md-4">
							<select class="form-control">
								<option value="">2014</option>
								<option value="">2015</option>
								<option value="">2016</option>
								<option value="">2017</option>
								<option value="">2018</option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-12">
							<button type="button" data-toggle="modal" data-target="#free-trial" class="btn btn-block btn-dblue btn-lg btn-box btn-primary">SUBMIT</button>
						</div>
					</div>
				</form>
			</div>
		</div>	
	</div>
</div>

<!-- Modal -->
<div class="modal fade s-modal" id="free-trial" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <a class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></a>
      	<div class="success-modal-body center-txt">
      		<span class="circ-icon circ-icon-lg gray-bg">
				<span class="fa fa-thumbs-up"></span>
			</span>
			<h1 class="museo300 border-bottom m-0 mb-20 pb-10"><big>Success!</big></h1>
			<p class="mb-30">
				<big>Your trial account has been successfully created, <strong>passUKtest have sent you a confirmation email</strong> with further instructions.</big>
			</p>
			<a href="#" class="btn btn-dblue btn-lg btn-box btn-primary">Confirm</a>
      	</div>
      </div>
    </div>
  </div>
</div>

<?php $this->load->view('footer.php'); ?>