<?php $this->load->view('header.php'); ?>

<div id="content" class="content-row">
	<div class="container">
		<div class="panel-group" id="accordion">
		
				<center><h1 class="main_title blue_text">PRIVACY POLICY</h1></center>
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <h4 class="panel-title">
                      <a data-toggle="collapse" data-parent="#accordion" href="#privacyCollapseIntro" title="privacyCollapseIntro">
                        Privacy Policy <i class="pull-right collapse_icon glyphicon glyphicon-chevron-down"></i>
                      </a>
                    </h4>
                  </div>
                  <div id="privacyCollapseIntro" class="panel-collapse collapse">
                    <div class="panel-body">
                      <p>This Privacy Policy defines the manner in which PassUKtest uses and maintains information collected from users of the PassUKtest website. This privacy policy applies to all products and services offered by PassUKtest.</p>
                      <p>Your privacy is our priority. This statement outlines PassUKtest Privacy policy and how we use and manage personal information provided to or collected by it.</p>
                      <p>Our staff is required by law to protect personal information which PassUKtest collects and holds. The laws Big School holds to protect users’ personal information are: The Victorian privacy laws, the Information Privacy Act 2000 and the Health Records Act 2001.</p>
                      
                      <h4>Contacting us</h4>
                      <p>
                        If you have any questions about this Privacy Policy, please contact us at:<br />
                        <h3 class="myriad_font">PassUKtest</h3>
                        34 New House<br>
                        67-68 Hatton Garden<br>
                        LONDON, EC1N 8JY<br>
                        United Kingdom<br>
                      </p>
                    </div>
                  </div>
                </div><!-- panel -->
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <h4 class="panel-title">
                      <a data-toggle="collapse" data-parent="#accordion" href="#privacyCollapseOne" title="privacyCollapseOne">
                        Personal Identification Information <i class="pull-right collapse_icon glyphicon glyphicon-chevron-down"></i>
                      </a>
                    </h4>
                  </div>
                  <div id="privacyCollapseOne" class="panel-collapse collapse">
                    <div class="panel-body">
                      <p><strong>Personal Identification Information</strong> &ndash; When users visit and register on our site, or subscribe to our eZine for registration purposes they may be asked for name and email address. We will collect and handle personal identification information only for the purposes of offering our educational services and only if our subscribers voluntarily submit such information to us.</p>
                    </div>
                  </div>
                </div><!-- panel -->
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <h4 class="panel-title">
                      <a data-toggle="collapse" data-parent="#accordion" href="#privacyCollapseTwo" title="privacyCollapseTwo">
                        Web Browser Cookies <i class="pull-right collapse_icon glyphicon glyphicon-chevron-down"></i>
                      </a>
                    </h4>
                  </div>
                  <div id="privacyCollapseTwo" class="panel-collapse collapse">
                    <div class="panel-body">
                      <p><strong>Web Browser Cookies</strong> &ndash; PassUKtest website may use "cookies" to enhance our users’ experience. When you use your browser to visit a website that uses cookies, they keep track of your movements within the site, or/and remember your registered login. You may choose to set your browser to stop cookies. If you do so, please be aware that some parts of our site may not function correctly.</p>
                    </div>
                  </div>
                </div><!-- panel -->
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <h4 class="panel-title">
                      <a data-toggle="collapse" data-parent="#accordion" href="#privacyCollapseThree" title="privacyCollapseThree">
                        How We Use Your Personal Information <i class="pull-right collapse_icon glyphicon glyphicon-chevron-down"></i>
                      </a>
                    </h4>
                  </div>
                  <div id="privacyCollapseThree" class="panel-collapse collapse">
                    <div class="panel-body">
                      <p>PassUKtest may collect and use personal information only for the following purposes:</p>
                      <p>
                        <ol>
                          <li>1. To improve our services &ndash; All the information you provide helps us review and follow with your requests, hence provide prompt support.</li>
                          <li>To contact you on regular base and to inform you on our latest updates and new materials. If you would like to be in our mailing list, you can subscribe and you will receive emails that may include new product or service information news, updates, etc. You can unsubscribe from receiving future emails at any time of your choice, you may contact us via our website.</li>
                        </ol>
                      </p>
                    </div>
                  </div>
                </div><!-- panel -->
                
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <h4 class="panel-title">
                      <a data-toggle="collapse" data-parent="#accordion" href="#privacyCollapseFour" title="privacyCollapseFour">
                        We Pledge We Will Protect your Information <i class="pull-right collapse_icon glyphicon glyphicon-chevron-down"></i>
                      </a>
                    </h4>
                  </div>
                  <div id="privacyCollapseFour" class="panel-collapse collapse">
                    <div class="panel-body">
                      <p>We build together the foundation of our relationship with you as our customers. You entrust us with your personal information, and we commit to respect your privacy and protect that information. We will NEVER sell or rent your name or personal information to any third party.</p>
                      <p>Our staff at PassUKtest is required to abide to this strict Privacy policy. Any employee who violates it will be subject to termination and other disciplinary measures.</p>
                    </div>
                  </div>
                </div><!-- panel -->

                <div class="panel panel-default">
                  <div class="panel-heading">
                    <h4 class="panel-title">
                      <a data-toggle="collapse" data-parent="#accordion" href="#privacyCollapseFive" title="privacyCollapseFive">
                        Changes in Privacy Policy <i class="pull-right collapse_icon glyphicon glyphicon-chevron-down"></i>
                      </a>
                    </h4>
                  </div>
                  <div id="privacyCollapseFive" class="panel-collapse collapse">
                    <div class="panel-body">
                      <p>PassUKtest may update this Privacy Policy from time to time. All changes will be duly documented at the bottom of our website. We we should endeavour to advise our clients of all changes to the privacy policy by way of email and should you have any concerns or suggestions, please contact us.</p>
                    </div>
                  </div>
                </div><!-- panel -->

                <div class="panel panel-default">
                  <div class="panel-heading">
                    <h4 class="panel-title">
                      <a data-toggle="collapse" data-parent="#accordion" href="#privacyCollapseSix" title="privacyCollapseSix">
                        Your Acceptance of these Terms <i class="pull-right collapse_icon glyphicon glyphicon-chevron-down"></i>
                      </a>
                    </h4>
                  </div>
                  <div id="privacyCollapseSix" class="panel-collapse collapse">
                    <div class="panel-body">
                      <p>By accepting our Privacy Policy, you as our customer admit and agree to keep these clauses for the use of all services available at PassUKtest’s site.</p>
                    </div>
                  </div>
                </div><!-- panel -->
              </div>
	</div>
</div>

<?php $this->load->view('footer.php'); ?>