        <div class="page-bottom">
            <div class="pre-footer">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <ul class="menu center-txt">
                              <li><a data-toggle="Home" class="cube_link" href="<?php echo base_url(); ?>"></a></li>
                              <li><a data-toggle="Pricing" class="cube_link" href="<?php echo base_url(); ?>site/pricing"></a></li>
                              <li><a data-toggle="Features" class="cube_link" href="<?php echo base_url(); ?>site/features"></a></li>
                              <li><a data-toggle="About Us" class="cube_link" href="<?php echo base_url(); ?>site/aboutus"></a></li>
                              <li><a data-toggle="Contact Us" class="cube_link" href="<?php echo base_url(); ?>site/contactus"></a></li>
                              <li><a data-toggle="Terms and Conditions" class="cube_link" href="<?php echo base_url(); ?>site/term_condition"></a></li>
                              <li><a data-toggle="Privacy Policy" class="cube_link" href="<?php echo base_url(); ?>site/privacy"></a></li>
                              <li><a data-toggle="Cookie Policy" class="cube_link" href="<?php echo base_url(); ?>site/cookie"></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div id="footer">
                <div class="footer-image"></div>
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="copyright">
                               <a href="index.php"><img src="<?php echo base_url(); ?>assets/images/logo.png" alt="Wagona" title="Wagona" /></a> Copyright &copy; 2014. Wagona Maths.All Rights Reserved.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div><!-- page -->

     <!-- modals -->

    <div class="ui-widget-content" id="workspace">
      <div class="workspace_container">
        <div class="workspace_header">
          <div class="row">
            <div class="butworks"><button class="btn close" onclick="hideWorkspace()">x</button></div>
            <div class="butworks"><button class="btn resize" onclick="resizeWorkspace()"><i class="fa fa-expand"></i></button></div>
            <div class="col-md-12">
              <h3 class="modal-title dblue_color cter" id="myModalLabel"><img title="Wagona" alt="Wagona" src="<?php echo base_url(); ?>assets/images/logo.png">Workspace</h3>
            </div>
            <div class="col-md-12">
              <ul class="s_list right_text">
                <li><a href="javascript:void(0);" class="work_draw_link mini_button yellow-bg"><span class="fa fa-pencil"></span></a></li>
                <li><a href="javascript:void(0);" class="work_text_link mini_button yellow-bg"><span class="fa fa-font"></span></a></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="workspace_body">
          <div class="workspace_text">
            <textarea></textarea>
          </div>
          <div class="workspace_draw relative_el">
            <div class="tools">
              <a href="#tools_sketch" data-tool="marker" class="mini_button yellow-bg"><span class="fa fa-pencil-square"></span></a>
              <a href="#tools_sketch" data-tool="eraser" class="mini_button yellow-bg"><span class="fa fa-eraser"></span></a>
            </div>
            <canvas id="tools_sketch" class="canvasBox"></canvas>
          </div>
        </div>
      </div>
    </div>



    <div class="calculator ui-widget-content">
      <div class="row">
        <div class="col-md-12">
          <div class="but"><button class="btn close fixclose" onclick="hideCalculator()">x</button></div>
          <FORM class="calculatorBox" NAME="sci-calc">
            <TABLE class="calc_main_table" CELLSPACING="0" CELLPADDING="1">
            <TR>
            <TD COLSPAN="5" ALIGN="center" class="calc_display"><INPUT NAME="display" VALUE="0" SIZE="28" MAXLENGTH="25"></TD>
            </TR>
            <tr>
              <td>
                  <table class="calc_table_inner">
                      <tr>
                          <td>
                              <TR>
                                  <TD ALIGN="center" class="calc_exp"><INPUT TYPE="button" VALUE=" EXP " ONCLICK="if (checkNum(this.form.display.value)) { exp(this.form) }"></TD>
                                  <TD ALIGN="center" class="calc_cos"><INPUT TYPE="button" VALUE="cos" ONCLICK="if (checkNum(this.form.display.value)) { cos(this.form) }"></TD>
                                  <TD ALIGN="center" class="calc_sin"><INPUT TYPE="button" VALUE=" sin" ONCLICK="if (checkNum(this.form.display.value)) { sin(this.form) }"></TD>
                                  <TD ALIGN="center" class="calc_tan"><INPUT TYPE="button" VALUE=" tan" ONCLICK="if (checkNum(this.form.display.value)) { tan(this.form) }"></TD>
                                  <TD ALIGN="center" class="calc_divide"><INPUT TYPE="button" VALUE="   &divide;   " ONCLICK="addChar(this.form.display, '/')"></TD>
                                  </TR>
                                  <TR>
                                  <TD ALIGN="center" class="calc_ln"><INPUT TYPE="button" VALUE="   ln   " ONCLICK="if (checkNum(this.form.display.value)) { ln(this.form) }"></TD>
                                  <TD ALIGN="center" class="calc_7 calc_yellow"><INPUT TYPE="button" VALUE="  7  " ONCLICK="addChar(this.form.display, '7')"></TD>
                                  <TD ALIGN="center" class="calc_8 calc_yellow"><INPUT TYPE="button" VALUE="  8  " ONCLICK="addChar(this.form.display, '8')"></TD>
                                  <TD ALIGN="center" class="calc_9 calc_yellow"><INPUT TYPE="button" VALUE="  9  " ONCLICK="addChar(this.form.display, '9')"></TD>
                                  <TD ALIGN="center" class="calc_multiply"><INPUT TYPE="button" VALUE="   &times;   " ONCLICK="addChar(this.form.display, '*')"></TD>
                                  </TR>
                                  <TR>
                                  <TD ALIGN="center" class="calc_sqrt"><INPUT TYPE="button" VALUE=" sqrt " ONCLICK="if (checkNum(this.form.display.value)) { sqrt(this.form) }"></TD>
                                  <TD ALIGN="center" class="calc_4 calc_yellow"><INPUT TYPE="button" VALUE="  4  " ONCLICK="addChar(this.form.display, '4')"></TD>
                                  <TD ALIGN="center" class="calc_5 calc_yellow"><INPUT TYPE="button" VALUE="  5  " ONCLICK="addChar(this.form.display, '5')"></TD>
                                  <TD ALIGN="center" class="calc_6 calc_yellow"><INPUT TYPE="button" VALUE="  6  " ONCLICK="addChar(this.form.display, '6')"></TD>
                                  <TD ALIGN="center" class="calc_minus"><INPUT TYPE="button" VALUE="   -   " ONCLICK="addChar(this.form.display, '-')"></TD>
                                  </TR>
                                  <TR>
                                  <TD ALIGN="center" class="calc_sq"><INPUT TYPE="button" VALUE="  sq  " ONCLICK="if (checkNum(this.form.display.value)) { square(this.form) }"></TD>
                                  <TD ALIGN="center" class="calc_1 calc_yellow"><INPUT TYPE="button" VALUE="  1  " ONCLICK="addChar(this.form.display, '1')"></TD>
                                  <TD ALIGN="center" class="calc_2 calc_yellow"><INPUT TYPE="button" VALUE="  2  " ONCLICK="addChar(this.form.display, '2')"></TD>
                                  <TD ALIGN="center" class="calc_3 calc_yellow"><INPUT TYPE="button" VALUE="  3  " ONCLICK="addChar(this.form.display, '3')"></TD>
                                  <TD ALIGN="center" class="calc_addition"><INPUT TYPE="button" VALUE="  +  " ONCLICK="addChar(this.form.display, '+')"></TD>
                                  </TR>
                                  <TR>
                                  <TD ALIGN="center" class="calc_open"><INPUT TYPE="button" VALUE="    (    " ONCLICK="addChar(this.form.display, '(')"></TD>
                                  <TD ALIGN="center" class="calc_0 calc_yellow"><INPUT TYPE="button" VALUE="  0  " ONCLICK="addChar(this.form.display, '0')"></TD>
                                  <TD ALIGN="center" class="calc_decimal calc_yellow"><INPUT TYPE="button" VALUE="   .  " ONCLICK="addChar(this.form.display, '.')"></TD>
                                  <TD ALIGN="center" class="calc_sign"><INPUT TYPE="button" VALUE=" +/- " ONCLICK="changeSign(this.form.display)"></TD>
                                  <TD ALIGN="center" class="calc_close"><INPUT TYPE="button" VALUE="   )   " ONCLICK="addChar(this.form.display, ')')"></TD>
                                  </TR>
                                  <TR>
                                  <TD ALIGN="center" class="calc_clear calc_orange"><INPUT TYPE="button" VALUE="clear" ONCLICK="this.form.display.value = 0 "></TD>
                                  <TD ALIGN="center" COLSPAN="2" class="calc_backspace calc_orange"><INPUT TYPE="button" VALUE="Backspace" ONCLICK="deleteChar(this.form.display)"></TD>
                                  <TD ALIGN="center" COLSPAN="2" class="calc_enter calc_orange"><INPUT TYPE="button" VALUE="enter" NAME="Enter" ONCLICK="if (checkNum(this.form.display.value)) { compute(this.form) }"></TD>
                              </TR>
                          </td>
                      </tr>
                  </table>
                </td>
              </tr>
            </TABLE>
          </FORM>
        </div>
      </div>
    </div>

    <div class="modal fade" id="statistics" aria-hidden="false">
        <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                  <h3 class="cter">Question Statistics</h3>
                  <a class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></a>
              </div>
              <div class="modal-body">
                <div class="container">
                  <div class="row">
                    <div class="col-md-12 pieContainer">
                      <img id="piechart-preloader" src="<?php echo base_url(); ?>assets/images/logo1.gif">
                      <div id="pieChart" class="chart"></div>
                      <label class="pieOrigin">
                        <p class="fLabel animated"><i class="fa fa-check green-col"></i>Correct Answers</p>
                        <p class="sLabel animated"><i class="fa fa-times red-col"></i>Wrong Answers</p>
                        <p class="absolutext animated">Number of times this question has been attempted</p>
                      </label>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </div>
    </div>

    <div class="modal fade s-modal s-modal-pad-sm in" id="feedback-modal" aria-hidden="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <a class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></a>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 center-txt">
                            <img src="<?php echo base_url(); ?>assets/images/logo.png" class="logo-sm mb-10" />
                            <div class="title">
                                <h4><strong class="red-col">Wagona wants your views!</strong></h4>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div id="feedback-item-1" class="feedback-item well" style="display: block;">
                                <p>Wagona values your opinions regarding the website so we have designed this quick feedback form for you to share your thoughts and ideas. Please feel free to express your thoughts and use the additional comments space to explain further if need be.</p>
                                <div class="feedback-btn center-txt">
                                    <button type="button" class="btn btn-dblue fb-next">Continue</button>
                                </div>
                            </div>
                            <div id="feedback-item-2" class="feedback-item well">
                                <p>1. We think that first impressions are important. What are your first impressions of the website? </p>
                                <form class="s-form">
                                  <input type="hidden" name="feedback-item-number" value="1">
                                  <input type="hidden" name="feedback-item-question-1" value="1. We think that first impressions are important. What are your first impressions of the website?">
                                    <div class="form-group">
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="feedback-item-choice-1" value="Good">
                                            Good
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="feedback-item-choice-1" value="Dull">
                                            Dull
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="feedback-item-choice-1" value="Common">
                                            Common
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="feedback-item-choice-1" value="Extraordinary">
                                            Extraordinary
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="feedback-item-choice-1" value="Too Busy">
                                            Too Busy
                                          </label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <textarea class="form-control" name="feedback-item-text-1"></textarea>
                                    </div>
                                </form>
                                <div class="feedback-btn clearfix center-txt">
                                    <button type="button" class="btn btn-dblue fb-back pull-left">Back</button>
                                    <button type="button" class="btn btn-dblue fb-next pull-right">Next</button>
                                </div>
                            </div><!-- feedback-item -->
                            <div id="feedback-item-3" class="feedback-item well">
                                <p>2. Is the website child friendly in terms of design, feel and look?</p>
                                <form class="s-form">
                                  <input type="hidden" name="feedback-item-number" value="2">
                                  <input type="hidden" name="feedback-item-question-2" value="2. Is the website child friendly in terms of design, feel and look?">
                                    <div class="form-group">
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="feedback-item-choice-2" value="Yes">
                                            Yes
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="feedback-item-choice-2" value="No">
                                            No
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="feedback-item-choice-2" value="Somehow">
                                            Somehow
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="feedback-item-choice-2" value="Too soon to tell">
                                            Too soon to tell
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="feedback-item-choice-2" value="I have no child">
                                            I have no child
                                          </label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <textarea class="form-control" name="feedback-item-text-2"></textarea>
                                    </div>
                                </form>
                                <div class="feedback-btn clearfix center-txt">
                                    <button type="button" class="btn btn-dblue fb-back pull-left">Back</button>
                                    <button type="button" class="btn btn-dblue fb-next pull-right">Next</button>
                                </div>
                            </div><!-- feedback-item -->
                            <div id="feedback-item-4" class="feedback-item well">
                                <p>3. Is the homepage important to you and why?</p>
                                <form class="s-form">
                                  <input type="hidden" name="feedback-item-number" value="3">
                                  <input type="hidden" name="feedback-item-question-3" value="3. Is the homepage important to you and why?">
                                    <div class="form-group">
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="feedback-item-choice-3" value="Not important, I want functionality">
                                            Not important, I want functionality
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="feedback-item-choice-3" value="Very Important, it sets the tone">
                                            Very Important, it sets the tone
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="feedback-item-choice-3" value="Somewhat important, but not critical">
                                            Somewhat important, but not critical
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="feedback-item-choice-3" value="Indifferent, a home page is just a homepage">
                                            Indifferent, a home page is just a homepage
                                          </label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <textarea class="form-control" name="feedback-item-text-3"></textarea>
                                    </div>
                                </form>
                                <div class="feedback-btn clearfix center-txt">
                                    <button type="button" class="btn btn-dblue fb-back pull-left">Back</button>
                                    <button type="button" class="btn btn-dblue fb-next pull-right">Next</button>
                                </div>
                            </div><!-- feedback-item -->
                            <div id="feedback-item-5" class="feedback-item well">
                                <p>4. How would you rate the design of the website compared to similar platforms?</p>
                                <form class="s-form">
                                  <input type="hidden" name="feedback-item-number" value="4">
                                  <input type="hidden" name="feedback-item-question-4" value="4. How would you rate the design of the website compared to similar platforms?">
                                    <div class="form-group">
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="feedback-item-choice-4" value="Amazing">
                                            Amazing
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="feedback-item-choice-4" value="Very good, still a lot of room to improve">
                                            Very good, still a lot of room to improve
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="feedback-item-choice-4" value="Ok, nothing special">
                                             Ok, nothing special
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="feedback-item-choice-4" value="Not good, start all over!">
                                            Not good, start all over!
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="feedback-item-choice-4" value="Design is not something that is important to me">
                                            Design is not something that is important to me
                                          </label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <textarea class="form-control" name="feedback-item-text-4"></textarea>
                                    </div>
                                </form>
                                <div class="feedback-btn clearfix center-txt">
                                    <button type="button" class="btn btn-dblue fb-back pull-left">Back</button>
                                    <button type="button" class="btn btn-dblue fb-next pull-right">Next</button>
                                </div>
                            </div><!-- feedback-item -->
                            <div id="feedback-item-6" class="feedback-item well">
                                <p>5. What e-learning site/s do you think we could learn from? Please write the name of the website here and a brief comment why we could learn a trick or two from them.</p>
                                <form class="s-form">
                                  <input type="hidden" name="feedback-item-number" value="5">
                                  <input type="hidden" name="feedback-item-question-5" value="5. What e-learning site/s do you think we could learn from? Please write the name of the website here and a brief comment why we could learn a trick or two from them.">
                                    <div class="form-group">
                                        <textarea class="form-control" name="feedback-item-text-5"></textarea>
                                    </div>
                                </form>
                                <div class="feedback-btn clearfix center-txt">
                                    <button type="button" class="btn btn-dblue fb-back pull-left">Back</button>
                                    <button type="button" class="btn btn-dblue fb-next pull-right">Next</button>
                                </div>
                            </div><!-- feedback-item -->
                            <div id="feedback-item-7" class="feedback-item well">
                                <p>6. What part of the Wagona website needs improvement?</p>
                                <form class="s-form">
                                  <input type="hidden" name="feedback-item-number" value="6">
                                  <input type="hidden" name="feedback-item-question-6" value="6. What part of the Wagona website needs improvement?">
                                    <div class="form-group">
                                        <div class="checkbox">
                                          <label>
                                            <input type="radio" name="feedback-item-choice-6" value="Banner">
                                            Banner
                                          </label>
                                        </div>
                                        <div class="checkbox">
                                          <label>
                                            <input type="radio" name="feedback-item-choice-6" value="Images">
                                            Images
                                          </label>
                                        </div>
                                        <div class="checkbox">
                                          <label>
                                            <input type="radio" name="feedback-item-choice-6" value="Icons">
                                            Icons
                                          </label>
                                        </div>
                                        <div class="checkbox">
                                          <label>
                                            <input type="radio" name="feedback-item-choice-6" value="Buttons">
                                            Buttons
                                          </label>
                                        </div>
                                        <div class="checkbox">
                                          <label>
                                            <input type="radio" name="feedback-item-choice-6" value="Colours">
                                            Colours
                                          </label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <textarea class="form-control" name="feedback-item-text-6"></textarea>
                                    </div>
                                </form>
                                <div class="feedback-btn clearfix center-txt">
                                    <button type="button" class="btn btn-dblue fb-back pull-left">Back</button>
                                    <button type="button" class="btn btn-dblue fb-next pull-right">Next</button>
                                </div>
                            </div><!-- feedback-item -->
                            <div id="feedback-item-8" class="feedback-item well">
                                <p>7. Did you understand immediately what the website is all about from the homepage or later pages?</p>
                                <form class="s-form">
                                  <input type="hidden" name="feedback-item-number" value="7">
                                  <input type="hidden" name="feedback-item-question-7" value="7. Did you understand immediately what the website is all about from the homepage or later pages?">
                                    <div class="form-group">
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="feedback-item-choice-7" value="Yes, I understood what the website is all about on the homepage">
                                            Yes, I understood what the website is all about on the homepage
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="feedback-item-choice-7" value="Yes, but it took me a while to understand what this website is all about">
                                            Yes, but it took me a while to understand what this website is all about
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="feedback-item-choice-7" value="No I still don’t get it, what is this website all about?">
                                            No I still don’t get it, what is this website all about?
                                          </label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <textarea class="form-control" name="feedback-item-text-7"></textarea>
                                    </div>
                                </form>
                                <div class="feedback-btn clearfix center-txt">
                                    <button type="button" class="btn btn-dblue fb-back pull-left">Back</button>
                                    <button type="button" class="btn btn-dblue fb-next pull-right">Next</button>
                                </div>
                            </div><!-- feedback-item -->
                            <div id="feedback-item-9" class="feedback-item well">
                                <p>8. Did you try and register as a parent or teacher? How was the registration process experience?</p>
                                <form class="s-form">
                                  <input type="hidden" name="feedback-item-number" value="8">
                                  <input type="hidden" name="feedback-item-question-8" value="8. Did you try and register as a parent or teacher? How was the registration process experience?">
                                    <div class="form-group">
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="feedback-item-choice-8" value="Yes I registered and it is straightforward">
                                            Yes I registered and it is straightforward
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="feedback-item-choice-8" value="No I did not try to register">
                                            No I did not try to register
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="feedback-item-choice-8" value="Yes I registered and the process was long winded and confusing">
                                            Yes I registered and the process was long winded and confusing
                                          </label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <textarea class="form-control" name="feedback-item-text-8"></textarea>
                                    </div>
                                </form>
                                <div class="feedback-btn clearfix center-txt">
                                    <button type="button" class="btn btn-dblue fb-back pull-left">Back</button>
                                    <button type="button" class="btn btn-dblue fb-next pull-right">Next</button>
                                </div>
                            </div><!-- feedback-item -->
                            <div id="feedback-item-10" class="feedback-item well">
                                <p>9. Did you attempt to use our live help/ feedback on the right hand edge of every page? Was there a response and did you enjoy the experience?</p>
                                <form class="s-form">
                                  <input type="hidden" name="feedback-item-number" value="9">
                                  <input type="hidden" name="feedback-item-question-9" value="9. Did you attempt to use our live help/ feedback on the right hand edge of every page? Was there a response and did you enjoy the experience?">
                                    <div class="form-group">
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="feedback-item-choice-9" value="Yes I tried, there was no response">
                                             Yes I tried, there was no response
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="feedback-item-choice-9" value="Yes I tried, there was response">
                                             Yes I tried, there was response
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="feedback-item-choice-9" value="No did not try out the feature">
                                             No did not try out the feature
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="feedback-item-choice-9" value="I still don't understand what LiveHelp is">
                                             I still don't understand what LiveHelp is
                                          </label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <textarea class="form-control" name="feedback-item-text-9"></textarea>
                                    </div>
                                </form>
                                <div class="feedback-btn clearfix center-txt">
                                    <button type="button" class="btn btn-dblue fb-back pull-left">Back</button>
                                    <button type="button" class="btn btn-dblue fb-next pull-right">Next</button>
                                </div>
                            </div><!-- feedback-item -->
                            <div id="feedback-item-11" class="feedback-item well">
                                <p>10. Do you feel the website prompted you to try the product? </p>
                                <form class="s-form">
                                  <input type="hidden" name="feedback-item-number" value="10">
                                  <input type="hidden" name="feedback-item-question-10" value="10. Do you feel the website prompted you to try the product?">
                                    <div class="form-group">
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="feedback-item-choice-10" value="Nothing at all, zero nudging">
                                            Nothing at all, zero nudging
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="feedback-item-choice-10" value="Yes, it did-couldn't wait">
                                            Yes, it did-couldn't wait
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="feedback-item-choice-10" value="I did , but ordinarily wouldn’t">
                                            I did , but ordinarily wouldn’t
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="feedback-item-choice-10" value="Free Trial? I did not see any prompts">
                                            Free Trial? I did not see any prompts
                                          </label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <textarea class="form-control" name="feedback-item-text-10"></textarea>
                                    </div>
                                </form>
                                <div class="feedback-btn clearfix center-txt">
                                    <button type="button" class="btn btn-dblue fb-back pull-left">Back</button>
                                    <button type="button" class="btn btn-dblue fb-next pull-right">Next</button>
                                </div>
                            </div><!-- feedback-item -->
                            <div id="feedback-item-12" class="feedback-item well">
                                <p>11. Does the site show its purpose effectively?</p>
                                <form class="s-form">
                                  <input type="hidden" name="feedback-item-number" value="11">
                                  <input type="hidden" name="feedback-item-question-11" value="11. Does the site show its purpose effectively?">
                                    <div class="form-group">
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="feedback-item-choice-11" value="Yes">
                                            Yes
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="feedback-item-choice-11" value="No">
                                            No
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="feedback-item-choice-11" value="Maybe">
                                            Maybe
                                          </label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <textarea class="form-control" name="feedback-item-text-11"></textarea>
                                    </div>
                                </form>
                                <div class="feedback-btn clearfix center-txt">
                                    <button type="button" class="btn btn-dblue fb-back pull-left">Back</button>
                                    <button type="button" class="btn btn-dblue fb-next pull-right">Next</button>
                                </div>
                            </div><!-- feedback-item -->
                            <div id="feedback-item-13" class="feedback-item well">
                                <p>12. How easy is locating desired information in the navigation process?</p>
                                <form class="s-form">
                                  <input type="hidden" name="feedback-item-number" value="12">
                                  <input type="hidden" name="feedback-item-question-12" value="12. How easy is locating desired information in the navigation process?">
                                    <div class="form-group">
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="feedback-item-choice-12" value="Easy and intuitive">
                                            Easy and intuitive
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="feedback-item-choice-12" value="Very easy">
                                            Very easy
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="feedback-item-choice-12" value="Hard">
                                            Hard
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="feedback-item-choice-12" value="Very Hard, it is like a maze">
                                            Very Hard, it is like a maze
                                          </label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <textarea class="form-control" name="feedback-item-text-12"></textarea>
                                    </div>
                                </form>
                                <div class="feedback-btn clearfix center-txt">
                                    <button type="button" class="btn btn-dblue fb-back pull-left">Back</button>
                                    <button type="button" class="btn btn-dblue fb-next pull-right">Next</button>
                                </div>
                            </div><!-- feedback-item -->
                            <div id="feedback-item-14" class="feedback-item well">
                                <p>13. Did you take the diagnostic test? How did you find it?</p>
                                <form class="s-form">
                                  <input type="hidden" name="feedback-item-number" value="13">
                                  <input type="hidden" name="feedback-item-question-13" value="13. Did you take the diagnostic test? How did you find it?">
                                    <div class="form-group">
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="feedback-item-choice-13" value="Engaging and reflective of my math level?">
                                            Engaging and reflective of my math level?
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="feedback-item-choice-13" value="Too long and boring">
                                            Too long and boring
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="feedback-item-choice-13" value="Engaging but not true of my maths level">
                                            Engaging but not true of my maths level
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="feedback-item-choice-13" value="Okay">
                                            Okay
                                          </label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <textarea class="form-control" name="feedback-item-text-13"></textarea>
                                    </div>
                                </form>
                                <div class="feedback-btn clearfix center-txt">
                                    <button type="button" class="btn btn-dblue fb-back pull-left">Back</button>
                                    <button type="button" class="btn btn-dblue fb-next pull-right">Next</button>
                                </div>
                            </div><!-- feedback-item -->
                            <div id="feedback-item-15" class="feedback-item well">
                                <p>14. Is the web content relevant and current?</p>
                                <form class="s-form">
                                  <input type="hidden" name="feedback-item-number" value="14">
                                  <input type="hidden" name="feedback-item-question-14" value="14. Is the web content relevant and current?">
                                    <div class="form-group">
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="feedback-item-choice-14" value="Positive">
                                            Positive
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="feedback-item-choice-14" value="Negative">
                                            Negative
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="feedback-item-choice-14" value="Satisfactory">
                                            Satisfactory
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="feedback-item-choice-14" value="Needs improvement">
                                            Needs improvement
                                          </label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <textarea class="form-control" name="feedback-item-text-14"></textarea>
                                    </div>
                                </form>
                                <div class="feedback-btn clearfix center-txt">
                                    <button type="button" class="btn btn-dblue fb-back pull-left">Back</button>
                                    <button type="button" class="btn btn-dblue fb-next pull-right">Next</button>
                                </div>
                            </div><!-- feedback-item -->
                            <div id="feedback-item-16" class="feedback-item well">
                                <p>15. Does the website have a fast-loading homepage? Do subsequent pages load quickly as well?</p>
                                <form class="s-form">
                                  <input type="hidden" name="feedback-item-number" value="15">
                                  <input type="hidden" name="feedback-item-question-15" value="15. Does the website have a fast-loading homepage? Do subsequent pages load quickly as well?">
                                    <div class="form-group">
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="feedback-item-choice-15" value="Yes">
                                            Yes
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="feedback-item-choice-15" value="No">
                                            No
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="feedback-item-choice-15" value="Maybe">
                                            Maybe
                                          </label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <textarea class="form-control" name="feedback-item-text-15"></textarea>
                                    </div>
                                </form>
                                <div class="feedback-btn clearfix center-txt">
                                    <button type="button" class="btn btn-dblue fb-back pull-left">Back</button>
                                    <button type="button" class="btn btn-dblue fb-next pull-right">Next</button>
                                </div>
                            </div><!-- feedback-item -->
                            <div id="feedback-item-17" class="feedback-item well">
                                <p>16. Could you trust to let your children use the site unsupervised?</p>
                                <form class="s-form">
                                    <div class="form-group">
                                      <input type="hidden" name="feedback-item-number" value="16">
                                      <input type="hidden" name="feedback-item-question-16" value="16. Could you trust to let your children use the site unsupervised?">
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="feedback-item-choice-16" value="Yes, I can trust this website">
                                            Yes, I can trust this website
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="feedback-item-choice-16" value="Yes, but I need more time to assess">
                                            Yes, but I need more time to assess
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="feedback-item-choice-16" value="Hmmm, website looks dodgy">
                                            Hmmm, website looks dodgy
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="feedback-item-choice-16" value="No, I just don't trust websites">
                                            No, I just don't trust websites
                                          </label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <textarea class="form-control" name="feedback-item-text-16"></textarea>
                                    </div>
                                </form>
                                <div class="feedback-btn clearfix center-txt">
                                    <button type="button" class="btn btn-dblue fb-back pull-left">Back</button>
                                    <button type="button" class="btn btn-dblue fb-next pull-right">Next</button>
                                </div>
                            </div><!-- feedback-item -->
                            <div id="feedback-item-18" class="feedback-item well">
                                <p>17. Would you register your child for a free trial?</p>
                                <form class="s-form">
                                  <input type="hidden" name="feedback-item-number" value="17">
                                  <input type="hidden" name="feedback-item-question-17" value="17. Would you register your child for a free trial?">
                                    <div class="form-group">
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="feedback-item-choice-17" value="Yes, I think I can trust this website">
                                            Yes, I think I can trust this website
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="feedback-item-choice-17" value="No, it has all the hallmarks of a website I wouldn’t trust.">
                                            No, it has all the hallmarks of a website I wouldn’t trust.
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="feedback-item-choice-17" value="I would want feedback from someone who has used the site first">
                                            I would want feedback from someone who has used the site first
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="feedback-item-choice-17" value="Yes, but I will go for the free trial first">
                                            Yes, but I will go for the free trial first
                                          </label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <textarea class="form-control" name="feedback-item-text-17"></textarea>
                                    </div>
                                </form>
                                <div class="feedback-btn clearfix center-txt">
                                    <button type="button" class="btn btn-dblue fb-back pull-left">Back</button>
                                    <button type="button" class="btn btn-dblue fb-next pull-right">Next</button>
                                </div>
                            </div><!-- feedback-item -->
                            <div id="feedback-item-19" class="feedback-item well">
                                <p>18. How likely are you to recommend the site to a friend/colleague when you have had the chance to try it?</p>
                                <form class="s-form">
                                  <input type="hidden" name="feedback-item-number" value="18">
                                  <input type="hidden" name="feedback-item-question-18" value="18. How likely are you to recommend the site to a friend/colleague when you have had the chance to try it?">
                                    <div class="form-group">
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="feedback-item-choice-18" value="Very likely">
                                            Very likely
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="feedback-item-choice-18" value="Somewhat likely">
                                            Somewhat likely
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="feedback-item-choice-18" value="Not really my thing to recommend">
                                            Not really my thing to recommend
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="feedback-item-choice-18" value="Somewhat unlikely">
                                            Somewhat unlikely
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="feedback-item-choice-18" value="Unlikely, website is not good enough">
                                            Unlikely, website is not good enough
                                          </label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <textarea class="form-control" name="feedback-item-text-18"></textarea>
                                    </div>
                                </form>
                                <div class="feedback-btn clearfix center-txt">
                                    <button type="button" class="btn btn-dblue fb-back pull-left">Back</button>
                                    <button type="button" class="btn btn-dblue fb-next pull-right">Next</button>
                                </div>
                            </div><!-- feedback-item -->
                            <div id="feedback-item-20" class="feedback-item well final">
                                <p>Thank you for participating on our survey. You have our assurance we will read through your comments and take requisite action. In the meantime feel free to contact us if you feel it necessary.</p>
                                <form class="s-form mb-20">
                                    <div class="form-group">
                                        <textarea class="form-control" name="feedback-final-text" placeholder="Message"></textarea>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-md-6">
                                            <input type="text" class="form-control" placeholder="Name" name="feedback-final-name" />
                                        </div>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control" placeholder="Email" name="feedback-final-email" />
                                        </div>
                                    </div>
                                </form>
                                <p>By using this feature and submitting feedback, suggestions or enhancements, you assign to Wagona Maths all right, title and interest to all associated proprietary rights worldwide, including without limitation, all patent, copyright, trade secret, trademark, moral right, rights of publicity, or other intellectual property rights.</p>
                                <p>Copyright © 2014 Wagona Maths. All Rights Reserved. Designated trademarks and brands are the property of their respective owners. Use of this website constitutes acceptance of the Wagona Maths Terms and Policies.</p>
                                <div class="feedback-btn clearfix center-txt">
                                    <button type="button" class="btn btn-dblue fb-back pull-left">Back</button>
                                    <button type="button" class="btn btn-dblue fb-next pull-right">Next</button>
                                </div>
                            </div><!-- feedback-item -->
                            <div id="feedback-item-21" class="feedback-item alert alert-success">
                                <p>Thank you for participating on our survey. You have our assurance we will read through your comments and take requisite action. In the meantime feel free to contact us if you feel it necessary.</p>
                            </div><!-- feedback-item -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="video-modal-index" class="modal fade s-modal in">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <a data-dismiss="modal" class="close"><span aria-hidden="true">×</span><span class="sr-only">Close</span></a>
            <h3><span class="fa fa-share-alt m-icon-sm"></span>Video</h3>
          </div>
          <div class="modal-body">
             <iframe width="560" height="315" src="https://www.youtube.com/embed/SkfcqLrU0fY" frameborder="0" allowfullscreen></iframe>
          </div>
        </div>
      </div>
    </div>

     <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/flexslider/js/jquery.flexslider-min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.videobackground.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/main.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/jquery-ui/jquery-ui.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/calculator.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/sketch.js"></script>
	<script>

       $('#myTab a').click(function (e) {
          e.preventDefault()
          $(this).tab('show')
        });

        $(function() {
          $('#tools_sketch').sketch({defaultColor: "#ffffff", defaultSize: "2"});
        });

       $(function() {
         $( ".calculator" ).draggable();
       });

       $(function() {
         $( "#workspace" ).draggable();
       });

       $(function() {
         $('#explanation-modal').draggable();
       });


       function showCalculator() {
        $('.calculator').show();
       }

       function hideCalculator() {
         $('.calculator').hide();
       }

       function showWorkspace() {
        $('#workspace').show();
       }

       function hideWorkspace() {
         $('#workspace').hide();
       }

       function resizeWorkspace(){
        if ($('#workspace').hasClass('fullWorkspace')) {
          $('#workspace').removeClass('fullWorkspace').css({'width':'500px','height':'380px'});
          document.getElementById('tools_sketch').width = 500;
          document.getElementById('tools_sketch').height = 300;
        }
        else {
          $('#workspace').addClass('fullWorkspace').css({'width':'900px','height':'500px'});
          document.getElementById('tools_sketch').width = 900;
          document.getElementById('tools_sketch').height = 425;
        }
       }


    </script>

    <script type="text/javascript">
      $(document).ready(function(){
        $(".workspace_text").hide();
        $(".work_text_link").click(function(){
          $(".workspace_text").show();
          $(".workspace_draw").hide();
        });
        $(".work_draw_link").click(function(){
          $(".workspace_draw").show();
          $(".workspace_text").hide();
        });
      });
    </script>

    <script type="text/javascript">

        $(document).ready(function(){
            var x= document.cookie;

            if ( x = 0) {
                $('#cookie-bar').css('display','block');
            }

        });

        $(function () {
          $('[data-toggle="tooltip"]').tooltip()
        })

        function bol(){
           var value=1;
           document.cookie = value;
           $("#cookie-bar").animate({bottom: '-30'});
        }

       // $(window).scroll(function() {
       //     var scroll = $(window).scrollTop();
       //     $('.toper-menu').stop().animate(
       //         {opacity: (( 180-scroll )/100)+0.1},
       //         "slow"
       //     );
       // });


    </script>

    <!--Start of Tawk.to Script-->
    <script type="text/javascript">
    var $_Tawk_API={},$_Tawk_LoadStart=new Date();
    (function(){
    var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
    s1.async=true;
    s1.src='https://embed.tawk.to/5582208f8e4b7b8c706122cd/default';
    s1.charset='UTF-8';
    s1.setAttribute('crossorigin','*');
    s0.parentNode.insertBefore(s1,s0);
    })();
    </script>
