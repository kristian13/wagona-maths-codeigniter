<?php $this->load->view('header.php'); ?>

<div id="content" class="content-row animated">
	<div class="container">
		<div class="row mb-20">
			<div class="col-md-6 col-sm-6 box-shad-right">
				<div class="box-two mob-mb-20">
					<div class="box-two-pad">
						<h3 class="m-0 bt-title museo300">Buy <span class="museo700 green-col">Now</span></h3>
						<div class="bt-featured yellow-bg white-col gridbox fx-slow">
							<h4 class="strikethrough"> $10/Month</h4>
							<h2>Offer Price: <br/> $5/month</h2>
						</div>
						<img src="<?php echo base_url(); ?>assets/images/cards.jpg"/>

						<a class="btn btn-dgreen btn-bottom" href="<?php echo base_url(); ?>school-signup">SCHOOL</a>
						<a class="btn btn-dgreen btn-bottom" href="<?php echo base_url(); ?>site/paidform">INDIVIDUAL</a>
					</div>
				</div>
			</div>
			<div class="floatcenter goldbadge yellow-bg">or</div>
			<div class='col-md-6 col-sm-6'>
				<div class="box-two mob-mb-20">
					<div class="box-two-pad">
						<h3 class="m-0 bt-title museo300">Try for <span class="museo700 green-col">Free</span></h3>
						<div class="bt-featured yellow-bg white-col gridbox fx-slow">
							<img class="featbadge" src="<?php echo base_url(); ?>assets/images/satisfactionbadge.png" />
						</div>
						<img src="<?php echo base_url(); ?>assets/images/cardsX.png"/>

						<a class="btn btn-dgreen btn-bottom" href="#">SCHOOL</a>
						<a class="btn btn-dgreen btn-bottom" href="<?php echo base_url(); ?>site/free_trial">INDIVIDUAL</a>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="center-txt">
					*By proceeding with payment for this order, you agree to our <a href="<?php echo base_url(); ?>site/term_condition">Terms and Conditions</a>
				</div>
			</div>
		</div>
	</div>
</div>

<?php $this->load->view('footer.php'); ?>