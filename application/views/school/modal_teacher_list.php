<!-- Teacher Modal -->
<div class="modal fade school-modal" id="teacher_list_modal" role="dialog" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog large" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
				<h4>Teachers</h4>
			</div>
			<div class="modal-body">
				<!-- Nav tabs -->
				<ul class="nav nav-tabs">
					<li role="presentation" class="active">
						<a href="#teacher_list" role="tab" data-toggle="tab">List</a>
					</li>
					<li role="presentation">
						<a href="#teacher_crud" role="tab" data-toggle="tab">Create Teacher</a>
					</li>
					<li role="presentation" class="pull-right">
						<a href="#teacher_help" role="tab" data-toggle="tab">Help</a>
					</li>
				</ul>

				<!-- Tab panes -->
				<div class="tab-content">
					<div role="tabpanel" class="tab-pane fade in active" id="teacher_list">
						<table class="table table-striped table-bordered" id="table-teacher-list">
							<thead>
								<tr>
									<th>Firstname</th>
									<th>Surname</th>
									<th>Username</th>
									<th>Password</th>
									<th>Classes</th>
									<th>Status</th>
									<th>Edit</th>
								</tr>
							</thead>
						</table>
					</div>
					<div role="tabpanel" class="tab-pane fade in" id="teacher_crud">
						<div class="form-container">
							<form>
								<span class="error-message invisible">Please input the form properly.</span>
								<div class="field-container">
									<div class="label-container"><span>Firstname:<span class="required pull-right">*</span></span></div>
									<div class="input-container">
										<input type="text" name="firstname" class="input-form">
									</div>
								</div>
								<div class="field-container">
									<div class="label-container"><span>Surname:<span class="required pull-right">*</span></span></div>
									<div class="input-container">
										<input type="text" name="surname" class="input-form">
									</div>
								</div>
								<div class="field-container">
									<div class="label-container"><span>Classes:<span class="required pull-right">*</span></span></div>
									<div class="input-container">
										<select id="classes" name="classes" data-placeholder="Select Classes" class="input-form" multiple>
											<option value=""></option>
											<?php foreach($classes as $key => $val): ?>
											<option value="<?php echo $val['class_id'] ?>"><?php echo $val['name'] ?></option>
											<?php endforeach; ?>
										</select>
									</div>
								</div>
								<div class="field-container">
									<div class="label-container"><span>Username:<span class="required pull-right">*</span></span></div>
									<div class="input-container">
										<input type="text" name="username" class="input-form">
									</div>
								</div>
								<div class="field-container">
									<div class="label-container"><span>Password:<span class="required pull-right">*</span></span></div>
									<div class="input-container">
										<input type="text" name="password" class="input-form" maxlength="32">
									</div>
								</div>
								<div class="field-container">
									<div class="label-container">&nbsp;</div>
									<div class="input-container">
										<a class="btn btn-default" onclick="generate_random_username_password('teacher')">Generate Random Username & Password</a>
									</div>
								</div>
								<div class="form-footer">
									<a class="btn btn-default cancel hide">Cancel</a>
									<button class="btn btn-primary submit">Save</button>
								</div>
							</form>
						</div>
						<div class="view-container hide">
							<div class="content"></div>
							<div class="form-footer">
								<a class="btn btn-default cancel hide">Cancel</a>
							</div>
						</div>
						<img src="<?php echo base_url(); ?>assets/images/loader.gif" class="loader hide" />
					</div>
					<div role="tabpanel" class="tab-pane fade in" id="teacher_help">
						<div class="rect-el-full rect-el lgray-bg removepad remarg">
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem cupiditate asperiores minima. Ullam repellat ab magnam pariatur laborum eos enim. Facere amet quibusdam, aspernatur nihil ut dignissimos qui cumque! Eos!</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>