<?php $this->load->view('school/header.php'); ?>
<?php $this->load->view('school/menu_school_dashboard.php'); ?>

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/r/bs/dt-1.10.9,r-1.0.7/datatables.min.css"/>
<link href="<?php echo base_url(); ?>assets/css/chosen.css" rel="stylesheet">
<div class="container school-container">
	<div class="row">
		<!-- <div class="col-md-3">
			<div class="rect-el lgray-bg gridbox school-frame-box" data-toggle="modal" data-target="#school_list_modal">
				<div class="btn btn-primary thumb-button-top">Schools</div>
				<div class="content">
					This is the static information about a school the administrator at the school will be able to view. The number of the students that they would have subscribed for correspond the maximum number of students
				</div>
			</div>
		</div> -->
		<div class="col-md-4">
			<div class="rect-el lgray-bg gridbox school-frame-box" data-toggle="modal" data-target="#class_list_modal">
				<div class="btn btn-primary thumb-button-top">Classes</div>
				<div class="content">
					Clicking on classes allows the administrator to create a new class within the subject. The subjects are predefined
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="rect-el lgray-bg gridbox school-frame-box" data-toggle="modal" data-target="#teacher_list_modal">
				<div class="btn btn-primary thumb-button-top">Teachers</div>
				<div class="content">
					When you click new teacher a screen pops up and gives the administrator the ability to  write email address, username and password for the teacher, subject and class When the administrator saves the teacher only the
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="rect-el lgray-bg gridbox school-frame-box" data-toggle="modal" data-target="#student_list_modal">
				<div class="btn btn-primary thumb-button-top">Students</div>
				<div class="content">
					Name, subject and classes will display on the screen. when the Administrator selects edit, then can make a teacher active or inactive or change their details and then save the information.
				</div>
			</div>
		</div>
	</div>
</div>

<?php // $this->load->view('school/modal_school_list.php'); ?>
<?php $this->load->view('school/modal_class_list.php'); ?>
<?php $this->load->view('school/modal_teacher_list.php'); ?>
<?php $this->load->view('school/modal_student_list.php'); ?>

<?php $this->load->view('school/footer.php'); ?>

<script type="text/javascript" src="https://cdn.datatables.net/r/bs/dt-1.10.9,r-1.0.7/datatables.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/school/school-dashboard.js"></script>