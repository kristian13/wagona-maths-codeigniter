<?php $this->load->view('school/header.php'); ?>
<?php $this->load->view('school/menu_teacher_dashboard.php'); ?>

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/r/bs/dt-1.10.9,fc-3.1.0,r-1.0.7/datatables.min.css"/>
<div class="container school-container">
	<div class="row">
		<div class="flexslider">
			<ul class="slides">
				<?php foreach($classes as $key => $val): ?>
					<?php if($key % 4 == 0): ?>
					<li>
						<div class="row">
					<?php endif; ?>

						<div class="col-md-3 school-teacher-box">
							<div class="rect-el lgray-bg gridbox">
								<p class="thumb-title whitebox"><?php echo $val['class_name']; ?></p>

								<?php if(is_numeric(preg_replace('/\D/', '', $val['average']))): ?>
								<div class="badger subj-ave-flip fx-slow" data-toggle="tooltip" title="This is the moving average of the class.">
									<div class="flipper">
										<div class="front <?php echo helper_get_score_bg_class($val['average']); ?>"><?php echo $val['average']; ?></div>
										<div class="back <?php echo helper_get_score_bg_class($val['average']); ?>"><?php echo helper_get_score_grade($val['average']); ?></div>
									</div>
								</div>
								<?php else: ?>
								<div class="badger subj-ave fx-slow <?php echo helper_get_score_bg_class($val['average']); ?>"  data-toggle="tooltip" title="This is the moving average of the class."><?php echo '--'; ?></div>
								<?php endif; ?>

								<div class="thumb-container">
								<?php foreach($val['students'] as $sKey => $sVal): ?>
								<p class="thumb-test-lister">
									<div class="row">
										<div class="col-sm-8">
											<span class="thumb-name"><?php echo $sVal['name'] ?></span>
										</div>
										<div class="col-sm-4">
											<?php if(is_numeric($sVal['subject_average'])): ?>
												<a href="<?php echo base_url(); ?>account/account_mytests?class_id=<?php echo $val['class_id'] ?>&student_id=<?php echo $sVal['student_id'] ?>" target="_blank">
													<span class="school-badger text-center <?php echo helper_get_score_bg_class($sVal['subject_average']); ?>"><?php echo $sVal['subject_average'] . '%'; ?></span>
												</a>
											<?php else: ?>
												<span class="school-badger text-center <?php echo helper_get_score_bg_class($sVal['subject_average']); ?>"><?php echo $sVal['subject_average']; ?></span>
											<?php endif; ?>
										</div>
									</div>
								</p>
								<?php endforeach; ?>
								</div>
							</div>
							<button type="button" class="btn btn-primary thumb-button" onclick="showTopicLauncher(<?php echo $val['class_id'] ?>, '<?php echo $val['class_name'] ?>', <?php echo $val['subject_id'] ?>)">Detailed Reports</button>
						</div>

					<?php if($key % 4 == 3): ?>
						</div>
					</li>
					<?php endif; ?>
				<?php endforeach; ?>
			</ul>
		</div>
	</div>
</div>

<div class="modal fade topiclaucher" aria-hidden="false">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 id="top-title" class="modal-title red-col"></h4>
        <a class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></a>
      </div>
      <div class="modal-body no-top">
        <div role="tabpanel">
					<!-- Nav tabs -->
					<ul class="nav nav-tabs" role="tablist">
						<li role="presentation" class="active"><a href="#selectTopics" aria-controls="home" role="tab" data-toggle="tab">Select Topic(s)</a></li>
						<li role="presentation"><a href="#instructs" aria-controls="profile" role="tab" data-toggle="tab">Instructions</a></li>
						<li role="presentation">
						<a href="#bragColours" aria-controls="profile" role="tab" data-toggle="tab">
							<strong>
								<span class="score-black">B</span><span class="score-red">R</span><span class="score-amber">A</span><span class="score-green">G</span>
							</strong>
							&nbsp;Colours
						</a>
						</li>
					</ul>

					<!-- Tab panes -->
					<div class="tab-content">
						<div role="tabpanel" class="tab-pane active" id="selectTopics">
							<p class="thumb-title mb-20" style="margin-top:25px;">Select Topic(s) to include in the Markbook</p>
							<p style="border-bottom: solid 2px #ccc; padding-bottom: 10px; font-weight: bold; margin: 0 0 0 17px"><input type="checkbox" id="chk_select_all" name="select_all"> Select All</p>
							<label class="origins">
								<table style="width: 100%;">
									<tbody>
										<tr>
											<td width="50%">&nbsp;</td>
											<td style="text-align: right; width:12.5%"><span class="badger score-black-bg green-tooltip" data-toggle="tooltip" data-placement="top" title="Questions you attempted for topic">Q</span></td>
											<td style="text-align: right; width:12.5%"><span class="badger score-black-bg green-tooltip circleCorrect fa fa-check" data-toggle="tooltip" data-placement="top" title="Questions you answered correctly for topic"></span></td>
											<td style="text-align: right; width:12.5%"><span class="badger score-black-bg green-tooltip circleCorrect fa fa-times" data-toggle="tooltip" data-placement="top" title="Questions you answered incorrectly for topic"></span></td>
											<td style="text-align: right; width:12.5%"><span class="badger score-black-bg green-tooltip" data-toggle="tooltip" data-placement="top" title="This is a moving average %age, and not ✓/Q %age calculation">%</span></td>
										</tr>
									</tbody>
								</table>
							</label>
							<div id="check-topic"></div>
						</div>

						<div role="tabpanel" class="tab-pane" id="instructs">
							<div class="container">
								<div class="row">
									<div class="col-md-12" type="button">
										<div class="rect-el-full rect-el lgray-bg removepad remarg">
											<p><i class="fa fa-th-list sky-col"></i>This test contains 10 multiple choice questions.</p>
											<p><i class="fa fa-check-circle green-col"></i>There is one correct answer for each question.</p>
											<p><i class="fa fa-clock-o yellow-col"></i>You have 20 minutes to complete this test.</p>
											<p><i class="fa fa-list-ol violet-col"></i>Make sure you attempt all the questions.</p>
											<p><i class="fa fa-arrow-left black-col"></i>Do not use the browser back button while taking this test.</p>
											<p><i class="fa fa-circle-o-notch babyblue-col"></i>The timer of the test will not stop once the test starts.</p>
											<p><i class="fa fa-windows blue-col"></i>Modern web browsers improve the test experience.</p>
											<p><i class="fa fa-font lightgreen-col"></i>The test questions are in English language.</p>
											<p><i class="fa fa-calculator pink-col"></i>Where necessary use our web calculator at the top of the question.</p>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div role="tabpanel" class="tab-pane" id="bragColours">
							<div class="container">
								<div class="row">
									<div class="col-md-12" type="button">
										<div class="rect-el-full rect-el lgray-bg removepad remarg">
											<p>Explanation of <strong><span class="score-black">B</span><span class="score-red">R</span><span class="score-amber">A</span><span class="score-green">G</span></strong> colours for topics</p>
											<p>
												<span class="score-black"><strong>Black</strong> - Insufficient questions attempted to assess topic knowledge.</span><br>
												<span class="score-red"><strong>Red</strong> - Topic of high concern requiring urgent attention (0%-49%).</span><br>
												<span class="score-amber"><strong>Amber</strong> - Topic of medium concern requiring moderate attention (50% - 74%).</span><br>
												<span class="score-green"><strong>Green</strong> - Topic of low concern requiring little attention (75%-100%).</span>
											</p>
											<p>The Wagona Maths algorithm encourages the student to practise regularly. Colours may change to reflect inconsistency in revising topics.</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
        </div>
      </div>
      <div class="modal-footer" style="text-align: left;">
      	<a class="btn btn-dblue btn-lg btn-box btn-primary button-rounded" onclick="showReportModal()" href="javascript: void(0)"><span class="fa fa-file-text-o margin-right"></span>Markbook</a>
				<a class="btn btn-dblue btn-lg btn-box btn-primary button-rounded pull-right" onclick="showSampleTestQuestions()" href="javascript: void(0)"><span class="fa fa-file-text-o margin-right"></span>Sample the Test</a>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div>

<div class="modal fade school-modal in" id="teacher_students_report_modal" role="dialog" data-backdrop="static" data-keyboard="false" aria-hidden="false">
	<div class="modal-dialog large" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
				<h5 style="text-transform: none; color: #fff;">
					<div class="row">
						<div class="col-sm-4">
							<span class="pull-left">Markbook for Class: <span class="report_class_name"></span></span>
						</div>
						<div class="col-sm-4 text-center">
							<?php echo $this->session->userdata('school_name') ?>
						</div>
						<div class="col-sm-4">
							<span style="display: block; text-align: right; margin-right: 27px;">Date <?php echo date('d/m/y H:i') ?></span>
						</div>
					</div>
				</h5>
			</div>
			<div class="modal-body no-top" style="height: 500px; font-size: 11px;"></div>
		</div>
	</div>
</div>

</div>
<?php $this->load->view('school/footer.php'); ?>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/flexslider/js/jquery.flexslider-min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/r/bs/dt-1.10.9,fc-3.1.0,r-1.0.7/datatables.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.slimscroll.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/school/teacher-dashboard.js"></script>