<?php $this->load->view('school/header.php'); ?>

<div class="container school-container school-signup">
	<!-- Title -->
	<div class="row">
		<div class="col-md-12">
			<div class="title">
				<h2>School Sign Up Form</h2>
			</div>
		</div>
	</div>

	<!-- Fields -->
	<div class="row">
		<div class="col-md-6 col-md-offset-3">
			<form class="s-form">
				<span class="error-message invisible">Please input the form correctly.</span>
				<h5>School Information</h5>
				<div class="form-group col-md-12">
					<input type="text" name="name" class="form-control" required autofocus>
					<label alt="Name of School" placeholder="Name of School"></label>
				</div>
				<div class="form-group col-md-12">
					<input type="text" name="address_1" class="form-control" required>
					<label alt="Address 1" placeholder="Address 1"></label>
				</div>
				<div class="form-group col-md-12">
					<input type="text" name="address_2" class="form-control" required>
					<label alt="Address 2" placeholder="Address 2"></label>
				</div>
				<div class="form-group col-md-12">
					<input type="text" name="address_3" class="form-control" required>
					<label alt="Address 3" placeholder="Address 3"></label>
				</div>
				<div class="form-group col-md-12">
					<input type="text" name="admin_contact" class="form-control" required>
					<label alt="Admin Contact" placeholder="Admin Contact"></label>
				</div>
				<div class="form-group col-md-6">
					<input type="text" name="cellphone_num" class="form-control" required>
					<label alt="Telephone Number" placeholder="Telephone Number"></label>
				</div>
				<div class="form-group col-md-6">
					<input type="text" name="telephone_num" class="form-control" required>
					<label alt="Cellphone Number" placeholder="Cellphone Number"></label>
				</div>
				<div class="form-group col-md-6">
					<input type="text" name="website_url" class="form-control" required>
					<label alt="Website URL" placeholder="Website URL"></label>
				</div>
				<div class="form-group col-md-6">
					<input type="number" name="num_students" class="form-control" required>
					<label alt="Number of Students" placeholder="Number of Students"></label>
				</div>
				<h5>Login Details</h5>
				<div class="form-group col-md-12">
					<input type="text" name="email_address" class="form-control" required>
					<label alt="Email Address" placeholder="Email Address"></label>
				</div>
				<div class="form-group col-md-12">
					<input type="password" name="password" class="form-control" required>
					<label alt="Password" placeholder="Password"></label>
				</div>
				<div class="form-group col-md-12">
					<input type="password" name="confirm_password" class="form-control" required>
					<label alt="Confirm Password" placeholder="Confirm Password"></label>
				</div>
				<div class="form-group col-md-12">
					<button class="btn btn-primary btn-lg col-md-4 col-md-offset-4 submit">Sign Up</button>
				</div>
			</form>
		</div>
	</div>
</div>

<?php $this->load->view('school/footer.php'); ?>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/school/school-signup.js"></script>