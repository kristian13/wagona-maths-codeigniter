<div class="container navi">
	<nav role="navigation" class="navbar navbar-default navbar-static-top" style="background: #e6e6e6; border-radius:30px;">
		<div style="background: #e6e6e6; border-radius:30px;" class="container navlister">
			<p class="navbar-text school-name-nav"><span class="school-name-text" title="<?php echo $this->session->userdata('school_name'); ?>"><?php echo $this->session->userdata('school_name'); ?></span></p>
			<div style="margin-top: 7px;" class="btn-group navbar-right">
				<button data-toggle="dropdown" class="btn btn-primary dropdown-toggle" type="button">
					<span class="glyphicon glyphicon-user"></span> <?php echo $this->session->userdata('account_name'); ?> <span class="caret"></span>
				</button>
				<ul role="menu" class="dropdown-menu">
					<li><a href="#">Profile</a></li>
					<li class="divider"></li>
					<li><a href="<?php echo base_url(); ?>site/logout">Logout</a></li>
				</ul>
			</div>
		</div>
	</nav>
</div>