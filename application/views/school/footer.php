	<div class="page-bottom">
		<div class="pre-footer">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<ul class="menu center-txt">
							<li><a data-toggle="Home" class="cube_link" href="<?php echo base_url(); ?>"></a></li>
							<li><a data-toggle="Pricing" class="cube_link" href="<?php echo base_url(); ?>site/pricing"></a></li>
							<li><a data-toggle="Features" class="cube_link" href="<?php echo base_url(); ?>site/features"></a></li>
							<li><a data-toggle="About Us" class="cube_link" href="<?php echo base_url(); ?>site/aboutus"></a></li>
							<li><a data-toggle="Contact Us" class="cube_link" href="<?php echo base_url(); ?>site/contactus"></a></li>
							<li><a data-toggle="Terms and Conditions" class="cube_link" href="<?php echo base_url(); ?>site/term_condition"></a></li>
							<li><a data-toggle="Privacy Policy" class="cube_link" href="<?php echo base_url(); ?>site/privacy"></a></li>
							<li><a data-toggle="Cookie Policy" class="cube_link" href="<?php echo base_url(); ?>site/cookie"></a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div id="footer">
			<div class="footer-image"></div>
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="copyright">
							<a href="index.php"><img src="<?php echo base_url(); ?>assets/images/logo.png" alt="Wagona" title="Wagona" /></a> Copyright &copy; 2014. Wagona Maths.All Rights Reserved.
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-1.11.0.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/chosen.jquery.min.js"></script>
	<script type="text/javascript">
		//cube link
		$('.cube_link').each(function(){
			var cube_toggle = $(this).attr('data-toggle');
			$(this).append('<span class="cube_hover"><span class="origin">' + cube_toggle + '</span><span class="transform">' + cube_toggle + '</span></span>');
		});
	</script>
</body>
</html>