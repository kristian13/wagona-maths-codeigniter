<!-- School Modal -->
<div class="modal fade school-modal" id="school_list_modal" role="dialog" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
				<h4>Schools</h4>
			</div>
			<div class="modal-body">
				<!-- Nav tabs -->
				<ul class="nav nav-tabs">
					<li role="presentation" class="active">
						<a href="#school_list" role="tab" data-toggle="tab">List</a>
					</li>
					<li role="presentation">
						<a href="#school_crud" role="tab" data-toggle="tab">Create School</a>
					</li>
					<li role="presentation" class="pull-right">
						<a href="#school_help" role="tab" data-toggle="tab">Help</a>
					</li>
				</ul>

				<!-- Tab panes -->
				<div class="tab-content">
					<div role="tabpanel" class="tab-pane fade in active" id="school_list">
						<table class="table table-striped table-bordered" id="table-school-list">
							<thead>
								<tr>
									<th>Name</th>
									<th>Website</th>
									<th>Num. of Students</th>
									<th></th>
								</tr>
							</thead>
						</table>
					</div>
					<div role="tabpanel" class="tab-pane fade in" id="school_crud">
						<div class="form-container">
							<form>
								<span class="error-message invisible">Please input the form properly.</span>
								<div class="field-container">
									<div class="label-container"><span>Name of School:<span class="required pull-right">*</span></span></div>
									<div class="input-container">
										<input type="text" name="name" class="input-form">
									</div>
								</div>
								<div class="field-container">
									<div class="label-container"><span>Address 1:<span class="required pull-right">*</span></span></div>
									<div class="input-container">
										<input type="text" name="address_1" class="input-form">
									</div>
								</div>
								<div class="field-container">
									<div class="label-container"><span>Address 2:</span></div>
									<div class="input-container">
										<input type="text" name="address_2" class="input-form">
									</div>
								</div>
								<div class="field-container">
									<div class="label-container"><span>Address 3:</span></div>
									<div class="input-container">
										<input type="text" name="address_3" class="input-form">
									</div>
								</div>
								<div class="field-container">
									<div class="label-container"><span>Admin Contact:<span class="required pull-right">*</span></span></div>
									<div class="input-container">
										<input type="text" name="admin_contact" class="input-form">
									</div>
								</div>
								<div class="field-container">
									<div class="label-container"><span>Telephone number:<span class="required pull-right">*</span></span></div>
									<div class="input-container">
										<input type="text" name="telephone_num" class="input-form">
									</div>
								</div>
								<div class="field-container">
									<div class="label-container"><span>Cellphone number:<span class="required pull-right">*</span></span></div>
									<div class="input-container">
										<input type="text" name="cellphone_num" class="input-form">
									</div>
								</div>
								<div class="field-container">
									<div class="label-container"><span>Email address:<span class="required pull-right">*</span></span></div>
									<div class="input-container">
										<input type="text" name="email_address" class="input-form">
									</div>
								</div>
								<div class="field-container">
									<div class="label-container"><span>Website url:<span class="required pull-right">*</span></span></div>
									<div class="input-container">
										<input type="text" name="website_url" class="input-form">
									</div>
								</div>
								<div class="field-container">
									<div class="label-container"><span>Number of Students:<span class="required pull-right">*</span></span></div>
									<div class="input-container">
										<input type="number" min="1" name="num_students" style="width: 50%;" class="input-form">
									</div>
								</div>
								<div class="form-footer">
									<a class="btn btn-default cancel hide">Cancel</a>
									<button class="btn btn-primary submit">Save</button>
								</div>
							</form>
						</div>
						<div class="view-container hide">
							<div class="content"></div>
							<div class="form-footer">
								<a class="btn btn-default cancel hide">Cancel</a>
							</div>
						</div>
						<img src="<?php echo base_url(); ?>assets/images/loader.gif" class="loader hide" />
					</div>
					<div role="tabpanel" class="tab-pane fade in" id="school_help">
						<div class="rect-el-full rect-el lgray-bg removepad remarg">
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem cupiditate asperiores minima. Ullam repellat ab magnam pariatur laborum eos enim. Facere amet quibusdam, aspernatur nihil ut dignissimos qui cumque! Eos!</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>