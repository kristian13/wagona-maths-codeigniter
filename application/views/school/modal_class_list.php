<!-- Class Modal -->
<div class="modal fade school-modal" id="class_list_modal" role="dialog" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
				<h4>Forms</h4>
			</div>
			<div class="modal-body">
				<!-- Nav tabs -->
				<ul class="nav nav-tabs">
					<li role="presentation" class="active">
						<a href="#class_list" role="tab" data-toggle="tab">List</a>
					</li>
					<li role="presentation">
						<a href="#class_crud" role="tab" data-toggle="tab">Create Class</a>
					</li>
					<li role="presentation" class="pull-right">
						<a href="#class_help" role="tab" data-toggle="tab">Help</a>
					</li>
				</ul>

				<!-- Tab panes -->
				<div class="tab-content">
					<div role="tabpanel" class="tab-pane fade in active" id="class_list">
						<label for="#list_subject">Stage&emsp;</label>
						<select id="list_subject" name="list_subject" data-placeholder="Select Stage">
							<option value="">ALL</option>
							<?php foreach($subjects as $key => $val): ?>
							<option value="<?php echo $val['subject_id'] ?>"><?php echo $val['description'] ?></option>
							<?php endforeach; ?>
						</select>
						<table class="table table-striped table-bordered" id="table-class-list">
							<thead>
								<tr>
									<th>Name</th>
									<th>Form</th>
									<th>Status</th>
									<th>Edit</th>
								</tr>
							</thead>
						</table>
					</div>
					<div role="tabpanel" class="tab-pane fade in" id="class_crud">
						<div class="form-container">
							<form>
								<span class="error-message invisible">Please input the form properly.</span>
								<div class="field-container">
									<div class="label-container"><span>Name of Class:<span class="required pull-right">*</span></span></div>
									<div class="input-container">
										<input type="text" name="name" class="input-form">
									</div>
								</div>
								<div class="field-container">
									<div class="label-container"><span>Stage:<span class="required pull-right">*</span></span></div>
									<div class="input-container">
										<select id="subject" name="subject" data-placeholder="Select Stage" class="input-form">
											<option value=""></option>
											<?php foreach($subjects as $key => $val): ?>
											<option value="<?php echo $val['subject_id'] ?>"><?php echo $val['description'] ?></option>
											<?php endforeach; ?>
										</select>
									</div>
								</div>
								<div class="form-footer">
									<a class="btn btn-default cancel hide">Cancel</a>
									<button class="btn btn-primary submit">Save</button>
								</div>
							</form>
						</div>
						<div class="view-container hide">
							<div class="content"></div>
							<div class="form-footer">
								<a class="btn btn-default cancel hide">Cancel</a>
							</div>
						</div>
						<img src="<?php echo base_url(); ?>assets/images/loader.gif" class="loader hide" />
					</div>
					<div role="tabpanel" class="tab-pane fade in" id="class_help">
						<div class="rect-el-full rect-el lgray-bg removepad remarg">
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem cupiditate asperiores minima. Ullam repellat ab magnam pariatur laborum eos enim. Facere amet quibusdam, aspernatur nihil ut dignissimos qui cumque! Eos!</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>