<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/favicon.png" type="image/x-icon">
	<link rel="icon" href="<?php echo base_url(); ?>assets/images/favicon.png" type="image/x-icon">

	<title>Wagona Maths</title>

	<link href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/flexslider/css/flexslider.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/animate.css">
	<link href='http://fonts.googleapis.com/css?family=Ubuntu:400,300' rel='stylesheet' type='text/css'>
	<link href="<?php echo base_url(); ?>assets/style-icomoon.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>assets/style.css" rel="stylesheet">

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="assets/bootstrap/js/html5shiv.js"></script>
	<script src="assets/bootstrap/js/respond.min.js"></script>
	<![endif]-->
	<script>
		var url = '<?php echo base_url(); ?>';
	</script>
</head>

<body>
	<header id="header">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="header-menu animated">
						<div class="header-socmed">
							<ul class="socmed menu">
								<li>
									<a href="#" class="rubber-it">
										<span class="soc-icon-circle">
											<span class="fa fa-facebook"></span>
										</span>
									</a>
								</li>
								<li>
									<a href="#" class="rubber-it">
										<span class="soc-icon-circle">
											<span class="fa fa-twitter"></span>
										</span>
									</a>
								</li>
								<li>
									<a href="#" class="rubber-it">
										<span class="soc-icon-circle">
											<span class="fa fa-google-plus"></span>
										</span>
									</a>
								</li>
								<li>
									<a href="#" class="rubber-it">
										<span class="soc-icon-circle">
											<span class="fa fa-youtube"></span>
										</span>
									</a>
								</li>
							</ul>
						</div>
						<div class="row">
							<div class="col-md-4 cus-md-head-1 plr-0 hidden-sm hidden-xs">
								<ul class="menu menu-header nav menu-left">
									<li><a data-toggle="Home" class="cube_link" href="<?php echo base_url(); ?>"></a></li>
									<li><a data-toggle="About Us" class="cube_link" href="<?php echo base_url(); ?>site/aboutus"></a></li>
									<li><a data-toggle="How It Works" class="cube_link" href="<?php echo base_url(); ?>site/howitworks"></a></li>
									<li><a data-toggle="Schools" class="cube_link" href="<?php echo base_url(); ?>site/schools"></a></li>
									<li><a data-toggle="Methodology" class="cube_link" href="<?php echo base_url(); ?>site/methodology"></a></li>
								</ul>
							</div>
							<div class="col-md-4 cus-md-head-2">
								<div id="logo">
									<a href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>assets/images/logo2r.gif" class="imgr" alt="Wagona" title="Wagona" /></a>
								</div>
							</div>
							<div class="col-md-4 cus-md-head-1 plr-0 hidden-sm hidden-xs">
								<ul class="menu menu-header nav menu-right">
									<li><a data-toggle="Features" class="cube_link" href="<?php echo base_url(); ?>site/features"></a></li>
									<li><a data-toggle="Pricing" class="cube_link" href="<?php echo base_url(); ?>site/pricing"></a></li>
									<li><a data-toggle="Contact Us" class="cube_link" href="<?php echo base_url(); ?>site/contactus"></a></li>
									<?php if(@$this->session->userdata('login')): ?>

										<?php if($this->session->userdata('school_user') == 'teacher'): ?>
										<li><a data-toggle="Account" class="cube_link" href="<?php echo base_url(); ?>teacher"></a></li>
										<?php elseif($this->session->userdata('school_user') == 'school'): ?>
										<li><a data-toggle="Account" class="cube_link" href="<?php echo base_url(); ?>school"></a></li>
										<?php else: ?>
										<li><a data-toggle="Account" class="cube_link" href="<?php echo base_url(); ?>account/dashboard"></a></li>
										<?php endif; ?>

									<li><a data-toggle="Logout" class="cube_link" href="<?php echo base_url(); ?>site/logout"></a></li>
									<?php else: ?>
									<li><a data-toggle="Login" class="cube_link" href="<?php echo base_url(); ?>site/login"></a></li>
									<li><a data-toggle="Sign Up" class="cube_link" href="<?php echo base_url(); ?>site/pricing"></a></li>
									<?php endif; ?>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>