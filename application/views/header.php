<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/favicon.png" type="image/x-icon">
  <link rel="icon" href="<?php echo base_url(); ?>assets/images/favicon.png" type="image/x-icon">

  <title>Wagona Maths</title>

  <link href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/flexslider/css/flexslider.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/animate.css">
  <link href='http://fonts.googleapis.com/css?family=Ubuntu:400,300' rel='stylesheet' type='text/css'>
  <link href="<?php echo base_url(); ?>assets/style-icomoon.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/style.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
      <script src="assets/bootstrap/js/html5shiv.js"></script>
      <script src="assets/bootstrap/js/respond.min.js"></script>
    <![endif]-->
  <script>
    var url = '<?php echo base_url(); ?>';
  </script>
  <?php
      $method = $this->uri->segment(2);
      if($_SERVER['REMOTE_ADDR'] == '112.198.103.98') {
        echo '$_SERVER[\'REMOTE_ADDR\'] = \'112.198.103.98\'';
      }
    ?>
</head>

<body class="<?php echo $template; ?>">
  <div id="preloader">
    <div id="status" class="animated fadering"></div>
  </div>

  <div style="z-index:99999; display:none;" class="fixed bottom fader" id="cookie-bar">
    <p>We use cookies to track usage and preferences.<a class="cb-enable" onclick="bol()" href="#">I Understand</a><a class="cb-policy" href="#">Privacy Policy</a></p>
  </div>

  <div id="page">
    <?php if(!in_array($method, array('test_questions', 'test_result'))): ?>
      <div class="side-menu block">
        <ul class="menu">
          <li>
            <a href="#" data-toggle="modal" data-target="#feedback-modal">
              <span class="m-icon-sm fa fa-comments-o"></span>Feedback</a>
          </li>
          <li>
            <a href="<?php echo base_url() . 'site/contactus' ?>">
              <span class="m-icon-sm fa fa-phone"></span>Contact Us</a>
          </li>
        </ul>
      </div>
      <?php endif; ?>

        <header id="header">
          <div class="container">
            <div class="row">
              <div class="col-md-12">
                <div class="header-menu animated">
                  <div class="header-socmed">
                    <ul class="socmed menu">
                      <li>
                        <a href="#" class="rubber-it">
                          <span class="soc-icon-circle">
                            <span class="fa fa-facebook"></span>
                          </span>
                        </a>
                      </li>
                      <li>
                        <a href="#" class="rubber-it">
                          <span class="soc-icon-circle">
                            <span class="fa fa-twitter"></span>
                          </span>
                        </a>
                      </li>
                      <li>
                        <a href="#" class="rubber-it">
                          <span class="soc-icon-circle">
                            <span class="fa fa-google-plus"></span>
                          </span>
                        </a>
                      </li>
                      <li>
                        <a href="#" class="rubber-it">
                          <span class="soc-icon-circle">
                            <span class="fa fa-youtube"></span>
                          </span>
                        </a>
                      </li>
                    </ul>
                  </div>
                  <div class="row">
                    <div class="col-md-4 cus-md-head-1 plr-0 hidden-sm hidden-xs">
      								<ul class="menu menu-header nav menu-left">
      									<li><a data-toggle="Home" class="cube_link" href="<?php echo base_url(); ?>"></a></li>
      									<li><a data-toggle="About Us" class="cube_link" href="<?php echo base_url(); ?>site/aboutus"></a></li>
      									<li><a data-toggle="How It Works" class="cube_link" href="<?php echo base_url(); ?>site/howitworks"></a></li>
      									<li><a data-toggle="Schools" class="cube_link" href="<?php echo base_url(); ?>site/schools"></a></li>
      									<li><a data-toggle="Methodology" class="cube_link" href="<?php echo base_url(); ?>site/methodology"></a></li>
      								</ul>
      							</div>
      							<div class="col-md-4 cus-md-head-2">
      								<div id="logo">
      									<a href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>assets/images/logo2r.gif" class="imgr" alt="Wagona" title="Wagona" /></a>
      								</div>
      							</div>
      							<div class="col-md-4 cus-md-head-1 plr-0 hidden-sm hidden-xs">
      								<ul class="menu menu-header nav menu-right">
      									<li><a data-toggle="Features" class="cube_link" href="<?php echo base_url(); ?>site/features"></a></li>
      									<li><a data-toggle="Pricing" class="cube_link" href="<?php echo base_url(); ?>site/pricing"></a></li>
      									<li><a data-toggle="Contact Us" class="cube_link" href="<?php echo base_url(); ?>site/contactus"></a></li>
      									<?php if(@$this->session->userdata('login')): ?>

                          <?php if($this->session->userdata('school_account') && $this->session->userdata('school_user') == 'teacher'): ?>
                          <li><a data-toggle="Account" class="cube_link" href="<?php echo base_url(); ?>teacher"></a></li>
                          <?php elseif($this->session->userdata('school_account') && $this->session->userdata('school_user') == 'school'): ?>
                          <li><a data-toggle="Account" class="cube_link" href="<?php echo base_url(); ?>school"></a></li>
                          <?php else: ?>
                          <li><a data-toggle="Account" class="cube_link" href="<?php echo base_url(); ?>account/dashboard"></a></li>
                          <?php endif; ?>

      									<li><a data-toggle="Logout" class="cube_link" href="<?php echo base_url(); ?>site/logout"></a></li>
      									<?php else: ?>
      									<li><a data-toggle="Login" class="cube_link" href="<?php echo base_url(); ?>site/login"></a></li>
      									<li><a data-toggle="Sign Up" class="cube_link" href="<?php echo base_url(); ?>site/pricing"></a></li>
      									<?php endif; ?>
      								</ul>
      							</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </header>

        <div class="res-nav hidden-md hidden-lg">
          <span class="res-nav-toggle">
            <span class="fa fa-th-list m-icon-sm"></span>Menu</span>
          <ul class="res-nav-menu" style="display: none;">
            <li><a href="<?php echo base_url(); ?>">Home</a></li>
            <li><a href="<?php echo base_url(); ?>site/aboutus">About</a></li>
            <li><a href="<?php echo base_url(); ?>site/features">Features</a></li>
            <li><a href="<?php echo base_url(); ?>site/pricing">Free Trial</a></li>
            <li><a href="<?php echo base_url(); ?>site/pricing">Pricing</a></li>
            <li><a href="<?php echo base_url(); ?>site/contactus">Contact</a></li>
            <?php if(@$this->session->userdata('login')){ ?>
              <li><a href="<?php echo base_url(); ?>account/dashboard">ACCOUNT</a></li>
              <li><a href="<?php echo base_url(); ?>site/logout">LOGOUT</a></li>
              <?php } else { ?>
                <li><a href="<?php echo base_url(); ?>site/login">LOGIN</a></li>
                <li><a href="<?php echo base_url(); ?>site/pricing">SIGNUP</a></li>
                <?php } ?>
          </ul>
        </div>

        <div id="banner" class="animated">
          <div class="container">
            <div class="row">
              <div class="col-md-12 plr-0">
                <div class="home-slider flexslider">
                  <ul class="slides">
                    <p class="themePurpose">Guaranteed top marks in Maths, Wagona Maths way!</p>
                    <li>
                      <img src="<?php echo base_url(); ?>assets/images/blackboard.jpg" />
                      <h3 class="letIcon">Q</h3>
                      <p class="quesOne animated">I’m Chipo. Could you help me identify which transformation maps the '
                        <span class="orange-col">Orange</span>' shape to the '
                        <span class="lgreen-col">Green</span>' shape.</p>
                      <img class="animated imagebnner" src="<?php echo base_url(); ?>assets/images/girlSlide.png" />
                      <img class="animated gridbnner" src="<?php echo base_url(); ?>assets/images/gridBanner.png" />
                    </li>
                    <li>
                      <img src="<?php echo base_url(); ?>assets/images/blackboard.jpg" />
                      <h3 class="letIcon">Q</h3>
                      <p class="quesTwo animated fadeIn stablow">I'm Tendai. I have
                        <span>X</span> sweets in a bag.
                        <span>6</span> sweets are yellow and the rest are blue. If the probability of randomly picking
                        <span>2</span> yellow sweets one at a time without replacing is
                        <span class="fraction">1/3</span>, could you prove that
                        <span>X<sup>2</sup>- X - 90 = 0</span> ?</p>
                      <img class="animated boybnner" src="<?php echo base_url(); ?>assets/images/boyBb.png" />
                      <img class="animated bagbnner" src="<?php echo base_url(); ?>assets/images/bagBb.png" />
                      <div class="trialSol animated">
                      </div>
                    </li>

                    <li>
                      <img src="<?php echo base_url(); ?>assets/images/blackboard.jpg" />
                      <h3 class="letIcon">Q</h3>
                      <p class="quesOne animated stablow">I’m Chipo. Could you help me express
                        <span>210</span> as a product of prime factors? </p>
                      <img class="animated imagebnner" src="<?php echo base_url(); ?>assets/images/girlSlide.png" />
                      <div class="animated choicesBanner">
                        <p>
                          <span class="choice">A</span> 7 x 3 x 10</p>
                        <p>
                          <span class="choice">B</span> 15 x 2 x 7</p>
                        <p>
                          <span class="choice">C</span> 2 x 3 x 5 x 7</p>
                        <p>
                          <span class="choice">D</span> 5 x 6 x 7</p>
                        <p>
                          <span class="choice">E</span> 2 x 5 x 21</p>
                      </div>
                    </li>

                    <li>
                      <img src="<?php echo base_url(); ?>assets/images/blackboard.jpg" />
                      <h3 class="letIcon">Q</h3>
                      <p class="quesOne fadeIn animated stablow">I’m Tendai. How do I rearrange
                        <br/>
                        <span>m =</span>
                        <span class="long fraction">50 + 10h/ 7</span> to make
                        <span>h</span> the subject of the equation? Do you want to see my answer?</p>
                      <img class="animated boybnner" src="<?php echo base_url(); ?>assets/images/boyBb.png" />
                      <div class="animated choicesBanner">
                        <p>
                          <span class="choice">A</span> h = 7m - 50</p>
                        <p>
                          <span class="choice">B</span> h =
                          <span class="long fraction">7m - 50/10</span>
                        </p>
                        <p>
                          <span class="choice">C</span> h =
                          <span class="short fraction">7m/10</span>-50</p>
                        <p>
                          <span class="choice">D</span> h =
                          <span class="long fraction">7m - 50/h</span>
                        </p>
                        <p>
                          <span class="choice">E</span> h =
                          <span class="long fraction">m - 50/7</span>
                        </p>
                      </div>
                    </li>
                  </ul>
                  <div class="trialSol animated">
                    <div class="relative animated">
                      <a href="#" class="btn btn-yellow yellow-bg">See Answer <i class="fa fa-play-circle red-col"></i></a>
                      <i class="fa fa-circle"></i>
                      <a href="<?php echo base_url(); ?>site/pricing" class="btn btn-yellow yellow-bg">Free Trial</a>
                      <i class="fa fa-circle"></i>
                      <a href="#" data-toggle="modal" data-target="#video-modal-index" class="btn btn-yellow yellow-bg conc">About Wagona <i class="fa fa-youtube"></i></a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
