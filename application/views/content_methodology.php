<?php $this->load->view('header.php'); ?>

<style>
  #check-topic {
    height: 300px;
    overflow-y: scroll;
    padding: 0 17px;
  }
</style>

<div id="page" class="fader content-row">
  <div class="container">
    <div class="row">

      <div class="col-md-12">

        <div class="divAbout">

          <div class="aboutBox1" id="how">

            <p class="about-title">Methodology</p>
            <p>
              The BRAG system is our very own creation that gives real time data on every child’s performance and anticipates potential areas of concern. The Mathematics Diagnostic Tool is wagona.com’s assessment tool that identifies the weaknesses and strengths, knowledge gaps and misconceptions of every student . Using the Mathematics Diagnostic Tool,
              wagona.com is able to prescribe the right ways to resolve the areas of weaknesses and consolidating the strong ones.
            </p>
            <p>We use a four step step cycle in our approach.</p>

          </div>
        </div>
      </div>

    </div>
  </div>

  <div class="container">
    <div class="row">
      <div class="col-md-6 col-sm-6">
        <i class="fa fa-chevron-down"></i>
        <div class="imageLink">
          <a class="invi brag rotator" data-pos="90" onclick="bragShow()">Brag</a>
          <a class="invi diag rotator selected-cycle" data-pos="360" onclick="diagShow()">Diag</a>
          <a class="invi teach rotator" data-pos="270" onclick="teachShow()">Howi</a>
          <a class="invi prac rotator" data-pos="180" onclick="pracShow()">prac</a>
          <img id="cycleRotate" src="<?php echo base_url(); ?>assets/images/cycleImage.png" class="absolute"/>
          <img id="blank" class="absolute imagefull" src="<?php echo base_url(); ?>assets/images/blank.png"/>
        </div>
        <img id="still" class="absolute" src="<?php echo base_url(); ?>assets/images/checkImage.png"/>
      </div>

      <div class="col-md-6 col-sm-6 heightLimiter">
        <div id="diagnosis" class="aboutBox black-bg animated">

          <p class="about-title white-col">DIAGNOSIS</p>

          <p>
            Knowledge gaps and Misconceptions are the major reasons why children struggle with mathematics. This makes starting with an assessment of
            student capabilities and weaknesses a natural starting point. The Mathematics Diagnostic Tool is created not only to score a child’s
            performance but to give an analysis of the performance in order to create a bespoke study programme geared to deliver improved results.
          </p>
        </div>

        <div class="aboutBox hide redx-bg animated" id="teach">

          <p class="about-title white-col">TEACH</p>

          <p>Every question on our platform comes with smart answers and  an explanation of the answers. A student can use the explanations to correct themselves or seek help on the particular  areas of concern.</p>
          <p> Teachers and Tutors can also use the diagnosis tool as a  SMART Intervention tool as it allows for targeted assistance to be given to students rather than the traditional generic classroom approach. </p>
        </div>

        <div id="practice" class="aboutBox hide orange-bg animated">

          <p class="about-title white-col">PRACTICE </p>

          <p>
            We believe practice makes perfect! The platform contains thousands of questions all containing SMART answers to allow a student to practice any particular topics until they
            become confident. With 24/7 access Maths practice can be done conveniently.
          </p>

        </div>

        <div id="brag" class="aboutBox hide greenx-bg animated">
          <p class="about-title white-col bragster"><b class="black-col">B</b><b class="red-col">R</b><b class="yellow-col">A</b><b class="green-col">G</b></p>
          <p>
            Progress on the platform is tracked through our innovative BRAG system. This is a traffic light system that monitors performance by collating practice and test statistics to create indicators that can be used to
           anticipate potential problem areas while consolidating mastered concepts.
          </p>
          <p>
            <b class="black-col">Black</b> - areas that have not yet been assessed<br/>
            <b class="red-col">Red</b> - area of high concern / high attention required<br/>
            <b class="yellow-col">Amber</b> - medium concern/ medium attention required<br/>
            <b class="green-col">Green</b> - areas good performance but will require constant validation.
          </p>
        </div>
      </div>

    </div>
  </div>
</div>

<?php $this->load->view('footer.php'); ?>
<script>
var currentRotatePosition = 0;
var currentDegree = 0;
$(function() {
  $(".rotator").click(function(){
    var deg = $(this).attr("data-pos");

    //$(".imageLink").css("transform","rotate(0deg)");
    $(".imageLink").css({"transform":"rotateZ("+deg+"deg)"});
    $("#still").css("transform","rotate(0deg)");


  });


});

function hideAll() {

    $('#brag').addClass('hide');
    $('#diagnosis').addClass('hide');
    $('#teach').addClass('hide');
    $('#practice').addClass('hide');
    $('#brag').removeClass('zoomInRight');
    $('#diagnosis').removeClass('zoomInRight');
    $('#teach').removeClass('zoomInRight');
    $('#practice').removeClass('zoomInRight');
  }

  function bragShow() {
    hideAll();
    $('#brag').removeClass('hide');
    $('#brag').addClass('zoomInRight fadeIn');
  }

  function diagShow() {
    hideAll();
    $('#diagnosis').removeClass('hide');
    $('#diagnosis').addClass('zoomInRight fadeIn');
  }

  function teachShow() {
    hideAll();
    $('#teach').removeClass('hide');
    $('#teach').addClass('zoomInRight fadeIn');
  }

  function pracShow() {
    hideAll();
    $('#practice').removeClass('hide');
    $('#practice').addClass('zoomInRight fadeIn');
  }

</script>
