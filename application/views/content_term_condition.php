<?php $this->load->view('header.php'); ?>

<div id="content" class="content-row">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="title">
					<h2><span class="green-col">Terms and Conditions</span></h2>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">

				<div class="panel-group" id="terms-condition">
				  <div class="panel panel-default">
				    <div class="panel-heading">
				      <h4 class="panel-title">
				        <a data-toggle="collapse" data-parent="#terms-condition" href="#collapse-1">
				          General
				        </a>
				      </h4>
				    </div>
				    <div id="collapse-1" class="panel-collapse collapse in">
				      <div class="panel-body">
				        <p>Thank you for visiting Wagona website and reviewing our Terms & Conditions (T&Cs) and Privacy Policy. Please read the T&Cs and Privacy Policy before using the site or submitting any personal information.</p>
				        <p>Wagona may update the T&Cs Privacy Policy at its own discretion. All changes will be duly documented at the bottom of our website. Wagona recommends our subscribers to frequently check for changes and should you have any concerns or suggestions, please contact us.</p>
				      </div>
				    </div>
				  </div>
				  <div class="panel panel-default">
				    <div class="panel-heading">
				      <h4 class="panel-title">
				        <a data-toggle="collapse" data-parent="#terms-condition" href="#collapse-2">
				          Use of our websites  
				        </a>
				      </h4>
				    </div>
				    <div id="collapse-2" class="panel-collapse collapse">
				      <div class="panel-body">
				        <p>The use of Wagona website is subject to the Terms & Conditions set forth below. By using our websites, you are agreeing to these legal T&Cs whether on a free trial or paying subscriber. By accepting our T&Cs you agree to keep these clauses for the use of all services available at Wagona’s site. </p>
				      </div>
				    </div>
				  </div>
				  <div class="panel panel-default">
				    <div class="panel-heading">
				      <h4 class="panel-title">
				        <a data-toggle="collapse" data-parent="#terms-condition" href="#collapse-3">
				          Intellectual Property  
				        </a>
				      </h4>
				    </div>
				    <div id="collapse-3" class="panel-collapse collapse">
				      <div class="panel-body">
				        <p>Wagona creates software, audio-video presentations and other intellectual property which will benefit students, teachers and schools. In order for all the material is to be copyrighted, trademarked, or offered for distribution, there must be a clear understanding Wagonad holds the ownership of the property.</p>
				        <ul>
				        	<li>Wagona Website Content may include links to other websites, quotations, images, photos, graphics, multimedia and interactive displays on the website. All external quotes and materials will have link to the original source, and copyrights will be duly kept.</li>
				        	<li>The content, layout, design, graphics and databases on this website are proprietary to Wagona. Unless explicitly permitted by Wagona no part of the website may be reproduced, or distributed in public other than website of Wagona. In the good spirit of mutual collaboration Wagona reserves the right to provide materials to schools as part of their partnership.</li>
				        	<li>If the intellectual property was created and developed by Wagona employee as part of his/her involvement with Wagona, the intellectual property shall remain the sole and exclusive property of the Wagona.</li>
				        </ul>
				      </div>
				    </div>
				  </div>
				  <div class="panel panel-default">
				    <div class="panel-heading">
				      <h4 class="panel-title">
				        <a data-toggle="collapse" data-parent="#terms-condition" href="#collapse-4">
				          User ID and Password 
				        </a>
				      </h4>
				    </div>
				    <div id="collapse-4" class="panel-collapse collapse">
				      <div class="panel-body">
				      	<p>After registration, all our users will be provided with a user ID and password. Your user ID and password will be confidential. Wagona will not be responsible for any breach of security of your User ID and password. We recommend that when creating a password, it needs to be long and strong, with a minimum of eight characters and a mix of upper and lowercase letters, numbers and symbols. You should not share your password with others and shall at all times be responsible and liable for any transactions or activities that occur on your account. In the event of any unauthorised use of your account please contact us immediately.</p>
				      </div>
				    </div>
				  </div>
				  <div class="panel panel-default">
				    <div class="panel-heading">
				      <h4 class="panel-title">
				        <a data-toggle="collapse" data-parent="#terms-condition" href="#collapse-5">
				          License to use website 
				        </a>
				      </h4>
				    </div>
				    <div id="collapse-5" class="panel-collapse collapse">
				      <div class="panel-body">
				      	<p>Our users may view, download, and print pages with lessons and exercises from the website for own personal use, subject to the restrictions below.</p>
				      	<p>Users must not:
				      		(a) Sell or sub-license material from Wagona website;<br>
				      		(b) Republish material from Wagona website (including republication on another website);<br>
				      		(c) Copy or use material on Wagona website for commercial purposes;<br>
				      		(d) Edit or otherwise modify any material on the website;
				      	</p>
				      </div>
				    </div>
				  </div>
				  <div class="panel panel-default">
				    <div class="panel-heading">
				      <h4 class="panel-title">
				        <a data-toggle="collapse" data-parent="#terms-condition" href="#collapse-6">
				          Virus protection    
				        </a>
				      </h4>
				    </div>
				    <div id="collapse-6" class="panel-collapse collapse">
				      <div class="panel-body">
				      	<p>Wagona website and all publications are duly protected by an Antivirus program (name). We would recommend you always to run an antivirus program on all material downloaded from the Internet. We bear no responsibility for any loss, or damage to your data or your computer system which may occur whilst using material derived from this website.</p>
				      </div>
				    </div>
				  </div>
				  <div class="panel panel-default">
				    <div class="panel-heading">
				      <h4 class="panel-title">
				        <a data-toggle="collapse" data-parent="#terms-condition" href="#collapse-7">
				          Costs of Accessing the Services   
				        </a>
				      </h4>
				    </div>
				    <div id="collapse-7" class="panel-collapse collapse">
				      <div class="panel-body">
				      	<p>Effective upon acceptance of these T&Cs, we hereby grant our users, a personal, nontransferable, revocable license to access and use the services for a non-commercial use. Price list of our services you can find [URL link]. Without explicit permit from Wagona users cannot sell, resell, reproduce, or copy for commercial purposes, any portion of the services.</p>
				      </div>
				    </div>
				  </div>
				  <div class="panel panel-default">
				    <div class="panel-heading">
				      <h4 class="panel-title">
				        <a data-toggle="collapse" data-parent="#terms-condition" href="#collapse-8">
				          Variation    
				        </a>
				      </h4>
				    </div>
				    <div id="collapse-8" class="panel-collapse collapse">
				      <div class="panel-body">
				      	<p>We may revise these T&Cs from time to time. The revised T&Cs will apply to the use of our website from the date of their publication on our website.</p>
				      </div>
				    </div>
				  </div>
				  <div class="panel panel-default">
				    <div class="panel-heading">
				      <h4 class="panel-title">
				        <a data-toggle="collapse" data-parent="#terms-condition" href="#collapse-9">
				          Registrations and authorisations     
				        </a>
				      </h4>
				    </div>
				    <div id="collapse-9" class="panel-collapse collapse">
				      <div class="panel-body">
				      	<p>Wagona is registered with [trade register].<br>
						Our registration number is [number].]</p>	
						<p>[We are registered with [professional body]. Our professional title is [title] and it has been granted in the United Kingdom. We are subject to the [rules] which can be found at [URL].]</p>
						<p>[We subscribe to the following code[s] of conduct: [code(s) of conduct]. [These codes/this code] can be consulted electronically at [URL(s)].]</p>
						<p>[Our VAT number is [number].]</p>	
				      </div>
				    </div>
				  </div>
				  <div class="panel panel-default">
				    <div class="panel-heading">
				      <h4 class="panel-title">
				        <a data-toggle="collapse" data-parent="#terms-condition" href="#collapse-10">
				          Law and jurisdiction     
				        </a>
				      </h4>
				    </div>
				    <div id="collapse-10" class="panel-collapse collapse">
				      <div class="panel-body">
				      	<p>These T&Cs will be governed by and interpreted to comply with English law, and any disputes should not be resolved without obtaining expert intervention from a lawyer qualified in the respective jurisdiction.</p>
				      	<p>The failure of Wagona to exercise our rights under these Terms & Conditions will not be perceived as a waiver of such rights, nor will influence the validity of these T&Cs. The provisions these T&Cs relate to the intellectual property, use restrictions, liability limitations and damages shall survive T&Cs termination or expiration for any reason.</p>
				      </div>
				    </div>
				  </div>

				</div>

			</div>
		</div>
	</div>
</div>

<?php $this->load->view('footer.php'); ?>