<?php $this->load->view('header.php'); ?>
<style>
  #check-topic {
	  height: 300px;
	  overflow-y: scroll;
	  padding: 0 17px;
	}
</style>

<div id="page" class="fader content-row">
	<div class="container">
		<div class="row">

		  <div class="col-md-3">
		    <ul id="features_list_1" class="features-list menu">
              <li data-toggle="#methodology" class="methodology active">
              	<a href="javascript:void(0);">
              	  <span class="m-icon-sm features-icon fa fa-star-o"></span>Methodology
              	</a>
              </li>
              <li data-toggle="#alternative-approach" class="alternative-approach">
              	<a href="javascript:void(0);">
                  <span class="m-icon-sm features-icon fa fa-picture-o"></span>Alternative approach
                </a>
              </li>
              <li data-toggle="#money-back" class="money-back">
              	<a href="javascript:void(0);">
              	  <span class="m-icon-sm features-icon fa fa-thumbs-up"></span>
              	    Satisfaction guarantee
              	</a>
              </li>
              <li data-toggle="#fresh-content" class="fresh-content">
              	<a href="javascript:void(0);">
              	  <span class="m-icon-sm features-icon fa fa-leaf"></span>Fresh content
                </a>
              </li>
              <li data-toggle="#bestspoke-study-program" class="bestspoke-study-program">
              	<a href="javascript:void(0);">
              	  <span class="m-icon-sm features-icon fa fa-book"></span>Bespoke study programme
              	</a>
              </li>
              <li data-toggle="#lively-engaging-learning" class="lively-engaging-learning">
              	<a href="javascript:void(0);">
                  <span class="m-icon-sm features-icon fa fa-smile-o"></span>Lively engaging learning
                </a>
              </li>
              <li data-toggle="#virtual-classroom" class="virtual-classroom">
              	<a href="javascript:void(0);">
              	  <span class="m-icon-sm features-icon fa fa-eye"></span>Virtual classrooom
              	</a>
              </li>
              <li data-toggle="#interactive-fun-learning" class="interactive-fun-learning">
              	<a href="javascript:void(0);">
                 <span class="m-icon-sm features-icon fa fa-weixin"></span>Interactive fun learning
                </a>
              </li>
              <li data-toggle="#national-curriculum" class="national-curriculum">
              	<a href="javascript:void(0);">
              	  <span class="m-icon-sm features-icon fa fa-flag"></span>National curriculum
              	</a>
              </li>
              <li data-toggle="#online-child-safety" class="online-child-safety">
              	<a href="javascript:void(0);">
                  <span class="m-icon-sm features-icon fa fa-shield"></span>Online child safety
                </a>
              </li>
            </ul>	
		  </div>

		  <div class="col-md-6">
        <div id="features">
          <div id="methodology" class="fc-content animated animated-show flipInX">

            <div class="text-content">
    			    <h2>We have tons to 
               <b class="black-col">B</b><b class="red-col">R</b><b class="yellow-col">A</b><b class="green-col">G</b>
               about!
              </h2>	

              <img src="<?php echo base_url(); ?>assets/images/dashboard-scope.jpg" />

              <p>Ever thought of the easiest way to monitor each of your student performance?</p>

    	   	    <p> We have! 
                <b class="black-col">B</b><b class="red-col">R</b><b class="yellow-col">A</b><b class="green-col">G</b>
                which stands for Black, Red, Amber and Green, is our own toll to measure students' strengths and weaknesses using
              </p>

              <p>The brainchild of 30 years worth of teaching experience <b class="black-col">B</b><b class="red-col">R</b><b class="yellow-col">A</b><b class="green-col">G</b>
               efficiently tells you if your student is actually learning or not.
              </p>

              <p>Now, educators can create better lesson plans to target each child's performance and mold them ito becoming master of academic subjects.</p>
             
              <p><i class="bstalic">Apart from easy assesment, <b class="black-col">B</b><b class="red-col">R</b><b class="yellow-col">A</b><b class="green-col">G</b>
               is available for you to try now -- for free.
              </i></p>
              <div class="btn-group feat_buttons">
               <a class="btn btn-default btn-lg button-box yellow2-bg">Free Trial</a>
               <a class="btn btn-default btn-lg button-box yellow2-bg">FAQ's</a>
               <a class="btn button-arrow" alt="rightarrow"><span class="fa fa-angle-right" aria-hidden="true"></span></a>
              </div>
            </div>
          </div>

          <div id="alternative-approach" class="fc-content">

            <div class="text-content">
              <h2>We provide various learning tools students won’t get bored of.
              </h2> 

              <img src="<?php echo base_url(); ?>assets/images/2.-Improve-on-your-results.png" alt="" class="pull-left" />
              <p>Traditional classroom settings is mainstream education. wagonaMaths.com has however come up with an e-learning tool that is most effective when used to complement classroom education. This in turn facilitates for successful education— which is why it’s high time we innovated.</p>
              <p>wagonaMaths.com has a plethora of teaching and learning tools like video,  live lessons, practice tests, mock tests among others. All these will keep not only students but also teachers and parents interested in becoming the best they can be.</p>
              <p>Of course, this makes Mathematics exciting!</p>
              <div class="btn-group feat_buttons">
               <a class="btn button-arrow" alt="leftarrow"><span class="fa fa-angle-left" aria-hidden="true"></span></a>
               <a class="btn btn-default btn-lg button-box yellow2-bg">Free Trial</a>
               <a class="btn btn-default btn-lg button-box yellow2-bg">FAQ's</a>
               <a class="btn button-arrow" alt="rightarrow"><span class="fa fa-angle-right" aria-hidden="true"></span></a>
              </div>
            </div>


          </div>

          <div id="money-back" class="fc-content">

            <div class="text-content">
              <h2> You are guaranteed to be fully satisfied.
              </h2> 

              <img src="<?php echo base_url(); ?>assets/images/3.-Money-Back-Guarantee.png" alt="" class="pull-left" />
              <p>wagonaMaths.com gives high regard in earning the trust and confidence of our community. This includes ensuring that their investment is worth it.</p>
              <p>
                While we strongly believe joining us is one of the best decisions you will have made
                for the future development of your child
              </p>
              <div class="btn-group feat_buttons">
               <a class="btn button-arrow" alt="leftarrow"><span class="fa fa-angle-left" aria-hidden="true"></span></a>
               <a class="btn btn-default btn-lg button-box yellow2-bg">Free Trial</a>
               <a class="btn btn-default btn-lg button-box yellow2-bg">FAQ's</a>
               <a class="btn button-arrow" alt="rightarrow"><span class="fa fa-angle-right" aria-hidden="true"></span></a>
              </div>
            </div>

          </div>

          <div id="fresh-content" class="fc-content">

            <div class="text-content">
              <h2>  We update our content as often as needed.
              </h2> 

              <img src="<?php echo base_url(); ?>assets/images/4.-Up-to-Date-Content-Guarantee.png" alt="" class="pull-left" />
              <p>Many speculate that online education means repetitive and generic content. Here at wagonaMaths.com, we ensure that all our teaching materials are updated and coincide with the latest national curriculum.</p>
              <p>Our Mathematics Diagnostic Tool , for one, contains a broad database of practice and mock tests to keep students learning, focused and stimulated. As current and useful as it can be, our Mathematics Diagnostic Tool is equipped with proven Mathematics software that helps students understand complex problems and soon, become masters at Mathematics.</p>
              <div class="btn-group feat_buttons">
               <a class="btn button-arrow" alt="leftarrow"><span class="fa fa-angle-left" aria-hidden="true"></span></a>
               <a class="btn btn-default btn-lg button-box yellow2-bg">Free Trial</a>
               <a class="btn btn-default btn-lg button-box yellow2-bg">FAQ's</a>
               <a class="btn button-arrow" alt="rightarrow"><span class="fa fa-angle-right" aria-hidden="true"></span></a>
              </div>
            </div>

          </div>

          <div id="bestspoke-study-program" class="fc-content">

            <div class="text-content">
              <h2>We personalise each student’s study program.
              </h2> 

              <img src="<?php echo base_url(); ?>assets/images/5.Personalized-Study-Program.png" alt="" class="pull-left" />
              <p>wagonaMaths.com personalises the learning of every student, because every student is different. Every student is individually taken care of so that they can reach their maximum potential.</p>
              <p>Included in our <b class="black-col">B</b><b class="red-col">R</b><b class="yellow-col">A</b><b class="green-col">G</b> system are personalized study programs. These are sessions created by our experts to target each student’s level of learning. From here, lessons and tools are generated to accommodate his/her ability.</p>
              <p>Various teaching styles will be used to take students step by step and ultimately, lead them to fully understand a subject. Their progress will be monitored daily, weekly and monthly so no student gets left behind.</p>
              <div class="btn-group feat_buttons">
               <a class="btn button-arrow" alt="leftarrow"><span class="fa fa-angle-left" aria-hidden="true"></span></a>
               <a class="btn btn-default btn-lg button-box yellow2-bg">Free Trial</a>
               <a class="btn btn-default btn-lg button-box yellow2-bg">FAQ's</a>
               <a class="btn button-arrow" alt="rightarrow"><span class="fa fa-angle-right" aria-hidden="true"></span></a>
              </div>
            </div>

          </div>

          <div id="lively-engaging-learning" class="fc-content">

            <div class="text-content">
              <h2> We keep learning interactive through visual lessons.
              </h2> 

              <img src="<?php echo base_url(); ?>assets/images/7.Live-lessons.png" alt="" class="pull-left" />
              <p>Fact: The brain processes visual information 60,000 times faster than it does with text. This means it’s essential to have visual aids throughout a child’s learning process.</p>
              <p>wagonaMaths.com understands such necessity, which is why we have video lessons that cover different topics for various academic subjects. It could be a tutorial, a lecture or even an assessment video. All these account for a more stimulating learning.</p>
              <p>What’s more is that students receive a playlist of video lessons specially made for their individual needs. In no time, they’ll get a hang of their lessons and ace their tests.</p>
              <div class="btn-group feat_buttons">
               <a class="btn button-arrow" alt="leftarrow"><span class="fa fa-angle-left" aria-hidden="true"></span></a>
               <a class="btn btn-default btn-lg button-box yellow2-bg">Free Trial</a>
               <a class="btn btn-default btn-lg button-box yellow2-bg">FAQ's</a>
               <a class="btn button-arrow" alt="rightarrow"><span class="fa fa-angle-right" aria-hidden="true"></span></a>
              </div>
            </div>

          </div>

          <div id="virtual-classroom" class="fc-content">

            <div class="text-content">
              <h2>We offer webinars that represent virtual classrooms.
              </h2> 

              <img src="<?php echo base_url(); ?>assets/images/virtual-classroom.png" alt="" class="pull-left" />
              <p>Surely, there will be times students would want to feel like they are part of a class. This goes for teachers too. Not to worry as wagonaMaths.com offers webinars (web seminars) that stand as virtual classrooms. Pupils can participate in live lessons with others just like in a regular classroom setting— only better.</p>
              <p>In webinars, teachers can easily give out virtual and real time lessons as well as learning materials that will help children understand their subjects more. A comfortable, convenient and effective setting, it will help students to grow with their fellow online classmates.</p>
              <div class="btn-group feat_buttons">
               <a class="btn button-arrow" alt="leftarrow"><span class="fa fa-angle-left" aria-hidden="true"></span></a>
               <a class="btn btn-default btn-lg button-box yellow2-bg">Free Trial</a>
               <a class="btn btn-default btn-lg button-box yellow2-bg">FAQ's</a>
               <a class="btn button-arrow" alt="rightarrow"><span class="fa fa-angle-right" aria-hidden="true"></span></a>
              </div>
            </div>

          </div>

          <div id="interactive-fun-learning" class="fc-content">

            <div class="text-content">
              <h2>We make sure students have fun while learning.
              </h2> 

              <img src="<?php echo base_url(); ?>assets/images/8.-Stimulating-Games-and-quizzes.png" alt="" class="pull-left" />
              <p>School shouldn’t be equated to boredom or distaste. In fact, here at wagonaMaths.com, we welcome a healthy dose of horsing around— and learning while the kids are at it.</p>
              <p>Whether it’s a pop culture quiz, crosswords or other recreational activities, we make sure educational games are part of a student’s growing process. These Flash-based activities make Mathematics and Science (among other subjects) enjoyable to learn.</p>
              <p>We’re big on animations, engaging music and other cool features kids will find irresistible. It’s as if they’ve come to the place where learning and playing work hand in hand.</p>
              <div class="btn-group feat_buttons">
               <a class="btn button-arrow" alt="leftarrow"><span class="fa fa-angle-left" aria-hidden="true"></span></a>
               <a class="btn btn-default btn-lg button-box yellow2-bg">Free Trial</a>
               <a class="btn btn-default btn-lg button-box yellow2-bg">FAQ's</a>
               <a class="btn button-arrow" alt="rightarrow"><span class="fa fa-angle-right" aria-hidden="true"></span></a>
              </div>
            </div>

          </div>

          <div id="national-curriculum" class="fc-content">

            <div class="text-content">
              <h2>We comply with the National Curriculum.
              </h2> 
              <img src="<?php echo base_url(); ?>assets/images/9.-national-curriculum.png" alt="" class="pull-left" />
              <p>wagonaMaths.com students are given the highest quality of education any parent/teacher can ever wish for.</p>
              <p>Unlike other online Math Programs, this is the first ever that has been designed specifically to meet both ZIMSEC and GSCE standards. Made by Zimababweans for Zimbabweans!</p>
              <p>Each of our activity is able to target every student’s learning ability. No task is wasted as each yields productive results.</p>
              <p>Our specialized Mathematics Diagnostic Tool (MDT) is also in compliance with the National Curriculum, which is why we do not fall short of meeting standards.</p>
              <div class="btn-group feat_buttons">
               <a class="btn button-arrow" alt="leftarrow"><span class="fa fa-angle-left" aria-hidden="true"></span></a>
               <a class="btn btn-default btn-lg button-box yellow2-bg">Free Trial</a>
               <a class="btn btn-default btn-lg button-box yellow2-bg">FAQ's</a>
               <a class="btn button-arrow" alt="rightarrow"><span class="fa fa-angle-right" aria-hidden="true"></span></a>
              </div>
            </div>

          </div>

          <div id="online-child-safety" class="fc-content">

            <div class="text-content">
              <h2>We take Online Child Protection seriously.
              </h2> 

              <img src="<?php echo base_url(); ?>assets/images/10.-Online-Child-protection-Policy.png" alt="" class="pull-left" />
              <p>There are a lot of threats surrounding the cyber world but with wagonaMaths.com you don’t have to worry. We assure you that students are safe thanks to our Child Protection Policy. This policy is based on the UN Convention on the Rights of the Child, and in line with international best practice.</p>
              <p>First, we provide them with appropriate safety and protection measures when surfing wagonaMaths.com. Second, our technical support is on top of confirming parent requests.
              </p>
              <p>When it comes to protecting the welfare of our children, we mean business.</p>
              <div class="btn-group feat_buttons">
               <a class="btn button-arrow" alt="leftarrow"><span class="fa fa-angle-left" aria-hidden="true"></span></a>
               <a class="btn btn-default btn-lg button-box yellow2-bg">Free Trial</a>
               <a class="btn btn-default btn-lg button-box yellow2-bg">FAQ's</a>
               <a class="btn button-arrow" alt="rightarrow"><span class="fa fa-angle-right" aria-hidden="true"></span></a>
              </div>
            </div>

          </div>

          <div id="detailed-student-report" class="fc-content">

            <div class="text-content">
              <h2>We keep you updated on your child’s progress.
              </h2> 

              <img src="<?php echo base_url(); ?>assets/images/11.Detailed-Reporting-System.png" alt="" class="pull-right" />
              <p>Parents need to know how their children are holding up in school since it can be a worrisome feeling not knowing if your kid is making progress. Not to worry because wagonaMaths.com provides a detailed reporting system to parents, which is a full report of your child’s performance.</p>
              <p>We ensure that these reports contain assessments, feedbacks and other reports detailing the pupil’s achievement by grade, subject and student subgroup. If you’re worried about what others might think of your child, don’t fret. We value your privacy and care for the confidentiality parents and their kids deserve.</p>
              <div class="btn-group feat_buttons">
               <a class="btn button-arrow" alt="leftarrow"><span class="fa fa-angle-left" aria-hidden="true"></span></a>
               <a class="btn btn-default btn-lg button-box yellow2-bg">Free Trial</a>
               <a class="btn btn-default btn-lg button-box yellow2-bg">FAQ's</a>
               <a class="btn button-arrow" alt="rightarrow"><span class="fa fa-angle-right" aria-hidden="true"></span></a>
              </div>
            </div>

          </div>

          <div id="parental-controls" class="fc-content">

            <div class="text-content">
              <h2>We let parents take the wheel.
              </h2> 

              <img src="<?php echo base_url(); ?>assets/images/12.-Parental-control.png" alt="" class="pull-right" />
              <p>Parental involvement is an impetus to any child’s learning capabilities. wagonaMaths.com knows Mums and Dads want to be more involved in shaping their children’s future. That’s why we provide parental control.</p>
              <p><strong>What is parental control?</strong></p>
              <ul class="inside_list">
                <li>Set the pace for your children’s learning so they won’t feel too much pressure.</li>
                <li>Modify your children’s Internet use with time limits.</li>
                <li>Manage your children’s activities from anytime anywhere with any Internet-ready device.</li>
                <li>Make your own recommendations to wagonaMaths.com.</li>
              </ul>
              <p>Here at wagonaMaths.com, we firmly believe you know what’s best for your child. Allow us to be your extra hand in maximizing your kid’s potential.</p>
              <div class="btn-group feat_buttons">
               <a class="btn button-arrow" alt="leftarrow"><span class="fa fa-angle-left" aria-hidden="true"></span></a>
               <a class="btn btn-default btn-lg button-box yellow2-bg">Free Trial</a>
               <a class="btn btn-default btn-lg button-box yellow2-bg">FAQ's</a>
               <a class="btn button-arrow" alt="rightarrow"><span class="fa fa-angle-right" aria-hidden="true"></span></a>
              </div>
            </div>

          </div>

          <div id="percent-33" class="fc-content">

            <div class="text-content">
              <h2>We offer a cheaper yet smarter way to better your child’s future!
              </h2> 

              <img src="<?php echo base_url(); ?>assets/images/13.-At-just-33p-a-day.png" alt="" class="pull-right" />
              <p>Expensive education doesn’t always mean it’s the best. In fact, wagonaMaths.com revolutionizes the way online education works. Whether you’re a parent who’s always trying to make ends meet or one who wants a change in their child’s learning life, then wagonaMaths.com is for you.</p>
              <p>No longer will you worry about overpriced tuition fees and materials that you constantly need to buy but would only use once. Opting for wagonaMaths.com means opting for a more affordable choice that doesn’t compromise vital education.</p>
              <div class="btn-group feat_buttons">
               <a class="btn button-arrow" alt="leftarrow"><span class="fa fa-angle-left" aria-hidden="true"></span></a>
               <a class="btn btn-default btn-lg button-box yellow2-bg">Free Trial</a>
               <a class="btn btn-default btn-lg button-box yellow2-bg">FAQ's</a>
               <a class="btn button-arrow" alt="rightarrow"><span class="fa fa-angle-right" aria-hidden="true"></span></a>
              </div>
            </div>

          </div>

          <div id="parent-support" class="fc-content">

            <div class="text-content">
              <h2>We have a support forum for parents who want to get more involved.
              </h2> 

              <img src="<?php echo base_url(); ?>assets/images/14.-Parental-Support.png" alt="" class="pull-right" />
              <p>There are times when parents can’t help but feel unreliable when it comes to aiding their children in learning say, how to solve certain mathematical problems. Now with wagonaMaths.com, you can easily get involved in the learning process by joining in our support forum moderated by experts.</p>
              <p>Whether you need aid on certain lessons or simply would like to relay their experience in wagonaMaths.com so far, trust that our support forum will be there to guide you through. In turn, you will productively lead your children to higher learning.</p>
              <div class="btn-group feat_buttons">
               <a class="btn button-arrow" alt="leftarrow"><span class="fa fa-angle-left" aria-hidden="true"></span></a>
               <a class="btn btn-default btn-lg button-box yellow2-bg">Free Trial</a>
               <a class="btn btn-default btn-lg button-box yellow2-bg">FAQ's</a>
               <a class="btn button-arrow" alt="rightarrow"><span class="fa fa-angle-right" aria-hidden="true"></span></a>
              </div>
            </div>

          </div>

          <div id="open-247" class="fc-content">

            <div class="text-content">
              <h2>We are open to serve you 24/7— even during the Holidays!
              </h2> 

              <img src="<?php echo base_url(); ?>assets/images/15.-Available-24-7-365.png" alt="" class="pull-right" />
              <p>Since we harness the power of the Internet, we can easily keep a log on those users who register or log in any day of the year, including Christmas. As an e-platform, our learning tools and materials are available to our members anytime anywhere.</p>
              <p>So if you want your children to continuously learn, simply check those lessons and programs you want to access, purchase them and check out. You can then download these materials for instant use.</p>
              <p>Trust that there are always new programs that wagonaMaths.com adds on a regular basis.</p>
              <div class="btn-group feat_buttons">
               <a class="btn button-arrow" alt="leftarrow"><span class="fa fa-angle-left" aria-hidden="true"></span></a>
               <a class="btn btn-default btn-lg button-box yellow2-bg">Free Trial</a>
               <a class="btn btn-default btn-lg button-box yellow2-bg">FAQ's</a>
               <a class="btn button-arrow" alt="rightarrow"><span class="fa fa-angle-right" aria-hidden="true"></span></a>
              </div>
            </div>

          </div>

          <div id="free-monthly-ezine" class="fc-content">

            <div class="text-content">
              <h2>We send FREE e-zines to keep students, teachers and parents up to date.
              </h2> 

              <img src="<?php echo base_url(); ?>assets/images/16.-Free-E-zine.png" alt="" class="pull-right" />
              <p>It’s important that students, parents and teachers know what the latest updates and news are within the wagonaMaths.com community. This way, an engaging environment is emanated and maintained.</p>
              <p>If you want to keep track of the latest school curriculum, resources, updates and even learning tips, you can simply sign up for a free e-zine and receive them regularly.  The subscription steps are fast and easy since we only need basic contact information to send the e-zine to you.</p>
              <p>You can view these e-zines online by clicking on the images below.</p>
              <div class="btn-group feat_buttons">
               <a class="btn button-arrow" alt="leftarrow"><span class="fa fa-angle-left" aria-hidden="true"></span></a>
               <a class="btn btn-default btn-lg button-box yellow2-bg">Free Trial</a>
               <a class="btn btn-default btn-lg button-box yellow2-bg">FAQ's</a>
               <a class="btn button-arrow" alt="rightarrow"><span class="fa fa-angle-right" aria-hidden="true"></span></a>
              </div>
            </div>

          </div>

          <div id="free-diagnostic-check" class="fc-content">

            <div class="text-content">
              <h2>We offer a FREE diagnostic test of practice questions for students.
              </h2> 

              <img src="<?php echo base_url(); ?>assets/images/17.-Free-Diagnostic-test.png" alt="" class="pull-right" />
              <p>It’s easy to teach students what they should learn. The real question here is are they actually learning?</p>
              <p>Mathematics is one of the most challenging subjects out there. With wagonaMaths.com, teachers can give out mathematical diagnostic tests to gauge their students’ individual strengths and weaknesses. From here, teachers can create tailor-fit lessons that will hit the mark.</p>
              <p>Get a feel of how these diagnostics work by trying out our FREE test comprised of practice Math questions in different levels and content strands. Simply follow the link here: <insert link here>.</p>
              <div class="btn-group feat_buttons">
               <a class="btn button-arrow" alt="leftarrow"><span class="fa fa-angle-left" aria-hidden="true"></span></a>
               <a class="btn btn-default btn-lg button-box yellow2-bg">Free Trial</a>
               <a class="btn btn-default btn-lg button-box yellow2-bg">FAQ's</a>
               <a class="btn button-arrow" alt="rightarrow"><span class="fa fa-angle-right" aria-hidden="true"></span></a>
              </div>
            </div>

          </div>

          <div id="mock-tests" class="fc-content">

            <div class="text-content">
              <h2>Expert teachers set loads of mock tests to get you 100% ready for the real examinations.
              </h2> 

              <img src="<?php echo base_url(); ?>assets/images/18.-Loads-of-Mock-tests-to-prepare-you-for-the-real-test..png" alt="" class="pull-right" />
              <p>As form of preparation, wagonaMaths.com has loads of mock tests students can get their hands on just before they take the real exams. Apart from that, here are the other benefits mock tests can provide students with:</p>
              <ul class="inside_list">
                <li>Encourages good study habits and time management</li>
                <li>Helps develops skills and techniques for taking exams</li>
                <li>Address strengths and weaknesses</li>
              </ul>
              <p>We also allow students to take part in webinars, competitions and other forums involving local schools with the constant aid of our experts. Such healthy competition always pushes our children to be better.</p>
              <div class="btn-group feat_buttons">
               <a class="btn button-arrow" alt="leftarrow"><span class="fa fa-angle-left" aria-hidden="true"></span></a>
               <a class="btn btn-default btn-lg button-box yellow2-bg">Free Trial</a>
               <a class="btn btn-default btn-lg button-box yellow2-bg">FAQ's</a>
               <a class="btn button-arrow" alt="rightarrow"><span class="fa fa-angle-right" aria-hidden="true"></span></a>
              </div>
            </div>

          </div>

          <div id="target-setting" class="fc-content">

            <div class="text-content">
              <h2>We give students, teachers and parents to check scores and goals online.
              </h2> 

              <img src="<?php echo base_url(); ?>assets/images/target-setting.png" alt="" class="pull-right" />
              <p>wagonaMaths.com benefits students, parents and teachers in different ways. Teachers and parents can easily set targeted goals or assign tasks for students to fulfill. Along with it are our materials (such as personalized study programs) to make sure these goals are met.</p>
              <p>Students, on the other hand, get to see their progress online. In the form of scores, children can monitor whether or not they’re doing the best they can and growing as wagonaMaths.com pupils.</p>
              <p>Transparency is what elevates relationships, which is why we make sure you know everything that’s going on.</p>
              <div class="btn-group feat_buttons">
               <a class="btn button-arrow" alt="leftarrow"><span class="fa fa-angle-left" aria-hidden="true"></span></a>
               <a class="btn btn-default btn-lg button-box yellow2-bg">Free Trial</a>
               <a class="btn btn-default btn-lg button-box yellow2-bg">FAQ's</a>
               <a class="btn button-arrow" alt="rightarrow"><span class="fa fa-angle-right" aria-hidden="true"></span></a>
              </div>
            </div>

          </div>

          <div id="big-community" class="fc-content">

            <div class="text-content">
              <h2>We are one big family you can call home.
              </h2> 

              <img src="<?php echo base_url(); ?>assets/images/19.-big-school-community.png" alt="" class="pull-right" />
              <p>Perhaps one of the best reasons why you should choose wagonaMaths.com is that we are a united community whose goal is to provide the most effective and enriching educational experience to students, teachers and parents alike.</p>
              <p>We don’t only see you as part of our team. We consider you as our family. As such, we will take care of you and give a hospitable environment for quality learning.</p>
              <p>If you are now ready to give wagonaMaths.com a chance to make a change, you can learn more about us by subscribing to a full feature  trial version.</p>
              <div class="btn-group feat_buttons">
               <a class="btn button-arrow" alt="leftarrow"><span class="fa fa-angle-left" aria-hidden="true"></span></a>
               <a class="btn btn-default btn-lg button-box yellow2-bg">Free Trial</a>
               <a class="btn btn-default btn-lg button-box yellow2-bg">FAQ's</a>
              </div>
            </div>

          </div>
        </div>
      </div>

		  <div class="col-md-3">

		  	<ul id="features_list_2" class="features-list menu">
              <li data-toggle="#detailed-student-report" class="detailed-student-report">
              	<a href="javascript:void(0);">
              	  <span class="m-icon-sm features-icon fa fa-newspaper-o"></span>Detailed student reports
              	</a>
              </li>
              <li data-toggle="#parental-controls" class="parental-controls">
              	<a href="javascript:void(0);">
                  <span class="m-icon-sm features-icon fa fa-user"></span>Parental control
                </a>
              </li>
              <li data-toggle="#percent-33" class="percent-33">
              	<a href="javascript:void(0);">
              	  <span class="m-icon-sm features-icon fa" style="font-size:18px; font-weight:900;">$</span>
              	    Just 33p a day
              	</a>
              </li>
              <li data-toggle="#parent-support" class="parent-support">
              	<a href="javascript:void(0);">
                  <span class="m-icon-sm features-icon fa fa-user-md"></span>Parent support
                </a>
              </li>
              <li data-toggle="#open-247" class="open-247">
              	<a href="javascript:void(0);">
              	  <span class="m-icon-sm features-icon fa fa-calendar-o"></span>Open 24/7.365 days
                </a>
              </li>
              <li data-toggle="#free-monthly-ezine" class="free-monthly-ezine">
              	<a href="javascript:void(0);">
              	  <span class="m-icon-sm features-icon fa fa-list-ul"></span>Free monthly e-Zine
              	</a>
              </li>
              <li data-toggle="#free-diagnostic-check" class="free-diagnostic-check">
              	<a href="javascript:void(0);">
                  <span class="m-icon-sm features-icon fa fa-check-square-o"></span>Free diagnostic check!
                </a>
              </li>
              <li data-toggle="#mock-tests" class="mock-tests">
              	<a href="javascript:void(0);">
              	  <span class="m-icon-sm features-icon fa fa-pagelines"></span>Mock tests
              	</a>
              </li>
              <li data-toggle="#target-setting" class="target-setting">
              	<a href="javascript:void(0);">
                 <span class="m-icon-sm features-icon fa fa-bullseye"></span>Target setting
                </a>
              </li>
              <li data-toggle="#big-community" class="big-community">
              	<a href="javascript:void(0);">
              	  <span class="m-icon-sm features-icon fa fa-users"></span>Big community
              	</a>
              </li>
              
            </ul>
				
	      </div>
		
		</div>
	</div>
</div>

<?php $this->load->view('footer.php'); ?>

<script type="text/javascript">
  $(document).ready(function(){
    $('.features-list li').click(function(){
      var toggle = $(this).attr('data-toggle');
      $('.fc-content').removeClass('animated animated-show flipInX');
      $(toggle).addClass('animated animated-show flipInX');
      $('.features-list li').removeClass('active');
      $(this).addClass('active');
    });

    $('.feat_buttons > a[alt="leftarrow"]').click(function(){

        var idget = "." + $(this).parent().parent().parent().attr('id');
        var idmove ="#" + $(this).parent().parent().parent().attr('id');
        $(idget).removeClass("active");
        $(idget).prev().addClass("active");
        $(idmove).removeClass('animated animated-show flipInX');
        $(idmove).prev().addClass('animated animated-show flipInX');
        if($(idget).is(":first-child")){
          //$("ul.features_list li:last-child").addClass("active");
          $("ul#features_list_1 li:last-child").addClass("active");
        }
    });

    $('.feat_buttons > a[alt="rightarrow"]').click(function(){

        var idget = "." + $(this).parent().parent().parent().attr('id');
        var idmove ="#" + $(this).parent().parent().parent().attr('id');
        $(idget).removeClass("active");
        $(idget).next().addClass("active");
        $(idmove).removeClass('animated animated-show flipInX');
        $(idmove).next().addClass('animated animated-show flipInX');

        if($(idget).is(":last-child")){
          //$("ul.features_list li:last-child").addClass("active");
          $("ul#features_list_2 li:first-child").addClass("active");
        }
        
    });

  });
</script>

</body>
</html>