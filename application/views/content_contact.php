<?php $this->load->view('header.php'); ?>

<div id="content" class="content-row">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="title">
					<h2>We are happy to <strong class="green-col">hear from you</strong></h2>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="contact-col-pad contact-col-border">
					<div class="mb-50">
						<h3 class="ubuntu-700 green-col mb-20"><span class="fa fa-group m-icon-sm red-col"></span>Social Channels</h3>
						<ul class="menu contact-socmed socialIconers">
							<li><a href="http://facebook.com"><span class="fa fa-facebook-square fb animated fx-norm rubber-it"></span></a></li>
							<li><a href="http://twitter.com"><span class="fa fa-twitter-square twitter animated fx-norm rubber-it"></span></a></li>
							<li><a href="http://plus.google.com"><span class="fa fa-google-plus-square googleplus animated fx-norm rubber-it"></span></a></li>
							<li><a href="http://youtube.com"><span class="fa fa-youtube-square youtube animated fx-norm rubber-it"></span></a></li>
							<li><a href="http://pinterest.com"><span class="fa fa-pinterest-square pinteres animated fx-norm rubber-it"></span></a></li>
						</ul>
					</div>
					<div class="">
						<h3 class="ubuntu-700 green-col mb-20"><span class="fa fa-map-marker m-icon-sm red-col"></span>General</h3>
						<div class="center-txt">
							<h3 class="ubuntu_400">
								<strong>Wagona Maths</strong><br>
								34 New House<br/>
								67-68 Hatton Garden<br/>
								LONDON, EC1N 8JY<br/>
								United Kingdom<br/>
							</h3>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="contact-col-pad">
					<h3 class="ubuntu-700 green-col mb-20"><span class="fa fa-envelope-o m-icon-sm red-col"></span>Mail</h3>
					<form class="s-form">
						<div class="form-group">
							<input type="text" class="form-control" id="cname" placeholder="Name" />
						</div>
						<div class="form-group">
							<input type="email" class="form-control" id="cemail" placeholder="Email" />
						</div>
						<div class="form-group">
							<input type="text" class="form-control" id="cphone" placeholder="Phone" />
						</div>
						<div class="form-group">
							<input type="text" class="form-control" id="csubject" placeholder="Subject" />
						</div>
						<div class="form-group">
							<textarea class="form-control" id="cmessage" placeholder="Your Message"></textarea>
						</div>
						<p id="result">&nbsp;</p>
						<div class="form-group">
					  	<button type="button" class="btn btn-dblue btn-block btn-lg" onClick="contactMe()">Send</button>
					  </div>
					</form>
					<h3 class="text-center ubuntu-700 green-col hide contact-success-message">Your message has been successfully submitted and will respond to you shortly.</h3>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	function contactMe(){
		var cname = $('#cname').val();
		var cemail  = $('#cemail').val();
		var cphone  = $('#cphone').val();
		var csubject = $('#csubject').val();
		var cmessage  = $('#cmessage').val();
		var form = $('#cname').closest('form');
		var success_message = $('.contact-success-message');

		if(cname.length > 0 && cemail.length > 0 && cphone.length > 0 && csubject.length > 0 && cmessage.length > 0){

			form.find('button').prop('disabled', 'disabled');
			$("#result").html('<i>Sending message, please wait...</i>');

			$.post( url + 'site/send_my_message',
				{
					name: cname,
					email: cemail,
					phone: cphone,
					subject: csubject,
					message: cmessage
				},
				function(data){
					if(data.success){

						form.hide();
						success_message.removeClass('hide').addClass('animate bounceIn');

					}else{
						alert(data.msg);
						return;
					}
			},"json");
		}else{
			alert("Fill all the fields.");
		}
	}
</script>

<?php $this->load->view('footer.php'); ?>