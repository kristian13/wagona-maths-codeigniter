<?php $this->load->view('header.php'); ?>
<div id="content" class="content-row">
	<div class="container">
		<div class="row">
			<div class="col-md-5 col-md-offset-1">
				<span class="divider-badge green-col hidden-xs hidden-sm">or</span>
				<div class="contact-col-pad contact-col-border">
					<h3 class="ubuntu-700 green-col mb-20 center-txt">Login Here</h3>
					
					<form class="s-form mob-mb-50">
						<div class="form-group">
							<input type="text" class="form-control" name="email" id="email" placeholder="Email/Username" />
						</div>
						<div class="form-group">
							<input type="password" class="form-control" name="password" id="password" placeholder="Password" />
							<label class="voidText">
					      <input type="checkbox"> <a href="#" class="green-col">Remember me</a>
					    </label>
							<div class="right-txt pt-2">
								<small><a href="#" class="green-col">Forgot my details</a></small>
							</div>
						</div>
					  <div class="form-group">
					  	<button type="button" class="btn btn-dgreen btn-lg" onClick="logmein()">LOGIN</button>
					  	<p class="midText">or</p>
              			<a href="<?php echo base_url(); ?>site/pricing" class="btn btn-dblue btn-lg">SIGN UP</a>
					  </div>
					</form>
					
				</div>
			</div>
			<div class="col-md-5">
				<div class="">
					<h3 class="ubuntu-700 green-col mb-20 center-txt">Login With</h3>
					<ul class="menu contact-socmed login-socmed socialIconers">
						<li><a href="http://facebook.com"><span class="fa fa-facebook-square fb rubber-it"></span></a></li>
						<li><a href="http://twitter.com"><span class="fa fa-twitter-square twitter rubber-it"></span></a></li>
						<li><a href="http://plus.google.com"><span class="fa fa-google-plus-square googleplus rubber-it"></span></a></li>
						<li><a href="http://youtube.com"><span class="fa fa-youtube-square youtube rubber-it"></span></a></li>
						<li><a href="http://pinterest.com"><span class="fa fa-pinterest-square pinteres rubber-it"></span></a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	function logmein(){
		var email = $('#email').val();
		var pass  = $('#password').val();	
		
		if(email.length > 0 && pass.length > 0){
			$.post( url + 'site/loginprocess',
				{ useremail: email, password: pass },
				function(data){
					if(data.success){		
						
						$("#result").html('<span class="label label-success" style="padding: 5px; font-weight: bold; font-size: 13px;">Success: please wait, page redirection...</span>');						
						window.location.href = data.link;
					}else{				
						alert(data.msg);						
						return;
					}			    
			},"json");
		}else{
			alert("Email and Password must filled.");
		}
	}
</script>

<?php $this->load->view('footer.php'); ?>