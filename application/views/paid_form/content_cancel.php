<?php $this->load->view('header.php'); ?>

<div id="content" class="content-row">
	<div class="thank-you">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h1 class="ubuntu-300">Subscription <strong>Cancelled</strong></h1>
					<h4 class="ubuntu_400">We regret to inform that your submitted account data was cancelled, please let us know if you're having problem with registering your account.</h4>
				</div>
			</div>
		</div>
	</div>
</div>

<?php $this->load->view('footer.php'); ?>