<?php $this->session->sess_destroy(); $this->load->view('header.php'); ?>

<div id="content" class="content-row">
	<div class="thank-you">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h1 class="ubuntu-300">Thank <strong>You</strong></h1>
					<h4 class="ubuntu_400">
					Thank you for registering with passUKtest. Your account has been created and a verification email has been sent to your registered email address.
					</h4>
				</div>
			</div>
		</div>
	</div>
</div>

<?php $this->load->view('footer.php'); ?>