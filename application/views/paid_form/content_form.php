<?php $this->load->view('header.php'); ?>

<div id="content" class="content-row">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="title">
					<h2>Create your <span class="blue-col">account</span></h2>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6 col-md-offset-3">
				<form class="form-horizontal s-form" id="regform" action="<?php echo base_url(); ?>site/paidregistration" method="post">
					<input type="hidden" name="cmd" value="_xclick" /> 
				    <input type="hidden" name="no_note" value="1" />
				    <input type="hidden" name="lc" value="UK" />
				    <input type="hidden" name="currency_code" value="GBP" />
				    <input type="hidden" name="bn" value="PP-BuyNowBF:btn_buynow_LG.gif:NonHostedGuest" />
				    
					<div class="form-group">
						<span class="req-field red-col">*</span>
						<div class="col-md-6">
							<input type="text" name="fname" id="fname" class="form-control" placeholder="First Name" />
						</div>
						<div class="col-md-6">
							<input type="text" name="sname" id="sname" class="form-control" placeholder="Surname" />
						</div>
					</div>
					<div class="form-group">
						<span class="req-field red-col">*</span>
						<div class="col-md-12">
							<input type="email" name="email" id="email" class="form-control" placeholder="Email">
						</div>
					</div>
					<div class="form-group">
						<span class="req-field red-col">*</span>
						<div class="col-md-12">
							<input type="password" name="pass" id="pass" class="form-control" placeholder="Password">
						</div>
					</div>
					<div class="form-group">
						<span class="req-field red-col">*</span>
						<div class="col-md-12">
							<input type="password" name="conpass" id="conpass" class="form-control" placeholder="Confirm Password">
						</div>
					</div>
					<div class="form-group">
						<span class="req-field red-col">*</span>
						<div class="col-md-12">
							<input type="text" name="nationality" id="nationality" class="form-control" placeholder="Nationality">
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-12">
							<label>Have you already booked your test?</label>
							<select class="form-control" name="is_test_booked">
								<option value="">(Optional)</option>
								<option value="">Yes</option>
								<option value="">No</option>
							</select>
						</div>
					</div>
					<div class="form-group mb-30">
						<div class="col-md-12">
							<label>Date of your test</label>
						</div>
						<div class="col-md-4">
							<select class="form-control" name="month_booked">
								<option value="">January</option>
								<option value="">February</option>
								<option value="">March</option>
								<option value="">April</option>
								<option value="">May</option>
								<option value="">June</option>
								<option value="">July</option>
								<option value="">August</option>
								<option value="">September</option>
								<option value="">October</option>
								<option value="">November</option>
								<option value="">December</option>
							</select>
						</div>
						<div class="col-md-4">
							<select class="form-control" name="day_booked">
								<option value="">01</option>
								<option value="">02</option>
								<option value="">03</option>
								<option value="">04</option>
								<option value="">05</option>
								<option value="">06</option>
								<option value="">07</option>
								<option value="">08</option>
								<option value="">09</option>
								<option value="">10</option>
								<option value="">11</option>
								<option value="">12</option>
								<option value="">13</option>
								<option value="">14</option>
								<option value="">15</option>
								<option value="">16</option>
								<option value="">17</option>
								<option value="">18</option>
								<option value="">19</option>
								<option value="">20</option>
								<option value="">21</option>
								<option value="">22</option>
								<option value="">23</option>
								<option value="">24</option>
								<option value="">25</option>
								<option value="">26</option>
								<option value="">27</option>
								<option value="">28</option>
								<option value="">29</option>
								<option value="">30</option>
								<option value="">37</option>
							</select>
						</div>
						<div class="col-md-4">
							<select class="form-control" name="year_booked">
								<option value="">2014</option>
								<option value="">2015</option>
								<option value="">2016</option>
								<option value="">2017</option>
								<option value="">2018</option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-12" id="result">
							<button type="button" onClick="submitForm()" class="btn btn-block btn-dblue btn-lg btn-box btn-primary">
							 <img src="<?php echo base_url(); ?>assets/images/cards.png" style="width: 200px;">	
							 &nbsp;&nbsp;&nbsp;
							 SUBMIT & PAY
							</button>
						</div>
					</div>
				</form>
			</div>
		</div>	
	</div>
</div>

<script>
		
	document.getElementById("regform").reset();
	document.getElementById("email").value = "";
	document.getElementById("pass").value = "";
	
	function submitForm(){
		
		var fname 			= $('#fname').val();
		var sname 			= $('#sname').val();
		var email 			= $('#email').val();
		var pass  			= $('#pass').val();
		var conpass 		= $('#conpass').val();
		var nationality 	= $('#nationality').val();
		var is_test_booked 	= $('#is_test_booked').val();
		var month_booked 	= $('#month_booked').val();
		var day_booked 		= $('#day_booked').val();
		var year_booked 	= $('#year_booked').val();				
		
		if(fname.length > 0 && sname.length > 0 && email.length > 0 && pass.length > 0 && conpass.length > 0 && nationality.length > 0){
			
			if(pass != conpass){
				alert("Password did not match, please check password confirmation.");
				return;
			}

			$("#result").html("<p style='text-align: left;'><img src='<?php echo base_url(); ?>assets/images/loader.gif'>&nbsp;&nbsp;&nbsp;&nbsp;<i>Please wait, data processing...</i></p>");
			
			$.post( url + 'site/paidregistration',
				{ datax: $("form#regform").serialize() },
				function(data){
					if(data.success){		
						
						$("#result").html("<p style='text-align: left;'><strong><i>Please wait, page redirecting to PayPal...</i></strong></p>");						
						window.location.href=data.link;
						alert("Registration successfully submitted, please wait for PayPal page redirection.");					
						
					}else{				
						alert(data.msg);
						$("#result").html('<button type="button" onClick="submitForm()" class="btn btn-block btn-dblue btn-lg btn-box btn-primary"><img src="<?php echo base_url(); ?>assets/images/cards.png" style="width: 200px;">&nbsp;&nbsp;&nbsp;SUBMIT & PAY</button>');
						return;
					}			    
			},"json");	
			
		}else{
			alert("Please fill all the required fields.");	
			return;
		}
	}
</script>

<?php $this->load->view('footer.php'); ?>