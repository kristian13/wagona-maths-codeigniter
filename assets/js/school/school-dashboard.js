var school_id = null,
  class_id = null,
  teacher_id = null,
  student_id = null,
  datatable_school_list,
  datatable_class_list,
  datatable_teacher_list,
  datatable_student_list,
  email_regex = /[a-z0-9!#$%&'*+\=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+\=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/,
  url_regex = /([-a-zA-Z0-9^\p{L}\p{C}\u00a1-\uffff@:%_\+.~#?&//=]{2,256}){1}(\.[a-z]{2,4}){1}(\:[0-9]*)?(\/[-a-zA-Z0-9\u00a1-\uffff\(\)@:%,_\+.~#?&//=]*)?([-a-zA-Z0-9\(\)@:%,_\+.~#?&//=]*)?/,
  num_regex = /^[1-9]\d*$/;

$(document).ready(function() {
  // Set Modal events
  $('#school_list_modal').on('show.bs.modal', function() {
    // Reset Modal and inputs
    reset_form($(this), true);
    form_enable($(this), true);

    // Activate First Tab
    $(this).find('.nav-tabs a:first').tab('show');

    // Get School List
    school_list_get();
  });
  $('#class_list_modal').on('show.bs.modal', function() {
    // Reset Modal and inputs
    reset_form($(this), true);
    form_enable($(this), true);

    // Activate First Tab
    $(this).find('.nav-tabs a:first').tab('show');

    // Initialize Class Datatable
    $('#list_subject').val('');
    $('#list_subject').trigger('chosen:updated');
    class_list_get();
  });
  $('#teacher_list_modal').on('show.bs.modal', function() {
    // Reset Modal and inputs
    reset_form($(this), true);
    form_enable($(this), true);

    // Activate First Tab
    $(this).find('.nav-tabs a:first').tab('show');

    // Initialize Class Datatable
    teacher_list_get();

    // Update Class List Dropwdown
    update_class_list_dropdown();
  });
  $('#student_list_modal').on('show.bs.modal', function() {
    // Reset Modal and inputs
    reset_form($(this), true);
    form_enable($(this), true);

    // Activate First Tab
    $(this).find('.nav-tabs a:first').tab('show');

    // Initialize Class Datatable
    student_list_get();

    // Update Class List Dropwdown
    update_class_list_dropdown();
  });

  // Set buttons event
  $('#school_list_modal .btn.submit').click(function(event) {
    event.preventDefault();
    school_list_save();
  });
  $('#school_list_modal .btn.cancel').click(function(event) {
    event.preventDefault();
    reset_form($('#school_list_modal'), true);
    form_enable($('#school_list_modal'), true);
  });
  $('#class_list_modal .btn.submit').click(function(event) {
    event.preventDefault();
    class_list_save();
  });
  $('#class_list_modal .btn.cancel').click(function(event) {
    event.preventDefault();
    reset_form($('#class_list_modal'), true);
    form_enable($('#class_list_modal'), true);
  });
  $('#teacher_list_modal .btn.submit').click(function(event) {
    event.preventDefault();
    teacher_list_save();
  });
  $('#teacher_list_modal .btn.cancel').click(function(event) {
    event.preventDefault();
    reset_form($('#teacher_list_modal'), true);
    form_enable($('#teacher_list_modal'), true);
  });
  $('#student_list_modal .btn.submit').click(function(event) {
    event.preventDefault();
    student_list_save();
  });
  $('#student_list_modal .btn.cancel').click(function(event) {
    event.preventDefault();
    reset_form($('#student_list_modal'), true);
    form_enable($('#student_list_modal'), true);
  });

  // Check if tab is disabled
  // Then prevent tab
  $('.nav-tabs li').click(function() {
    if ($(this).hasClass('disabled')) {
      return false;
    }
  });

  // Initialize Select
  $('#list_subject').chosen({
    width: '200px'
  });
  $('#list_subject').change(function() {
    fn_reloadClassList();
  });
  var form_select = [
    '#class_list_modal select[name="subject"]',
    '#teacher_list_modal select[name="classes"]',
    '#student_list_modal select[name="class"]'
  ];
  $(form_select.join(',')).chosen({
    width: '334px'
  });
});

function school_list_save() {
  var modal = $('#school_list_modal'),
    name = modal.find('input[name="name"]'),
    address_1 = modal.find('input[name="address_1"]'),
    address_2 = modal.find('input[name="address_2"]'),
    address_3 = modal.find('input[name="address_3"]'),
    admin_contact = modal.find('input[name="admin_contact"]'),
    telephone_num = modal.find('input[name="telephone_num"]'),
    cellphone_num = modal.find('input[name="cellphone_num"]'),
    email_address = modal.find('input[name="email_address"]'),
    website_url = modal.find('input[name="website_url"]'),
    num_students = modal.find('input[name="num_students"]'),
    submit = modal.find('.submit'),
    valid = true;

  // Reset Modal first
  reset_form(modal);

  // Validate

  // Name
  if (name.val().trim() === '') {
    name.addClass('error');
    valid = false;
  }

  // Address 1
  if (address_1.val().trim() === '') {
    address_1.addClass('error');
    valid = false;
  }

  // Admin Contact
  if (admin_contact.val().trim() === '') {
    admin_contact.addClass('error');
    valid = false;
  }

  // Telephone number
  if (telephone_num.val().trim() === '') {
    telephone_num.addClass('error');
    valid = false;
  }

  // Cellphone number
  if (cellphone_num.val().trim() === '') {
    cellphone_num.addClass('error');
    valid = false;
  }

  // Email address
  if (email_address.val().trim() === '' || !email_regex.test(email_address.val().trim())) {
    email_address.addClass('error');
    valid = false;
  }

  // Website url
  if (website_url.val().trim() === '' || !url_regex.test(website_url.val().trim())) {
    website_url.addClass('error');
    valid = false;
  }

  // Number of Students
  if (num_students.val().trim() === '' || !num_regex.test(num_students.val().trim())) {
    num_students.addClass('error');
    valid = false;
  }

  // Show Error Message
  if (!valid) {
    modal.find('.error-message').removeClass('invisible');
    return;
  }

  // Submit Form
  var data = {
    name: name.val().trim(),
    address_1: address_1.val().trim(),
    address_2: address_2.val().trim(),
    address_3: address_3.val().trim(),
    admin_contact: admin_contact.val().trim(),
    telephone_num: telephone_num.val().trim(),
    cellphone_num: cellphone_num.val().trim(),
    email_address: email_address.val().trim(),
    website_url: website_url.val().trim(),
    num_students: num_students.val().trim()
  };

  // Check if school_id is not null
  if (school_id !== null) {
    data.school_id = school_id;
  }

  // Disable Button
  submit.prop('disabled', true);

  // Submit
  $.ajax({
    type: 'POST',
    url: school_id !== null ? 'school/school_update' : 'school/school_create',
    data: data,
    success: function(d) {
      var data = JSON.parse(d);

      if (data.result === false) {
        alert(data.message);
        reset_form(modal);
        return;
      } else {
        // Reset Modal and inputs
        reset_form(modal, true);
        form_enable(modal, true);

        // Get School List
        school_list_get();
      }
    }
  });
}

function school_list_get() {
  // Check if table is initialized
  if (!$.fn.DataTable.isDataTable('#school_list_modal table')) {
    datatable_school_list = $('#school_list_modal table').DataTable({
      ajax: {
        url: 'school/school_get',
        type: 'POST'
      },
      dom: "<'row'<'col-sm-6'l><'col-sm-6'f>>" +
        "<'row'<'col-sm-12'tr>>" +
        "<'row'<'col-sm-12'p>>",
      pagingType: 'full_numbers',
      language: {
        paginate: {
          first: '&laquo;',
          previous: '&lsaquo;',
          next: '&rsaquo;',
          last: '&raquo;'
        }
      },
      columnDefs: [{
        className: 'col-md-4',
        targets: [0]
      }, {
        className: 'col-md-3',
        targets: [1]
      }, {
        className: 'col-md-3 text-center',
        targets: [2]
      }, {
        className: 'col-md-2 text-center',
        targets: [3]
      }],
      initComplete: function(settings, json) {
        $('#school_list_modal #table-school-list_wrapper .dataTables_paginate').css('text-align', 'center');
        $('#school_list_modal [data-toggle="tooltip"]').tooltip();
        $('#school_list_modal [data-action="edit"').click(function(event) {
          event.preventDefault();
          school_edit($(this).data('id'), $(this).data('name'));
        });
        $('#school_list_modal [data-action="view"').click(function(event) {
          event.preventDefault();
          school_view($(this).data('id'), $(this).data('name'));
        });
      }
    });
  }
  // Else, update data
  else {
    datatable_school_list.ajax.reload(function() {
      $('#school_list_modal [data-toggle="tooltip"]').tooltip();
      $('#school_list_modal [data-action="edit"').click(function(event) {
        event.preventDefault();
        school_edit($(this).data('id'), $(this).data('name'));
      });
      $('#school_list_modal [data-action="view"').click(function(event) {
        event.preventDefault();
        school_view($(this).data('id'), $(this).data('name'));
      });
    }, false);
  }
}

function school_edit(id, school_name) {
  var modal = $('#school_list_modal'),
    name = modal.find('input[name="name"]'),
    address_1 = modal.find('input[name="address_1"]'),
    address_2 = modal.find('input[name="address_2"]'),
    address_3 = modal.find('input[name="address_3"]'),
    admin_contact = modal.find('input[name="admin_contact"]'),
    telephone_num = modal.find('input[name="telephone_num"]'),
    cellphone_num = modal.find('input[name="cellphone_num"]'),
    email_address = modal.find('input[name="email_address"]'),
    website_url = modal.find('input[name="website_url"]'),
    num_students = modal.find('input[name="num_students"]');

  $('#school_list_modal .nav-tabs li:nth-child(2) a').text('Edit School - ' + school_name);
  $('#school_list_modal .form-container').css('visibility', 'hidden');
  $('#school_list_modal .loader').removeClass('hide');
  form_enable(modal, false);

  // Set School Id
  school_id = id;

  $.ajax({
    type: 'POST',
    url: 'school/school_get',
    data: {
      school_id: id
    },
    success: function(d) {
      var data = JSON.parse(d);

      name.val(data.name);
      address_1.val(data.address_1);
      address_2.val(data.address_2);
      address_3.val(data.address_3);
      admin_contact.val(data.admin_contact);
      telephone_num.val(data.telephone_num);
      cellphone_num.val(data.cellphone_num);
      email_address.val(data.email_address);
      website_url.val(data.website_url);
      num_students.val(data.num_students);

      $('#school_list_modal .form-container').css('visibility', '');
      $('#school_list_modal .loader').addClass('hide');
    }
  });
}

function school_view(id, school_name) {
  $('#school_list_modal .nav-tabs li:nth-child(2) a').text('View School Details - ' + school_name);
  $('#school_list_modal .form-container').css('visibility', 'hidden');
  $('#school_list_modal .loader').removeClass('hide');
  form_enable($('#school_list_modal'), false);

  // Set School Id
  school_id = id;

  $.ajax({
    type: 'POST',
    url: 'school/school_view',
    data: {
      school_id: id
    },
    success: function(d) {
      var data = JSON.parse(d);

      $('#school_list_modal .form-container').css('visibility', '');
      $('#school_list_modal .form-container').hide();
      $('#school_list_modal .view-container').removeClass('hide');
      $('#school_list_modal .view-container .content').html(data.html);
      $('#school_list_modal .loader').addClass('hide');
    }
  });
}

function class_list_get() {
  // Check if table is initialized
  if (!$.fn.DataTable.isDataTable('#class_list_modal table')) {
    datatable_class_list = $('#class_list_modal table').DataTable({
      ajax: {
        url: 'school/class_get',
        type: 'POST',
        data: function(d) {
          d.subject_id = $('#list_subject').val();
        }
      },
      dom: "<'row'<'col-sm-6'l><'col-sm-6'f>>" +
        "<'row'<'col-sm-12'tr>>" +
        "<'row'<'col-sm-12'p>>",
      pagingType: 'full_numbers',
      language: {
        paginate: {
          first: '&laquo;',
          previous: '&lsaquo;',
          next: '&rsaquo;',
          last: '&raquo;'
        }
      },
      columnDefs: [{
        className: 'col-md-5',
        targets: [0]
      }, {
        className: 'col-md-4',
        targets: [1]
      }, {
        className: 'col-md-1 text-center',
        targets: [2]
      }, {
        className: 'col-md-2 text-center',
        targets: [3]
      }],
      initComplete: function(settings, json) {
        $('#class_list_modal #table-class-list_wrapper .dataTables_paginate').css('text-align', 'center');
				class_button_events();
      }
    });
  }
  // Else, update data
  else {
    datatable_class_list.ajax.reload(function() {
			class_button_events();
		}, false);
  }
}

function class_edit(id, class_name) {
  var modal = $('#class_list_modal'),
    name = modal.find('input[name="name"]'),
    subject = modal.find('select[name="subject"]');

  $('#class_list_modal .nav-tabs li:nth-child(2) a').text('Edit Class - ' + class_name);
  $('#class_list_modal .form-container').css('visibility', 'hidden');
  $('#class_list_modal .loader').removeClass('hide');
  form_enable(modal, false);

  // Set Class Id
  class_id = id;

  $.ajax({
    type: 'POST',
    url: 'school/class_get',
    data: {
      class_id: id
    },
    success: function(d) {
      var data = JSON.parse(d);

      name.val(data.name);
      subject.val(data.subject_id);
      subject.trigger('chosen:updated');

      $('#class_list_modal .form-container').css('visibility', '');
      $('#class_list_modal .loader').addClass('hide');
    }
  });
}

function class_status(id, bool) {
  var data = {
    class_id: id,
    status: bool
  };

  $.ajax({
    type: 'POST',
    url: 'school/class_update_status',
    data: data,
    success: function(d) {
      var data = JSON.parse(d);

      // Get Class List
      class_list_get();

      // Update Class List Dropwdown
      update_class_list_dropdown();
    }
  });
}

function class_list_save() {
  var modal = $('#class_list_modal'),
    name = modal.find('input[name="name"]'),
    subject = modal.find('select[name="subject"]'),
    submit = modal.find('.submit'),
    valid = true;

  // Reset Modal first
  reset_form(modal);

  // Validate

  // Name
  if (name.val().trim() === '') {
    name.addClass('error');
    valid = false;
  }

  // Subject
  if (subject.val() === '') {
    var chosen_container = subject.next('.chosen-container');
    chosen_container.find('.chosen-single, .chosen-drop').addClass('error');
    valid = false;
  }

  // Show Error Message
  if (!valid) {
    modal.find('.error-message').removeClass('invisible');
    return;
  }

  // Submit Form
  var data = {
    name: name.val().trim(),
    subject_id: subject.val()
  };

  // Check if class_id is not null
  if (class_id !== null) {
    data.class_id = class_id;
  }

  // Disable Button
  submit.prop('disabled', true);

  // Submit
  $.ajax({
    type: 'POST',
    url: class_id !== null ? 'school/class_update' : 'school/class_create',
    data: data,
    success: function(d) {
      var data = JSON.parse(d);

      if (data.result === false) {
        alert(data.message);
        reset_form(modal);
        return;
      } else {
        // Reset Modal and inputs
        reset_form(modal, true);
        form_enable(modal, true);

        // Get Class List
        $('#list_subject').val('');
        $('#list_subject').trigger('chosen:updated');
        class_list_get();

        // Update Class List Dropwdown
        update_class_list_dropdown();
      }
    }
  });
}

function class_button_events() {
	// Unbind click
	$('#class_list_modal').off('click', '[data-action="edit"]');
	$('#class_list_modal').off('click', '[data-action="deactivate"]');
	$('#class_list_modal').off('click', '[data-action="activate"]');

	// Bind click
	$('#class_list_modal').on('click', '[data-action="edit"]', function(event) {
		event.preventDefault();
		class_edit($(this).data('id'), $(this).data('name'));
	});
	$('#class_list_modal').on('click', '[data-action="deactivate"]', function(event) {
		event.preventDefault();
		class_status($(this).data('id'), false);
	});
	$('#class_list_modal').on('click', '[data-action="activate"]', function(event) {
		event.preventDefault();
		class_status($(this).data('id'), true);
	});

	// Tooltip
	datatable_class_list.$('[data-toggle="tooltip"]').tooltip();
}

function teacher_list_get() {
  // Check if table is initialized
  if (!$.fn.DataTable.isDataTable('#teacher_list_modal table')) {
    datatable_teacher_list = $('#teacher_list_modal table').DataTable({
      ajax: {
        url: 'school/teacher_get',
        type: 'POST'
      },
      dom: "<'row'<'col-sm-6'l><'col-sm-6'f>>" +
        "<'row'<'col-sm-12'tr>>" +
        "<'row'<'col-sm-12'p>>",
      pagingType: 'full_numbers',
      language: {
        paginate: {
          first: '&laquo;',
          previous: '&lsaquo;',
          next: '&rsaquo;',
          last: '&raquo;'
        }
      },
      columnDefs: [{
        className: 'col-md-2',
        targets: [0]
      }, {
        className: 'col-md-2',
        targets: [1]
      }, {
        className: 'col-md-2',
        targets: [2]
      }, {
        className: 'col-md-3',
        targets: [3]
      }, {
        className: 'col-md-1 text-center',
        targets: [4]
      }, {
        className: 'col-md-2 text-center',
        targets: [5]
      }],
      initComplete: function(settings, json) {
        $('#teacher_list_modal #table-teacher-list_wrapper .dataTables_paginate').css('text-align', 'center');
				teacher_button_events();
      }
    });
  }
  // Else, update data
  else {
    datatable_teacher_list.ajax.reload(function() {
			teacher_button_events();
    }, false);
  }
}

function teacher_edit(id, teacher_name) {
  var modal = $('#teacher_list_modal'),
    firstname = modal.find('input[name="firstname"]'),
    surname = modal.find('input[name="surname"]'),
    classes = modal.find('select[name="classes"]'),
    username = modal.find('input[name="username"]'),
    password = modal.find('input[name="password"]');

  $('#teacher_list_modal .nav-tabs li:nth-child(2) a').text('Edit Teacher - ' + teacher_name);
  $('#teacher_list_modal .form-container').css('visibility', 'hidden');
  $('#teacher_list_modal .loader').removeClass('hide');
  form_enable(modal, false);

  // Set Teacher Id
  teacher_id = id;

  $.ajax({
    type: 'POST',
    url: 'school/teacher_get',
    data: {
      teacher_id: id
    },
    success: function(d) {
      var data = JSON.parse(d);

      firstname.val(data.firstname);
      surname.val(data.surname);
      var class_ids = data.class_ids.split(', ');
      classes.val(class_ids);
      classes.trigger('chosen:updated');
      username.val(data.username);
      password.val(data.password);

      $('#teacher_list_modal .form-container').css('visibility', '');
      $('#teacher_list_modal .loader').addClass('hide');
    }
  });
}

function teacher_status(id, bool) {
  var data = {
    teacher_id: id,
    status: bool
  };

  $.ajax({
    type: 'POST',
    url: 'school/teacher_update_status',
    data: data,
    success: function(d) {
      var data = JSON.parse(d);

      // Get Class List
      teacher_list_get();
    }
  });
}

function teacher_list_save() {
  var modal = $('#teacher_list_modal'),
    firstname = modal.find('input[name="firstname"]'),
    surname = modal.find('input[name="surname"]'),
    classes = modal.find('select[name="classes"]'),
    username = modal.find('input[name="username"]'),
    password = modal.find('input[name="password"]'),
    submit = modal.find('.submit'),
    valid = true;

  // Reset Modal first
  reset_form(modal);

  // Validate

  // Firstname
  if (firstname.val().trim() === '') {
    firstname.addClass('error');
    valid = false;
  }

  // Surname
  if (surname.val().trim() === '') {
    surname.addClass('error');
    valid = false;
  }

  // Classes
  if (classes.val() === null || classes.val().toString() === '') {
    var chosen_container = classes.next('.chosen-container');
    chosen_container.find('.chosen-choices, .chosen-drop').addClass('error');
    valid = false;
  }

  // Show Error Message
  if (!valid) {
    modal.find('.error-message').removeClass('invisible');
    return;
  }

  // Submit Form
  var data = {
    firstname: firstname.val().trim(),
    surname: surname.val().trim(),
    classes: classes.val().toString(),
    username: username.val().trim(),
    password: password.val().trim()
  };

  // Check if teacher_id is not null
  if (teacher_id !== null) {
    data.teacher_id = teacher_id;
  }

  // Disable Button
  submit.prop('disabled', true);

  // Submit
  $.ajax({
    type: 'POST',
    url: teacher_id !== null ? 'school/teacher_update' : 'school/teacher_create',
    data: data,
    success: function(d) {
      var data = JSON.parse(d);

      if (data.result === false) {
        alert(data.message);
        reset_form(modal);
        return;
      } else {
        // Reset Modal and inputs
        reset_form(modal, true);
        form_enable(modal, true);

        teacher_list_get();
      }
    }
  });
}

function teacher_button_events() {
	// Unbind click
	$('#teacher_list_modal').off('click', '[data-action="edit"]');
	$('#teacher_list_modal').off('click', '[data-action="deactivate"]');
	$('#teacher_list_modal').off('click', '[data-action="activate"]');

	// Bind click
	$('#teacher_list_modal').on('click', '[data-action="edit"]', function(event) {
		event.preventDefault();
		teacher_edit($(this).data('id'), $(this).data('name'));
	});
	$('#teacher_list_modal').on('click', '[data-action="deactivate"]', function(event) {
		event.preventDefault();
		teacher_status($(this).data('id'), false);
	});
	$('#teacher_list_modal').on('click', '[data-action="activate"]', function(event) {
		event.preventDefault();
		teacher_status($(this).data('id'), true);
	});

	// Tooltip
	datatable_teacher_list.$('[data-toggle="tooltip"]').tooltip();
}

function student_list_get() {
  // Check if table is initialized
  if (!$.fn.DataTable.isDataTable('#student_list_modal table')) {
    datatable_student_list = $('#student_list_modal table').DataTable({
      ajax: {
        url: 'school/student_get',
        type: 'POST'
      },
      dom: "<'row'<'col-sm-6'l><'col-sm-6'f>>" +
        "<'row'<'col-sm-12'tr>>" +
        "<'row'<'col-sm-12'p>>",
      pagingType: 'full_numbers',
      language: {
        paginate: {
          first: '&laquo;',
          previous: '&lsaquo;',
          next: '&rsaquo;',
          last: '&raquo;'
        }
      },
      columnDefs: [{
        className: 'col-md-2',
        targets: [0]
      }, {
        className: 'col-md-2',
        targets: [1]
      }, {
        className: 'col-md-2',
        targets: [2]
      }, {
        className: 'col-md-2',
        targets: [3]
      }, {
        className: 'col-md-2',
        targets: [4]
      }, {
        className: 'col-md-1 text-center',
        targets: [5]
      }, {
        className: 'col-md-1 text-center',
        targets: [6]
      }],
      initComplete: function(settings, json) {
        $('#student_list_modal #table-student-list_wrapper .dataTables_paginate').css('text-align', 'center');
				student_button_events();
      }
    });
  }
  // Else, update data
  else {
    datatable_student_list.ajax.reload(function() {
			student_button_events();
    }, false);
  }
}

function student_edit(id, student_name) {
  var modal = $('#student_list_modal'),
    firstname = modal.find('input[name="firstname"]'),
    surname = modal.find('input[name="surname"]'),
    _class = modal.find('select[name="class"]'),
    username = modal.find('input[name="username"]'),
    password = modal.find('input[name="password"]');

  $('#student_list_modal .nav-tabs li:nth-child(2) a').text('Edit Student - ' + student_name);
  $('#student_list_modal .form-container').css('visibility', 'hidden');
  $('#student_list_modal .loader').removeClass('hide');
  form_enable(modal, false);

  // Set Student Id
  student_id = id;

  $.ajax({
    type: 'POST',
    url: 'school/student_get',
    data: {
      student_id: id
    },
    success: function(d) {
      var data = JSON.parse(d);

      firstname.val(data.firstname);
      surname.val(data.surname);
      _class.val(data.class_id);
      _class.trigger('chosen:updated');
      username.val(data.username);
      password.val(data.password);

      $('#student_list_modal .form-container').css('visibility', '');
      $('#student_list_modal .loader').addClass('hide');
    }
  });
}

function student_status(id, bool) {
  var data = {
    student_id: id,
    status: bool
  };

  $.ajax({
    type: 'POST',
    url: 'school/student_update_status',
    data: data,
    success: function(d) {
      var data = JSON.parse(d);

      // Get Class List
      student_list_get();
    }
  });
}

function student_list_save() {
  var modal = $('#student_list_modal'),
    firstname = modal.find('input[name="firstname"]'),
    surname = modal.find('input[name="surname"]'),
    _class = modal.find('select[name="class"]'),
    username = modal.find('input[name="username"]'),
    password = modal.find('input[name="password"]'),
    submit = modal.find('.submit'),
    valid = true;

  // Reset Modal first
  reset_form(modal);

  // Validate

  // Firstname
  if (firstname.val().trim() === '') {
    firstname.addClass('error');
    valid = false;
  }

  // Surname
  if (surname.val().trim() === '') {
    surname.addClass('error');
    valid = false;
  }

  // Class
  if (_class.val() === '') {
    var chosen_container = _class.next('.chosen-container');
    chosen_container.find('.chosen-single, .chosen-drop').addClass('error');
    valid = false;
  }

  // Username
  if (username.val().trim() === '') {
    username.addClass('error');
    valid = false;
  }

  // Password
  if (password.val().trim() === '') {
    password.addClass('error');
    valid = false;
  }

  // Show Error Message
  if (!valid) {
    modal.find('.error-message').removeClass('invisible');
    return;
  }

  // Submit Form
  var data = {
    firstname: firstname.val().trim(),
    surname: surname.val().trim(),
    class_id: _class.val(),
    username: username.val(),
    password: password.val()
  };

  // Check if teacher_id is not null
  if (student_id !== null) {
    data.student_id = student_id;
  }

  // Disable Button
  submit.prop('disabled', true);

  // Submit
  $.ajax({
    type: 'POST',
    url: student_id !== null ? 'school/student_update' : 'school/student_create',
    data: data,
    success: function(d) {
      var data = JSON.parse(d);

      if (data.result === false) {
        alert(data.message);
        reset_form(modal);
        return;
      } else {
        // Reset Modal and inputs
        reset_form(modal, true);
        form_enable(modal, true);

        student_list_get();
      }
    }
  });
}

function student_button_events() {
	// Unbind click
	$('#student_list_modal').off('click', '[data-action="edit"]');
	$('#student_list_modal').off('click', '[data-action="deactivate"]');
	$('#student_list_modal').off('click', '[data-action="activate"]');

	// Bind click
	$('#student_list_modal').on('click', '[data-action="edit"]', function(event) {
		event.preventDefault();
		student_edit($(this).data('id'), $(this).data('name'));
	});
	$('#student_list_modal').on('click', '[data-action="deactivate"]', function(event) {
		event.preventDefault();
		student_status($(this).data('id'), false);
	});
	$('#student_list_modal').on('click', '[data-action="activate"]', function(event) {
		event.preventDefault();
		student_status($(this).data('id'), true);
	});

	// Tooltip
	datatable_student_list.$('[data-toggle="tooltip"]').tooltip();
}

function reset_form(modal, reset_input) {
  reset_input = reset_input || false;

  // Reset Error Message
  modal.find('.error-message').addClass('invisible');

  // Remove error class
  modal.find('input.input-form, select.input-form').each(function() {
    if ($(this).is('select')) {
      var chosen_container = $(this).next('.chosen-container');
      chosen_container.find('.chosen-single, .chosen-choices, .chosen-drop').removeClass('error');
    } else {
      $(this).removeClass('error');
    }

    if (reset_input) {
      if ($(this).is('select')) {
        $(this).val('');
        $(this).trigger('chosen:updated');
      } else {
        $(this).val('');
      }
    }
  });

  // Enable save button
  modal.find('.submit').prop('disabled', false);
}

function form_enable(modal, bool) {
  if (bool) {
    modal.find('.nav-tabs li:nth-child(1), .nav-tabs li:nth-child(3)').removeClass('disabled');
    modal.find('.nav-tabs li:nth-child(1) a').tab('show');
    modal.find('.btn.cancel').addClass('hide');
    modal.find('.form-container').show();
    modal.find('.form-container').css('visibility', '');
    modal.find('.view-container').addClass('hide');
    modal.find('.loader').addClass('hide');
    school_id = null;
    class_id = null;
    teacher_id = null;
    student_id = null;

    switch (modal[0].id) {
      case 'school_list_modal':
        modal.find('.nav-tabs li:nth-child(2) a').text('Create School');
        break;
      case 'class_list_modal':
        modal.find('.nav-tabs li:nth-child(2) a').text('Create Class');
        break;
      case 'teacher_list_modal':
        modal.find('.nav-tabs li:nth-child(2) a').text('Create Teacher');
        break;
      case 'student_list_modal':
        modal.find('.nav-tabs li:nth-child(2) a').text('Create Student');
        break;
      default:
        break;
    }
  } else {
    modal.find('.nav-tabs li:nth-child(1), .nav-tabs li:nth-child(3)').addClass('disabled');
    modal.find('.btn.cancel').removeClass('hide');
    modal.find('.nav-tabs li:nth-child(2) a').tab('show');
  }
}

function update_class_list_dropdown() {
  $.ajax({
    type: 'POST',
    url: 'school/class_get_dropdown',
    success: function(d) {
      var data = JSON.parse(d);

      // Remove Options, except first
      $('#teacher_list_modal select#classes option:not(:first)').remove();
      $('#student_list_modal select#class option:not(:first)').remove();

      // Append Options
      for (var i = 0; i < data.length; i++) {
        var option = '<option value="' + data[i].class_id + '">' + data[i].name + '</option>';

        $('#teacher_list_modal select#classes').append(option);
        $('#student_list_modal select#class').append(option);
      }

      // Update Select element using chosen
      $('#teacher_list_modal select#classes').trigger('chosen:updated');
      $('#student_list_modal select#class').trigger('chosen:updated');
    }
  });
}

function generate_random_username_password(role) {
  var modal = $('#' + role + '_list_modal'),
    username = modal.find('input[name="username"]'),
    password = modal.find('input[name="password"]');

  username.val(random_text(4, 2));
  password.val(random_text(4, 2));
}

function random_text(num_l, num_n) {
  var chars = 'abcdefghijklmnopqrstuvwxyz0123456789';
  var text = '';
  var len = num_l + num_n;
  var c_num_l = 0;
  var c_num_n = 0;
  for (var i = 0; i < len; i++) {
    var valid = false;
    while (!valid) {
      var char = chars.charAt(Math.floor(Math.random() * chars.length));
      if (isNaN(char) && c_num_l < num_l) {
        text += char;
        c_num_l++;
        valid = true;
      } else if (!isNaN(char) && c_num_n < num_n) {
        text += char;
        c_num_n++;
        valid = true;
      }
    }
  }

  return text;
}
