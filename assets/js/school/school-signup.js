var email_regex = /[a-z0-9!#$%&'*+\=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+\=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/,
	url_regex = /([-a-zA-Z0-9^\p{L}\p{C}\u00a1-\uffff@:%_\+.~#?&//=]{2,256}){1}(\.[a-z]{2,4}){1}(\:[0-9]*)?(\/[-a-zA-Z0-9\u00a1-\uffff\(\)@:%,_\+.~#?&//=]*)?([-a-zA-Z0-9\(\)@:%,_\+.~#?&//=]*)?/,
	num_regex = /^[1-9]\d*$/;

$(document).ready(function() {

	$('.school-signup .btn.submit').click(function(event) {
		event.preventDefault();
		sign_up();
	});
});

function sign_up() {
	var form = $('.school-signup form'),
		name = form.find('input[name="name"]'),
		address_1 = form.find('input[name="address_1"]'),
		address_2 = form.find('input[name="address_2"]'),
		address_3 = form.find('input[name="address_3"]'),
		admin_contact = form.find('input[name="admin_contact"]'),
		telephone_num = form.find('input[name="telephone_num"]'),
		cellphone_num = form.find('input[name="cellphone_num"]'),
		website_url = form.find('input[name="website_url"]'),
		num_students = form.find('input[name="num_students"]'),
		email_address = form.find('input[name="email_address"]'),
		password = form.find('input[name="password"]'),
		confirm_password = form.find('input[name="confirm_password"]'),
		submit = form.find('.submit'),
		error_message = form.find('.error-message'),
		error_text = 'Please input the form correctly.',
		valid = true;

	// Reset Form
	reset_form();

	// Validate

	// Name
	if(name.val().trim() == '') {
		name.addClass('error');
		valid = false;
	}

	// Address 1
	if(address_1.val().trim() == '') {
		address_1.addClass('error');
		valid = false;
	}

	// Admin Contact
	if(admin_contact.val().trim() == '') {
		admin_contact.addClass('error');
		valid = false;
	}


	// Telephone number
	if(telephone_num.val().trim() == '') {
		telephone_num.addClass('error');
		valid = false;
	}

	// Cellphone number
	if(cellphone_num.val().trim() == '') {
		cellphone_num.addClass('error');
		valid = false;
	}

	// Website url
	if(website_url.val().trim() == '' || !url_regex.test(website_url.val().trim())) {
		website_url.addClass('error');
		valid = false;
	}

	// Number of Students
	if(num_students.val().trim() == '' || !num_regex.test(num_students.val().trim())) {
		num_students.addClass('error');
		valid = false;
	}

	// Email address
	if(email_address.val().trim() == '' || !email_regex.test(email_address.val().trim())) {
		email_address.addClass('error');
		valid = false;
	}

	// Password
	if(password.val().trim() == '') {
		password.addClass('error');
		valid = false;
	}

	// Confirm Password
	if(confirm_password.val().trim() == '') {
		confirm_password.addClass('error');
		valid = false;
	}

	// Check if valid is still true
	// And Passwords does not match
	if(valid && password.val() != confirm_password.val()) {
		password.addClass('error');
		confirm_password.addClass('error');
		valid = false;
		error_text = 'Passwords does not match.'
	}

	// Show Error Message
	if(!valid) {
		error_message.text(error_text);
		error_message.removeClass('invisible');
		return;
	}

	// Submit Form
	var data = {
		name: name.val().trim(),
		address_1: address_1.val().trim(),
		address_2: address_2.val().trim(),
		address_3: address_3.val().trim(),
		admin_contact: admin_contact.val().trim(),
		telephone_num: telephone_num.val().trim(),
		cellphone_num: cellphone_num.val().trim(),
		website_url: website_url.val().trim(),
		num_students: num_students.val().trim(),
		email_address: email_address.val().trim(),
		password: password.val().trim()
	};

	// Disable Button
	submit.prop('disabled', true);

	// Submit
	$.ajax({
		type: 'POST',
		url: 'school/process_signup',
		data: data,
		success: function(data) {
			var data = JSON.parse(data);
			
			if(data.result == false) {
				alert(data.message);
				form.find('input[name="' + data.field + '"]').addClass('error');
				submit.prop('disabled', false);
				return;
			}
			else {
				window.location = data.link;
			}
		}
	});
}

function reset_form() {
	var form = $('.school-signup form');

	// Reset Error Message
	form.find('.error-message').addClass('invisible');

	// Remove error class
	form.find('input.form-control').each(function() {
		$(this).removeClass('error');
	});

	// Enable save button
	form.find('.submit').prop('disabled', false);
}