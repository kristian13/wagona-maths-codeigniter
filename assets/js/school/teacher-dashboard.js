var class_details = {};
var datatable_students_report;

$(document).ready(function() {
	// Flexslider
	$('.flexslider').flexslider({
		animation: 'slide',
		slideshow: false,
		controlNav: false,
		startAt: 1,
		prevText: "<span class='slide-arrow glyphicon glyphicon-chevron-left'></span>",
		nextText: "<span class='slide-arrow glyphicon glyphicon-chevron-right'></span>"
	});

	// Tooltip
	$('[data-toggle="tooltip"]').tooltip();

	// Checkbox Select/Unselect All
	$('#chk_select_all').change(function() {
		var checked = $(this).is(':checked');

		$('input.checkbox_topic').each(function() {
			if(!this.disabled) {
				this.checked = checked;
			}
		});
	});

	// Slimscroll
	$('.thumb-container').slimscroll({
	  height: '170px'
	});

	// Flip Subject Average every 2 seconds
	setInterval(function() {
		$('.badger.subj-ave-flip').toggleClass('flip');
	}, 5000);
});

// Show Modal Topic Launcher
function showTopicLauncher(class_id, class_name, subject_id) {
	$('.topiclaucher').modal('show');
	$('#chk_select_all').prop('checked', false);
	$('#top-title').html(class_name);
	$('#check-topic').html('<img src="' + url + 'assets/images/upload.gif" alt="loading" />');
	class_details.class_id = class_id;
	class_details.class_name = class_name;
	class_details.subject_id = subject_id;

	$.post(
		url + 'teacher/get_teacher_subject_topics',
		{
			class_id: class_id,
			subject_id: subject_id
		},
		function(data) {
			if(data.result === true) {
				$('#check-topic').html(data.html);
			}
			else {
				$('#check-topic').html('Failed to load tpoics, please try again.');
			}
		},
		'json'
	);
}

function showReportModal() {
	var modal = $('#teacher_students_report_modal');
	var checkbox_topics = $('input.checkbox_topic:checked');

	// Validate if there are no topics selected
	if(checkbox_topics.length === 0) {
		alert('Please select topics.');
		return;
	}

	// Show Teacher Topic Reports Modal
	modal.modal('show');
	modal.find('.modal-body').html('<img src="' + url + 'assets/images/upload.gif" alt="loading" />');
	modal.find('.report_class_name').text(class_details.class_name);

	// Initialize Column Headers
	var column_headers = [];
	var topic_ids = [];
	column_headers.push('Surname', 'Firstname');
	checkbox_topics.each(function() {
		column_headers.push($(this).data('description').trim());
		topic_ids.push($(this).val());
	});
	column_headers.push('Average');

	// Check if table is initialized
	if ($.fn.DataTable.isDataTable('#teacher_students_report_modal table')) {
		datatable_students_report.destroy();
	}

	// Initialize Table
	var table = '';
	table += '<table class="table table-striped table-bordered">';
		table += '<thead><tr>';
		$.each(column_headers, function(key, val) {
			var th_class = '';
			if(key > 1 && key < column_headers.length - 1) {
				table += '<th class="rotate-vertical" title="' + val + '"><div><div>' + val + '</div></div></th>';
			}
			else {
				table += '<th>' + val + '</th>';
			}

		});
		table += '</tr></thead>';
	table += '</table>';
	table += '<div class="row"><div class="col-md-2 col-md-offset-5"><button class="btn btn-primary btn-block school-btn-pdf" disabled="disabled" onclick="generatePDFReport()">Generate PDF</button></div></div>';
	modal.find('.modal-body').html(table);

	// Initialize Datatable
	datatable_students_report = $('#teacher_students_report_modal table').DataTable({
		scrollY: '200px',
    scrollX: true,
    scrollCollapse: true,
		ajax: {
			url: url + 'teacher/get_student_report',
			type: 'POST',
			data: function (d) {
				d.class_id = class_details.class_id;
				d.topic_ids = topic_ids;
			}
		},
		pagingType: 'full_numbers',
		language: {
			paginate: {
				first: '&laquo;',
				previous: '&lsaquo;',
				next: '&rsaquo;',
				last: '&raquo;'
			}
		},
		fixedColumns:   {
			leftColumns: 2,
			rightColumns: 1
		},
		initComplete: function() {
			$('.school-student-data-percentage-td').each(function() {
				var classes = $(this).attr('class').replace('school-student-data-percentage-td', '');
				$(this).closest('td').addClass(classes);
			});

			// Enable PDF button
			$('.school-btn-pdf').prop('disabled', '');
		}
	});
}

function showSampleTestQuestions() {
	var checkbox_topics = $('input.checkbox_topic:checked');
	var urltest = url + 'account/test_free_quiz';

	// Validate if there are no topics selected
	if(checkbox_topics.length === 0) {
		alert('Please select topics.');
		return;
	}

	var topic_ids = checkbox_topics.map(function() {
			return this.value;
	}).get().join('_');

	// Go to Test Questions
	window.open(urltest + '/' + topic_ids, '_blank');
}

function generatePDFReport() {
	var modal = $('#teacher_students_report_modal');
	var checkbox_topics = $('input.checkbox_topic:checked');

	// Disable PDF button
	$('.school-btn-pdf').text('Generating...');
	$('.school-btn-pdf').prop('disabled', 'disabled');

	// Initialize Topic Ids
	var column_headers = [];
	var topic_ids = [];
	column_headers.push('Surname', 'Firstname');
	checkbox_topics.each(function() {
		column_headers.push($(this).data('description').trim());
		topic_ids.push($(this).val());
	});
	column_headers.push('Average');

	$.post(
		url + 'teacher/generate_student_report',
		{
			class_id: class_details.class_id,
			topic_ids: topic_ids,
			column_headers: column_headers
		},
		function(data) {
			// Enable PDF button
			$('.school-btn-pdf').text('Generate PDF');
			$('.school-btn-pdf').prop('disabled', '');

			// Open New Window
			window.open(url + 'reports/' + data.file_name, '_blank');
		},
		'json'
	);
}
