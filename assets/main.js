

$(window).load(function(){
	// $('html').fadeIn();
	jQuery('#status').removeClass('fadering');
	jQuery('#status').delay(450).addClass('flipOutX');
	setTimeout(function(){
    jQuery('#status').delay(100).fadeOut(); // will first fade out the loading animation
	  jQuery('#preloader').delay(200).fadeOut('slow'); // will fade out the white DIV that covers the website.
	  jQuery('body').delay(300).css({'overflow':'visible'});
	}, 500);

	setTimeout(function(){
    $('.header-menu').addClass('bounceInDown');
	  $('#banner').addClass('pulse');
	  $('.animated2').addClass('bounceInUp');
	  $('#content').delay(800).addClass('bounceIn')
	 	}, 600);

    setTimeout(function(){
      $('.quesOne').addClass('fadeIn');
      $('.guartee').addClass('fadeIn');
    }, 900);

   setTimeout(function(){
    $('.imagebnner').addClass('fadeInRight');
    },1000);
   $(window).trigger('resize');






//jQuery('.slogan').addClass('fadeInDown');

	// var bgColor = ["#fbdede", "#bcd4ff"];
	// var bgCounter = 0;
	// $('body').css('background',bgColor[bgCounter]);

	// setInterval(function(){
	// 	bgCounter++;
	// 	if(bgCounter > 1){
	// 		bgCounter = 0;
	// 	}
	// 	$('body').css('background',bgColor[bgCounter]);
	// },10000);

});

//effects
$('.rubber-it').addClass('animated');
$('.rubber-it').mouseenter(function(){
	$(this).addClass('rubberBand');
}).mouseleave(function(){
	$(this).removeClass('rubberBand');
});

$('.form-control').focus(function(){
	$(this).addClass('animated pulse');
});
$('.form-control').blur(function(){
	$(this).removeClass('animated pulse');
});

//cube link
$('.cube_link').each(function(){
	var cube_toggle = $(this).attr('data-toggle');
	$(this).append('<span class="cube_hover"><span class="origin">' + cube_toggle + '</span><span class="transform">' + cube_toggle + '</span></span>');
});

$('#banner').click(function(){
	$('.banner-text h3, .banner-text h1, .banner-content').hide();
	$('.banner-text h3').removeClass('pulse');
	$('.banner-text h1').removeClass('pulse');
	$('.banner-content').removeClass('fadeInUp');
  $('.boybnner').removeClass('fadeInRight');
  $('.bagbnner').removeClass('fadeIn');
  $('.imagebnner').removeClass('fadeInRight');

	setTimeout(function(){
		$('.banner-text h3').fadeIn().addClass('pulse');
	},500);
	setTimeout(function(){
		$('.banner-text h1').fadeIn().addClass('pulse');
	},1000);
	setTimeout(function(){
		$('.banner-content').fadeIn().addClass('fadeInUp');
	},200);

  setTimeout(function(){
    $('.boybnner').addClass('fadeInRight');
    $('.bagbnner').addClass('fadeIn');
    $('.imagebnner').addClass('fadeInRight');
  },400);
});

//ready
$(document).ready(function(){
	//menu
	$('.res-nav-toggle').click(function(){
		$('.res-nav-menu').slideToggle();
	});
	//toggler
	$('.toggler').click(function(){
		var toggleElement = $(this).attr('data-toggle');
		$(toggleElement).slideToggle();
	});
	//close
	$('.close-el').click(function(){
		var closeElement = $(this).attr('data-close');
		$(closeElement).fadeOut();
	});
	//tooltip
	$(function () {
		$(".tooltip-bottom").tooltip({placement: 'bottom'});
	});
	//click redirect
	$('.redirect-link').click(function(){
		window.location.assign($(this).attr('data-link'));
	});
	//feedback
	$('.fb-next').click(function(){
		var feedback_div = $(this).closest('.feedback-item');

		// Check if it's final
		if(feedback_div.hasClass('final')) {
			submitFeedback(feedback_div);
		}
		else {
			$('.feedback-item').hide();
			feedback_div.next().fadeIn();
		}
	});
	$('.fb-back').click(function(){
		$('.feedback-item').hide();
		$(this).closest('.feedback-item').prev().fadeIn();
	});


});

function volumeUp() {
  	if ($('.fa-volume-up').hasClass('hide')) {
	  	$('.fa-volume-off').addClass('hide');
	  	$('.fa-volume-up').removeClass('hide');
	  	$('#audio').prop('muted', false); //unmute
	  }
	  else if ($('.fa-volume-off').hasClass('hide')) {
      $('.fa-volume-off').removeClass('hide');
	  	$('.fa-volume-up').addClass('hide');
	  	$('#audio').prop('muted', true); //mute
    }
  }

/*for piechart*/

$(function() {
	$('#btn-load-piechart').click(function() {
    var question_id = quest[current_page - 1];
    var preloader = $('#piechart-preloader');
    var pieChart = $('#pieChart');

    // Hide first the pie chart
    pieChart.hide();
    pieChart.empty();
    $('.pieTip').remove();

    // Show First the Preloader
    preloader.show();

    $.post(
      url + 'account/get_question_details',
      {question_id: question_id},
      function(data) {
        console.log(data);
        var piechart_data;

        if(data.question_count == 0) {
          piechart_data = [
            {
              title: 'Attempted',
              value: data.question_count,
              color: '#ffd302'
            }
          ];
        }
        else {
          piechart_data = [
            {
              title: 'Attempted',
              value: data.question_count,
              color: '#ffd302'
            },
            {
              title: 'Wrong Answers',
              value: data.wrong_count,
              color: '#c0392b'
            },
            {
              title: 'Correct Answers',
              value: data.correct_count,
              color: '#319207'
            }
          ];
        }

        $("#pieChart").drawPieChart(piechart_data);
        $('#piechart-preloader').addClass('flipOutX').fadeOut();
        $("#pieChart").delay(500).fadeIn();
        $('.fLabel').delay(1000).addClass('fadeInLeft');
        $('.sLabel').delay(1200).addClass('fadeInLeft');
        $('.absolutext').delay(1400).addClass('fadeInRight');
      },
      'json'
    );
	});
});

/*!
 * jquery.drawPieChart.js
 * Version: 0.3(Beta)
 * Inspired by Chart.js(http://www.chartjs.org/)
 *
 * Copyright 2013 hiro
 * https://github.com/githiro/drawPieChart
 * Released under the MIT license.
 */
;(function($, undefined) {
  $.fn.drawPieChart = function(data, options) {
    var $this = this,
      W = $this.width(),
      H = $this.height(),
      centerX = W/2,
      centerY = H/2,
      cos = Math.cos,
      sin = Math.sin,
      PI = Math.PI,
      settings = $.extend({
        segmentShowStroke : true,
        segmentStrokeColor : "#fff",
        segmentStrokeWidth : 1,
        baseColor: "#fff",
        baseOffset: 15,
        edgeOffset: 30,//offset from edge of $this
        pieSegmentGroupClass: "pieSegmentGroup",
        pieSegmentClass: "pieSegment",
        lightPiesOffset: 12,//lighten pie's width
        lightPiesOpacity: .3,//lighten pie's default opacity
        lightPieClass: "lightPie",
        animation : true,
        animationSteps : 90,
        animationEasing : "easeInOutExpo",
        tipOffsetX: -15,
        tipOffsetY: -45,
        tipClass: "pieTip",
        beforeDraw: function(){  },
        afterDrawed : function(){  },
        onPieMouseenter : function(e,data){  },
        onPieMouseleave : function(e,data){  },
        onPieClick : function(e,data){  }
      }, options),
      animationOptions = {
        linear : function (t){
          return t;
        },
        easeInOutExpo: function (t) {
          var v = t<.5 ? 8*t*t*t*t : 1-8*(--t)*t*t*t;
          return (v>1) ? 1 : v;
        }
      },
      requestAnimFrame = function(){
        return window.requestAnimationFrame ||
          window.webkitRequestAnimationFrame ||
          window.mozRequestAnimationFrame ||
          window.oRequestAnimationFrame ||
          window.msRequestAnimationFrame ||
          function(callback) {
            window.setTimeout(callback, 1000 / 60);
          };
      }();

    var $wrapper = $('<svg width="' + W + '" height="' + H + '" viewBox="0 0 ' + W + ' ' + H + '" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"></svg>').appendTo($this);
    var $groups = [],
        $pies = [],
        $lightPies = [],
        easingFunction = animationOptions[settings.animationEasing],
        pieRadius = Min([H/2,W/2]) - settings.edgeOffset,
        segmentTotal = 0;

    //Draw base circle
    var drawBasePie = function(){
      var base = document.createElementNS('http://www.w3.org/2000/svg', 'circle');
      var $base = $(base).appendTo($wrapper);
      base.setAttribute("cx", centerX);
      base.setAttribute("cy", centerY);
      base.setAttribute("r", pieRadius+settings.baseOffset);
      base.setAttribute("fill", settings.baseColor);
    }();

    //Set up pie segments wrapper
    var pathGroup = document.createElementNS('http://www.w3.org/2000/svg', 'g');
    var $pathGroup = $(pathGroup).appendTo($wrapper);
    $pathGroup[0].setAttribute("opacity",0);

    //Set up tooltip
    var $tip = $('<div class="' + settings.tipClass + '" />').appendTo('body').hide(),
      tipW = $tip.width(),
      tipH = $tip.height();

    for (var i = 0, len = data.length; i < len; i++){
      segmentTotal += data[i].value;
      var g = document.createElementNS('http://www.w3.org/2000/svg', 'g');
      g.setAttribute("data-order", i);
      g.setAttribute("class", settings.pieSegmentGroupClass);
      $groups[i] = $(g).appendTo($pathGroup);
      $groups[i]
        .on("mouseenter", pathMouseEnter)
        .on("mouseleave", pathMouseLeave)
        .on("mousemove", pathMouseMove)
        .on("click", pathClick);

      var p = document.createElementNS('http://www.w3.org/2000/svg', 'path');
      p.setAttribute("stroke-width", settings.segmentStrokeWidth);
      p.setAttribute("stroke", settings.segmentStrokeColor);
      p.setAttribute("stroke-miterlimit", 2);
      p.setAttribute("fill", data[i].color);
      p.setAttribute("class", settings.pieSegmentClass);
      $pies[i] = $(p).appendTo($groups[i]);

      var lp = document.createElementNS('http://www.w3.org/2000/svg', 'path');
      lp.setAttribute("stroke-width", settings.segmentStrokeWidth);
      lp.setAttribute("stroke", settings.segmentStrokeColor);
      lp.setAttribute("stroke-miterlimit", 2);
      lp.setAttribute("fill", data[i].color);
      lp.setAttribute("opacity", settings.lightPiesOpacity);
      lp.setAttribute("class", settings.lightPieClass);
      $lightPies[i] = $(lp).appendTo($groups[i]);
    }

    settings.beforeDraw.call($this);
    //Animation start
    triggerAnimation();

    function pathMouseEnter(e){
      var index = $(this).data().order;
      $tip.text(data[index].title + ": " + data[index].value).fadeIn(200);
      if ($groups[index][0].getAttribute("data-active") !== "active"){
        $lightPies[index].animate({opacity: .8}, 180);
      }
      settings.onPieMouseenter.apply($(this),[e,data]);
    }
    function pathMouseLeave(e){
      var index = $(this).data().order;
      $tip.hide();
      if ($groups[index][0].getAttribute("data-active") !== "active"){
        $lightPies[index].animate({opacity: settings.lightPiesOpacity}, 100);
      }
      settings.onPieMouseleave.apply($(this),[e,data]);
    }
    function pathMouseMove(e){
      $tip.css({
        top: e.pageY + settings.tipOffsetY,
        left: e.pageX - $tip.width() / 2 + settings.tipOffsetX
      });
    }
    function pathClick(e){
      var index = $(this).data().order;
      var targetGroup = $groups[index][0];
      for (var i = 0, len = data.length; i < len; i++){
        if (i === index) continue;
        $groups[i][0].setAttribute("data-active","");
        $lightPies[i].css({opacity: settings.lightPiesOpacity});
      }
      if (targetGroup.getAttribute("data-active") === "active"){
        targetGroup.setAttribute("data-active","");
        $lightPies[index].css({opacity: .8});
      } else {
        targetGroup.setAttribute("data-active","active");
        $lightPies[index].css({opacity: 1});
      }
      settings.onPieClick.apply($(this),[e,data]);
    }
    function drawPieSegments (animationDecimal){
      var startRadius = -PI/2,//-90 degree
          rotateAnimation = 1;
      if (settings.animation) {
        rotateAnimation = animationDecimal;//count up between0~1
      }

      $pathGroup[0].setAttribute("opacity",animationDecimal);

      //draw each path
      for (var i = 0, len = data.length; i < len; i++){
        var segmentAngle = rotateAnimation * ((data[i].value/segmentTotal) * (PI*2)),//start radian
            endRadius = startRadius + segmentAngle,
            largeArc = ((endRadius - startRadius) % (PI * 2)) > PI ? 1 : 0,
            startX = centerX + cos(startRadius) * pieRadius,
            startY = centerY + sin(startRadius) * pieRadius,
            endX = centerX + cos(endRadius) * pieRadius,
            endY = centerY + sin(endRadius) * pieRadius,
            startX2 = centerX + cos(startRadius) * (pieRadius + settings.lightPiesOffset),
            startY2 = centerY + sin(startRadius) * (pieRadius + settings.lightPiesOffset),
            endX2 = centerX + cos(endRadius) * (pieRadius + settings.lightPiesOffset),
            endY2 = centerY + sin(endRadius) * (pieRadius + settings.lightPiesOffset);
        var cmd = [
          'M', startX, startY,//Move pointer
          'A', pieRadius, pieRadius, 0, largeArc, 1, endX, endY,//Draw outer arc path
          'L', centerX, centerY,//Draw line to the center.
          'Z'//Cloth path
        ];
        var cmd2 = [
          'M', startX2, startY2,
          'A', pieRadius + settings.lightPiesOffset, pieRadius + settings.lightPiesOffset, 0, largeArc, 1, endX2, endY2,//Draw outer arc path
          'L', centerX, centerY,
          'Z'
        ];
        $pies[i][0].setAttribute("d",cmd.join(' '));
        $lightPies[i][0].setAttribute("d", cmd2.join(' '));
        startRadius += segmentAngle;
      }
    }

    var animFrameAmount = (settings.animation)? 1/settings.animationSteps : 1,//if settings.animationSteps is 10, animFrameAmount is 0.1
        animCount =(settings.animation)? 0 : 1;
    function triggerAnimation(){
      if (settings.animation) {
        requestAnimFrame(animationLoop);
      } else {
        drawPieSegments(1);
      }
    }
    function animationLoop(){
      animCount += animFrameAmount;//animCount start from 0, after "settings.animationSteps"-times executed, animCount reaches 1.
      drawPieSegments(easingFunction(animCount));
      if (animCount < 1){
        requestAnimFrame(arguments.callee);
      } else {
        settings.afterDrawed.call($this);
      }
    }
    function Max(arr){
      return Math.max.apply(null, arr);
    }
    function Min(arr){
      return Math.min.apply(null, arr);
    }
    return $this;
  };
})(jQuery);

// Feedback
function submitFeedback(feedback_div) {
	var num_feedback_questions = 18,
			final_message = feedback_div.find('textarea[name="feedback-final-text"]'),
			final_name = feedback_div.find('input[type="text"][name="feedback-final-name"]'),
			final_email = feedback_div.find('input[type="text"][name="feedback-final-email"]'),
			feedback_list = [],
			feedback_obj;

	for(var i = 1; i <= num_feedback_questions; i++ ) {
		var num_input = $('input[type="hidden"][value="' + i + '"]'),
				question_input = num_input.next(),
				form = question_input.closest('form'),
				radio = form.find('input[type="radio"][name="feedback-item-choice-' + i + '"]'),
				textarea = form.find('textarea[name="feedback-item-text-' + i + '"]'),
				details = {};

		details.question = question_input.val();
		details.choice = radio.length > 0 ? (radio.filter(':checked').val() === undefined ? '' : radio.filter(':checked').val()) : 'N/A';
		details.comment = textarea.val().trim();
		feedback_list.push(details);
	}

	// Check if Name are Eamil are not empty and valid
	if(final_name.val().trim() == '' || final_email.val().trim() == '' || /.+@.+/.test(final_email.val().trim()) == false) {
		alert('Enter at least Name and valid Email.');
		return;
	}

	// Disable Buttons
	feedback_div.find('button').prop('disabled', 'disabled');

	feedback_obj = {
		message: final_message.val().trim(),
		name: final_name.val().trim(),
		email: final_email.val().trim(),
		feedback: feedback_list
	};

	$.post( url + 'site/send_my_feedback', feedback_obj,
		function(data){
			$('.feedback-item').hide();
			feedback_div.next().fadeIn();
		},
		'json'
	);
}
